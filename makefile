BASE=.
DEBUG ?= false
CC = $(shell which gcc)

ifneq ($(DEBUG),true)
# compile with full optimization
CFLAGS      = -rdynamic -m64 -O3 -pipe -fexceptions -fno-math-errno -falign-functions=4 -funroll-loops \
               -fexpensive-optimizations -malign-double \
               -finline-functions -fkeep-inline-functions -minline-all-stringops -foptimize-register-move \
               -ftree-loop-linear -ftree-loop-im -ftree-loop-ivcanon -fivopts -fvariable-expansion-in-unroller \
               -fstrength-reduce -frerun-loop-opt -ftree-vectorize -fforce-addr -ftracer -fivopts -funswitch-loops \
               -fmodulo-sched -fgcse-lm -fgcse-sm -fgcse-las -fgcse-after-reload -ftree-ch -funswitch-loops \
               -funsafe-math-optimizations -funswitch-loops -std=c11 -D_ISOC11_SOURCE -D_DEFAULT_SOURCE \
               -D_XOPEN_SOURCE -D_XOPEN_SOURCE_EXTENDED -D_POSIX_C_SOURCE=200809L -fomit-frame-pointer
LIBS = -ldl

else
# compile with debug information
CFLAGS      = -O0 -rdynamic -g -DDEBUG -std=c11 -D_ISOC11_SOURCE -D_DEFAULT_SOURCE -D_XOPEN_SOURCE \
              -D_XOPEN_SOURCE_EXTENDED -D_POSIX_C_SOURCE=200809L
LIBS = -ldl
endif

# libs
LIBS        += -lm `xml2-config --libs`
LIBSPATH    += -L/usr/lib
CFLAGS      += `xml2-config --cflags` -D DISABLE_PARAM_VALIDATION



INCLUDEPATH += -I. -I$(BASE)/lib -I$(BASE)/src -I/usr/include

LIB                      = $(wildcard lib/*.c) $(wildcard lib/*/*.c)
SOURCES                  = $(wildcard src/*.c) $(LIB)
OBJECTS                  = $(patsubst %.c, %.o, $(SOURCES))

SOURCES_TEST             = $(wildcard test/*.c) $(filter-out src/autotester.c, $(SOURCES))
OBJECTS_TEST             = $(patsubst %.c, %.o, $(SOURCES_TEST))

autotester: $(OBJECTS)
	@$(CC) -o $@ $^ $(CFLAGS) $(LIBS) $(INCLUDEPATH)

autotester_test: $(OBJECTS_TEST)
	@$(CC) -o $@ $^ $(CFLAGS) $(LIBS) $(INCLUDEPATH)

clean:
	find $(BASE) -name "*.o" -exec rm -rf {} \;
	rm -rf $(BASE)/autotester

%.o : %.c
	@echo " [CC] $@...."
	@$(CC) -c $(CFLAGS) $< -o $@ $(INCLUDEPATH)

#
#
#
define ASCII_PIC

            _         _ 
           /x\\       /x\\ 
          /v\\x\\     /v\\/\\ 
          \\><\\x\\   /></x/ 
           \\><\\x\\ /></x/ 
   __ __  __\\><\\x/></x/___ 
  /##_##\\/       \\</x/    \\__________ 
 |###|###|  \\         \\    __________\\ 
  \##|##/ \\__\\____\\____\\__/          \\\\ 
    |_|   |  |  | |  | |              \\| 
    \\*/   \\  |  | |  | /              / 
             /    / 

endef
export ASCII_PIC
PIC:
	@echo "$$ASCII_PIC"
