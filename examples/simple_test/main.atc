<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<atc>
  <test name="example system test">
    <description format="markdown">
# test description

This is an example system test where two files are merged into one using the linux system command tool `paste`.

## validations

In this example there are two validations configured to ensure the correctness of the resulting data:

1. The result file must exist and must be non-empty.
2. The result file's checksum must match the expected value. This demonstrates how reference files
can be used to validate the results of the program under test. Reference files could for example contain
result data from previous, successful scientific simulations.
    </description>
    <program name="paste" /><!-- defines the program under test, it must be executable in the environment running autotester
        and can contain a relative or absolute path. The name or path of the program will be inserted in the variable
        $program$ which is used below. -->
    <call expected-return-code="0"><!-- defines how the program will be called in the test and which
        return code is expected -->
      <command>$program$ $file1$ $file2$ &gt; $result$</command><!-- command to be executed, variables will be
        replaced before executing (see the variable definitions below) -->
      <timeout unit="s" kill-signal="9">1</timeout><!-- this defines an optional time limit for the
        command execution, as well as the kill signal which will be used to kill the process if it exceeds the limit -->
    </call>

    <!-- The following section allows to define variables which can be used in the <command> strings of the test itself,
         or also in the commands of the validations.
         The special variables INPUTDATA_PATH, COMPARE_PATH and OUTPUT_PATH are defined globally and point to the
         local subfolders "input/", "compare/" and "output/" by default. Those values can be overwritten in the
         configuration file $HOME/.autotester/config.xml -->
    <variables>
      <variable name="file1">$INPUTDATA_PATH$/data1.txt</variable>
      <variable name="file2">$INPUTDATA_PATH$/data2.txt</variable>
      <variable name="result">$OUTPUT_PATH$/out.txt</variable>
    </variables>

    <validations>
      <validation>
        <description>The result file must exist and must be non-empty</description><!-- The description of a validation is helpful to see what exactly went wrong -->
        <call expected-return-code="0"><!-- the call for a validation is defined in the same way as for the test -->
          <command>test $options$ $result$</command>
        </call>
        <!-- each validation can define variables in a separate scope, but the variables from its parent test are also available -->
        <variables>
          <variable name="options">-s</variable>
        </variables>
      </validation>
      <validation>
        <description>The result file's checksum must match the expected value</description>
        <call>
          <command>grep -q `sha256sum $result$` $checksum_file$</command>
        </call>
        <variables>
          <variable name="checksum_file">$COMPARE_PATH$/checksum.txt</variable>
        </variables>
      </validation>
    </validations>
  </test>
</atc>

