<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:variable name="tests-total" select="count(autotester-results/test[status!='SKIP'])"/>
    <xsl:variable name="tests-passed" select="count(autotester-results/test[status='PASS'])"/>
    <xsl:variable name="tests-failed" select="
         count(autotester-results/test[status='EXECUTION FAIL'])
       + count(autotester-results/test[status='EXECUTION TIMEOUT'])
       + count(autotester-results/test[status='ATC FAIL'])
       + count(autotester-results/test[status='VALIDATION FAIL'])
       + count(autotester-results/test[status='VALIDATION TIMEOUT'])"/>
    <xsl:variable name="tests-skipped" select="count(autotester-results/test[status='SKIP'])"/>
    <xsl:variable name="percentage" select="$tests-passed div $tests-total*100"/>
    <xsl:variable name="validations-total" select="count(autotester-results/test/validations/validation[status!='SKIP'])"/>
    <xsl:variable name="validations-passed" select="count(autotester-results/test/validations/validation[status='PASS'])"/>
    <xsl:variable name="validations-failed" select="
        count(autotester-results/test/validations/validation[status='FAILED'])
      + count(autotester-results/test[status='EXECUTION TIMEOUT'])"/>

    <xsl:text>AUTOTESTER RESULTS&#xA;</xsl:text>
    <xsl:call-template name="separator"/>
    <xsl:apply-templates select="/autotester-results/test"/>
    <xsl:text>SUMMARY&#xa;</xsl:text>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Tests total: </xsl:text>
    <xsl:value-of select="$tests-total"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Tests passed: </xsl:text>
    <xsl:value-of select="$tests-passed"/>
    <xsl:text> (</xsl:text>
    <xsl:value-of select="$percentage"/>
    <xsl:text>%)&#xa;</xsl:text>

    <xsl:text>Tests failed: </xsl:text>
    <xsl:value-of select="$tests-failed"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Tests skipped: </xsl:text>
    <xsl:value-of select="$tests-skipped"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Validations total: </xsl:text>
    <xsl:value-of select="$validations-total"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Validations passed: </xsl:text>
    <xsl:value-of select="$validations-passed"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Validations failed: </xsl:text>
    <xsl:value-of select="$validations-failed"/>
    <xsl:text>&#xa;</xsl:text>

  </xsl:template>
  <xsl:template name="separator">
    <xsl:text>--------------------------------------------------------------------&#xa;</xsl:text>
  </xsl:template>
  <xsl:template name="separator2">
    <xsl:text>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~&#xa;</xsl:text>
  </xsl:template>
  <xsl:template match="test">
    <xsl:text>TEST </xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="status"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>&#xa;&#xa;</xsl:text>

    <xsl:text>From ATC: </xsl:text>
    <xsl:value-of select="@atc"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:text>Group: </xsl:text>
    <xsl:value-of select="@group"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:apply-templates select="./description"/>

    <xsl:apply-templates select="call"/>

    <xsl:apply-templates select="validations"/>

    <xsl:call-template name="separator"/>
  </xsl:template>

  <xsl:template match="description">
    <xsl:if test="string-length(text()) &gt; 0">
      <xsl:text>Description: </xsl:text>
      <xsl:value-of select="normalize-space(.)"/>
      <xsl:text>&#xa;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="call">
    <xsl:apply-templates select="command"/>
    <xsl:text> (</xsl:text>
    <xsl:value-of select="@status"/>
    <xsl:text>)&#xa;</xsl:text>

    <xsl:text>Return code: </xsl:text>
    <xsl:value-of select="@actual-return-code"/>
    <xsl:text> (expected </xsl:text>
    <xsl:value-of select="@expected-return-code"/>
    <xsl:text>) </xsl:text>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="command">
    <xsl:text>Command: </xsl:text>
    <xsl:value-of select="text()"/>
  </xsl:template>

  <xsl:template match="validations">
    <xsl:variable name="validations-passed" select="count(validation[status='PASS'])" />
    <xsl:variable name="validations-total" select="count(validation)" />
    <xsl:text>Validations: </xsl:text>
    <xsl:value-of select="$validations-passed"/>
    <xsl:text> of </xsl:text>
    <xsl:value-of select="$validations-total"/>
    <xsl:text> passed</xsl:text>
    <xsl:text>&#xa;</xsl:text>
    <xsl:call-template name="separator2"/>
    <xsl:apply-templates select="validation"/>
  </xsl:template>

  <xsl:template match="validation">
    <xsl:text>VALIDATION </xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="status"/>
    <xsl:text>&#xa;</xsl:text>

    <xsl:apply-templates select="description"/>
    <xsl:apply-templates select="call"/>
  </xsl:template>

  <xsl:template match="text()"/>
</xsl:stylesheet>
