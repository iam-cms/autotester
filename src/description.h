/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_DESCRIPTION_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_DESCRIPTION_H

#include <libxml/tree.h>

typedef struct description_s {
  char *content;
  char *format;
} Description;

Description* Description_parse(xmlNodePtr node);
void Description_deinit(Description* description);
void Description_toXml(Description* description, xmlNodePtr parent_node);

#endif //POSTPROCESSING_DEBUG_DESCRIPTION_H
