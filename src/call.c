/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include <container/stringconv.h>
#include "call.h"
#include "xmlhelper.h"
#include "xmldef.h"


Call* Call_parse(xmlNodePtr parent_node) {
  Call *result = NULL;
  if (parent_node != NULL) {
    int parsing_result = XML_RESULT_NOT_SET;
    xmlNodePtr callNode = xml_find_node(parent_node, "call", &parsing_result);
    if (callNode != NULL) {
      result = Malloc(sizeof(Call));
      *result = (Call) {
        .command = xml_node_content(callNode, "command", &parsing_result),
        .timeout = Timeout_parse(callNode),
        .actual_return_code = -1
      };
    }
    xml_attribute_long(callNode, "expected-return-code", &parsing_result, &result->expected_return_code);
    if (callNode != NULL) {
      xmlFreeNode(callNode);
    }
  }
  return result;
}

long Call_execute(Call* call, double* systemtime, double* usertime, double* totaltime, char* outputlog_name) {
  long iscall;
  char *call_tmp;

  if (call->timeout != NULL) {
    call_tmp = NULL;
    char *unit_str = call->timeout->time_unit ? call->timeout->time_unit : "";
    String_createFormatedString(&call_tmp, "timeout --signal=%ld %f%s %s", call->timeout->kill_signal, call->timeout->max_time,
                                unit_str, call->command);
  } else {
    call_tmp = Strdup(call->command);
  }
  if (verbose <= VERBOSE_NORMAL) {
    String_appendFormatedString(&call_tmp, " 2>%s ", outputlog_name);
  }

  measureCPUTime(systemtime, usertime, totaltime);
  iscall = System(call_tmp);
  measureCPUTime(systemtime, usertime, totaltime);

  if (WIFEXITED(iscall)) {
    iscall = WEXITSTATUS(iscall);
  }
  Free(call_tmp);
  call->actual_return_code = iscall;
  if (call->timeout && Timeout_occurred(call->timeout, (int) call->actual_return_code)) {
    call->result = CALL_RESULT_TIMEOUT;
  } else {
    call->actual_return_code = iscall;
    call->result = CALL_RESULT_FINISHED;
  }
  return iscall;
}

void Call_deinit(Call* call) {
  if (call->command != NULL) Free(call->command);
  if (call->timeout != NULL) {
    Timeout_deinit(call->timeout);
    Free(call->timeout);
  }
}

void Call_toXml(Call* call, xmlNodePtr parent_node) {

  if (!call || !parent_node) {
    return;
  }

  xmlNodePtr call_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "call");
  xmlAddChild(parent_node, call_node);

  xmlNodePtr command_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "command");
  xmlAddChild(call_node, command_node);
  xmlNodeSetContent(command_node, BAD_CAST call->command);

  char *expected_rc_str = NULL;
  String_createFormatedString(&expected_rc_str, "%ld", call->expected_return_code);
  xmlSetProp(call_node, BAD_CAST "expected-return-code", BAD_CAST expected_rc_str);
  Free(expected_rc_str);

  // do not print the actual return code in case an timeout has occurred
  if (call->actual_return_code != -1 && (!call->timeout || !call->timeout->has_occurred)) {
    char *actual_rc_str = NULL;
    String_createFormatedString(&actual_rc_str, "%ld", call->actual_return_code);
    xmlSetProp(call_node, BAD_CAST "actual-return-code", BAD_CAST actual_rc_str);
    Free(actual_rc_str);
  }

  if (call->result != CALL_RESULT_NOT_SET) {
    char *status_str = NULL;
    if (call->result == CALL_RESULT_FINISHED) {
      status_str = "finished";
    } else if (call->result == CALL_RESULT_TIMEOUT) {
      status_str = "killed";
    }
    xmlSetProp(call_node, BAD_CAST "status", BAD_CAST status_str);
  }

  Timeout_toXml(call->timeout, call_node);
}
