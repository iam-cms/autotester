/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_INTEGRATIONTEST_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_INTEGRATIONTEST_H

#define CONFIG_FILE ".autotester/config.xml"
#define BINARY_VARIABLE_NAME "program"

#include "autotesterlogfile.h"
#include "variablescope.h"
#include "timeout.h"
#include "call.h"
#include "description.h"
#include "program.h"
#include "validation.h"
#include "resultdocument.h"

#define EXECUTION               0
#define VALIDATION_EXTERNAL     1

// status codes (from best to worst)
#define IT_OK                0
#define IT_VALIDATIONTIMEOUT 1
#define IT_VALIDATIONFAIL    2
#define IT_EXECUTIONTIMEOUT  3
#define IT_EXECUTIONCRASH    4

typedef struct environment_s {
  char *name;
  char *value;
} Environment;

#define COMPARE_PATH_INDEX                 0
#define INPUTDATA_PATH_INDEX               1
#define OUTPUT_PATH_INDEX                  2
#define BINARY_PATH_INDEX                  3
#define VALIDATION_BINARY_PATH_INDEX       4


extern Environment environment[5];

void initEnvironment(void);
void deinitEnvironment(void);

typedef struct integrationtest_s {

  long           id;
  char          *name;
  Description   *description;
  Program       *program; // program under test
  Call          *call;

  char          *atc_path;
  char          *atc_group; // group name derived from the atc path
  char          *group; // group name specified in the ATC file

  char          *result;

  char          *base_path;
  char          *outputdir;
  long           validationcount;
  Validation    *validations;

  double         systemtime;
  double         usertime;
  double         totaltime;

  VariableScope *variable_scope;
  AutotesterLog *autotesterlog;

} IntegrationTest;

void IntegrationTest_deleteOutputFiles(IntegrationTest *integrationtest);

long IntegrationTest_init(IntegrationTest *integrationtest, xmlNodePtr integrationtest_node, char *atcfilename,
                          AutotesterLog *autotesterlog, char *overwrite);

void IntegrationTest_deinit(IntegrationTest *integrationtest);

long IntegrationTest_execute(IntegrationTest* integrationtest);
long IntegrationTest_validate(IntegrationTest* integrationtest);

// functions to add and update results to the xml tree of the resultdocument
void
IntegrationTest_toXml(xmlNodePtr parent, struct integrationtest_s *integrationtest);

#endif
