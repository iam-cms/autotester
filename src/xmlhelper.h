/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_XMLHELPER_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_XMLHELPER_H

#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/tree.h>

#include "integrationtest.h"

#define XML_RESULT_NOT_SET                -1
#define XML_RESULT_FOUND                   0
#define XML_RESULT_FOUND_AMBIGUOUS         1
#define XML_RESULT_NOT_FOUND               2
#define XML_TYPE_ERROR                     3

/**
 * Copying nodes recursively with children, properties, etc.
 * http://xmlsoft.org/html/libxml-tree.html#xmlCopyNode
 */
#define XML_COPY_MODE 1

/* functions related to variable parsing */
char* parseVariableValue(xmlNodePtr node, const char* variablename);

/* xml wrapper/helper functions */

char* xml_attribute(xmlNodePtr node, const char* attributename, int* result_code);
void xml_attribute_long(xmlNodePtr node, const char* attributename, int* result_code, long* result);

/**
 * Find a set of nodes by xpath.
 * The result of this function must be free'd by the caller (using xmlXPathFreeObject())
 */
xmlXPathObjectPtr xml_find_nodes(xmlNodePtr node, const char *xpath, int *result_code);
/**
 * Find a single node by xpath.
 * The result of this function must be free'd by the caller (using xmlFreeNode())
 */
xmlNodePtr xml_find_node(xmlNodePtr node, const char* xpath, int* result_code);
char* xml_node_content(xmlNodePtr node, const char* xpath, int* result_code);
void xml_node_content_long(xmlNodePtr node, const char* xpath, int* result_code, long* result);
void xml_node_content_float(xmlNodePtr node, const char* xpath, int* result_code, float* result);
int xml_count_nodes(xmlNodePtr node, const char* childname);

void xml_error_node_missing(const char* missing_element_name, FILE* logfile, xmlNodePtr parent, char* filename);

#endif //POSTPROCESSING_DEBUG_XMLHELPER_H
