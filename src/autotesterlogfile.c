/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <unistd.h>
#include <limits.h>
#include <wrapper.h>
#include <container/stringconv.h>

#include "autotesterlogfile.h"


void AutotesterLog_init(AutotesterLog *autotesterlog, char *logfilename, bool nolocallog) {
  *autotesterlog = (AutotesterLog) {
    .tests             = 0,
    .failedExecutions  = 0,
    .failedValidations = 0,
    .executedTests     = 0,
    .testsOK           = 0,
    .failedTests       = 0,
    .failedATCs        = 0,
    .duration          = 0.0,
    .nolocallog        = nolocallog,

    .logfile = (logfilename == NULL) ? stdout : FOpen(logfilename, "w"),
  };

  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) {
    autotesterlog->logpath = NULL;
    String_createFormatedString(&autotesterlog->logpath, "%s/", cwd);
  } else {
    mydebug("Error: Could not get working directory");
  }

  setbuf(autotesterlog->logfile, NULL); // we will use fork(), no buffer to avoid flush
}

void AutotesterLog_deinit(AutotesterLog* autotesterlog) {
  if (autotesterlog->logfile != stdout) FClose(autotesterlog->logfile);
  if (autotesterlog->logpath) Free(autotesterlog->logpath);

  AutotesterLog_deinitLogFiles(autotesterlog);
}

void AutotesterLog_print(AutotesterLog* autotesterlog) {
  fprintf(autotesterlog->logfile, "-----------Summary----------\n");
  fprintf(autotesterlog->logfile, "[TESTS COUNT    ]: %ld\n",    autotesterlog->tests);
  fprintf(autotesterlog->logfile, "[TESTS EXECUTED ]: %ld\n",    autotesterlog->executedTests);
  fprintf(autotesterlog->logfile, "[OK             ]: %ld\n",    autotesterlog->testsOK);
  fprintf(autotesterlog->logfile, "[FAILED         ]: %ld\n",    autotesterlog->failedTests);
  if (autotesterlog->failedATCs != 0) {
    fprintf(autotesterlog->logfile, "[FAIL ATC       ]: %ld\n",    autotesterlog->failedATCs);
  }
  fprintf(autotesterlog->logfile, "[FAIL EXECUTION ]: %ld\n",    autotesterlog->failedExecutions);
  fprintf(autotesterlog->logfile, "[FAIL VALIDATION]: %ld\n",    autotesterlog->failedValidations);
  fprintf(autotesterlog->logfile, "[CPU TIME       ]: %s\n",     makehumanreadabletime(autotesterlog->duration*100));
}

void AutotesterLog_deleteLogs(AutotesterLog* autotesterlog) {
  if (autotesterlog->executionlog_name) {
    if (checkFileAccess(autotesterlog->executionlog_name)) {
      Unlink(autotesterlog->executionlog_name);
    }
  }
  if (autotesterlog->validationlog_name) {
    if (checkFileAccess(autotesterlog->validationlog_name)) {
      Unlink(autotesterlog->validationlog_name);
    }
  }
  if (autotesterlog->outputlog_name) {
    if (checkFileAccess(autotesterlog->outputlog_name)) {
      Unlink(autotesterlog->outputlog_name);
    }
  }
}

void AutotesterLog_setLogFolder(AutotesterLog* autotesterlog, char* logfolder) {
  // deinit previously initialized log files and log file paths
  AutotesterLog_deinitLogFiles(autotesterlog);

  autotesterlog->executionlog_name = NULL;
  autotesterlog->validationlog_name = NULL;
  autotesterlog->outputlog_name = NULL;
  String_createFormatedString(&autotesterlog->executionlog_name, "%s/log_execution.txt", logfolder);
  String_createFormatedString(&autotesterlog->validationlog_name, "%s/log_validation.txt", logfolder);
  String_createFormatedString(&autotesterlog->outputlog_name, "%s/log_output.txt", logfolder);

  if (autotesterlog->nolocallog) {
    autotesterlog->executionlog  = stdout;
    autotesterlog->validationlog = stdout;
  } else {
    autotesterlog->executionlog  = FOpen(autotesterlog->executionlog_name, "w");
    autotesterlog->validationlog = FOpen(autotesterlog->validationlog_name, "w");
  }

  if (verbose <= VERBOSE_NORMAL) {
    autotesterlog->outputlog  = FOpen(autotesterlog->outputlog_name, "w");
  }
}

void AutotesterLog_deinitLogFiles(AutotesterLog* autotesterlog) {
  if (autotesterlog->executionlog != NULL) {
    if (autotesterlog->executionlog != stdout) {
      FClose(autotesterlog->executionlog);
    }
    autotesterlog->executionlog = NULL;
  }

  if (autotesterlog->validationlog != NULL) {
    if (autotesterlog->validationlog != stdout) {
      FClose(autotesterlog->validationlog);
    }
    autotesterlog->validationlog = NULL;
  }
  if (verbose <= VERBOSE_NORMAL) {
    if (autotesterlog->outputlog != NULL) {
      FClose(autotesterlog->outputlog);
      autotesterlog->outputlog = NULL;
    }
  }

  if (autotesterlog->executionlog_name) Free(autotesterlog->executionlog_name);
  if (autotesterlog->validationlog_name) Free(autotesterlog->validationlog_name);
  if (autotesterlog->outputlog_name) Free(autotesterlog->outputlog_name);
}

