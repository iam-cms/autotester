/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_TIMEOUT_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_TIMEOUT_H

#define DEFAULT_TIMEOUT_KILL_SIGNAL       9
#define DEFAULT_TIMEOUT_TIME_UNIT        "s"

#include <stdbool.h>
#include <libxml/tree.h>

typedef struct timeout_s {
  float          max_time;
  char          *time_unit;
  long           kill_signal;
  bool           has_occurred;
} Timeout;

/**
 * Initialize @param timeout with values parsed from @param parent_node
 * @param timeout will be initialized with the default values
 *  - if @param parent_node is NULL, or
 *  - if @param parent_node does not contain a tag <timeout>
 * @param parent_node Pointer to the xml node containing the tag <timeout>
 * @param timeout Pointer to target Timeout struct pointer
 */
Timeout* Timeout_parse(xmlNodePtr parent_node);
void Timeout_deinit(Timeout* timeout);

/**
 * Check if a timeout occurred based on the @param return_code of the call.
 *
 * The return code of the 'timeout' command-line utility tells if a timeout occurred:
 *  - 137 if a timeout occurred and when signal 9 was used to kill the process,
 *  - 124 if a timeout occurred and when any other signal was used to kill the process,
 *  - return value of the called program in case no timeout occurred */
bool Timeout_occurred(Timeout* timeout, int return_code);
void Timeout_toXml(Timeout* timeout, xmlNodePtr parent_node);

#endif
