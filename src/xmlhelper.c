/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include <container/stringconv.h>
#include "xmlhelper.h"

char* parseVariableValue(xmlNodePtr node, const char* variablename) {
  char *result = NULL;
  if (!variablename || !node || strlen(variablename) == 0) {
    return result;
  }
  int parsing_result = XML_RESULT_NOT_SET;
  char *xpath = NULL;
  String_createFormatedString(&xpath, "variable[@name='%s']", variablename);
  char *value = xml_node_content(node, xpath, &parsing_result);
  Free(xpath);
  if (value != NULL) {
    result = Strdup(value);
    Free(value);
  }
  return result;
}


/* xml wrapper/helper functions */

char* xml_attribute(xmlNodePtr node, const char* attributename, int* result_code) {
  char *result = NULL;
  char *attributevalue = (char*) xmlGetProp(node, BAD_CAST attributename);
  if (attributevalue != NULL) {
    *result_code = XML_RESULT_FOUND;
    result = Strdup(attributevalue);
    Free(attributevalue);
  } else {
    *result_code = XML_RESULT_NOT_FOUND;
  }
  return result;
}

void xml_attribute_long(xmlNodePtr node, const char* attributename, int* result_code, long* result) {
  char *endptr;
  xmlChar *raw_result = xmlGetProp(node, BAD_CAST attributename);
  if (raw_result != NULL) {
    long result_long = strtol((char*) raw_result, &endptr, 10);
    if (*endptr != '\0' || strlen((char*) raw_result) == 0) {
      mywarn("Unable to parse attribute '%s' in tag '%s'. Value must be integer.", attributename, node->name);
      *result_code = XML_TYPE_ERROR;
    } else {
      *result = result_long;
      *result_code = XML_RESULT_FOUND;
    }
    Free(raw_result);
  } else {
    *result_code = XML_RESULT_NOT_FOUND;
  }
}


xmlXPathObjectPtr xml_find_nodes(xmlNodePtr node, const char *xpath, int *result_code) {
  xmlXPathObjectPtr result = NULL;
  if (node != NULL) {
    xmlXPathContextPtr context = xmlXPathNewContext(node->doc);
    context->node = node;
    result = xmlXPathEvalExpression(BAD_CAST xpath, context);
    if (result != NULL) {
      *result_code = XML_RESULT_FOUND;
    } else {
      *result_code = XML_RESULT_NOT_FOUND;
    }
    xmlXPathFreeContext(context);
  }
  return result;
}

xmlNodePtr xml_find_node(xmlNodePtr node, const char* xpath, int* result_code) {
  xmlNodePtr result = NULL;
  char *xpath_relative = NULL;
  String_createFormatedString(&xpath_relative, "./%s", xpath);
  xmlXPathObjectPtr resultObj = xml_find_nodes(node, xpath_relative, result_code);
  if (resultObj) {
    xmlNodeSetPtr resultSet = resultObj->nodesetval;
    if (!resultSet || resultSet->nodeNr == 0 || !resultSet->nodeTab) {
      *(result_code) = XML_RESULT_NOT_FOUND;
    } else {
      *(result_code) = resultSet->nodeNr > 1 ? XML_RESULT_FOUND_AMBIGUOUS : XML_RESULT_FOUND;
      result = xmlCopyNode(resultSet->nodeTab[0], XML_COPY_MODE);
    }
    xmlXPathFreeObject(resultObj);
  }
  Free(xpath_relative);
  return result;
}

// return the TEXT content of a node (not it's subnodes)
char* xml_node_content(xmlNodePtr node, const char* xpath, int* result_code) {
  char *result = NULL;
  char *xpath_full = NULL;
  String_createFormatedString(&xpath_full, "%s/text()", xpath);
  xmlNodePtr resultNode = xml_find_node(node, xpath_full, result_code);
  if (!(resultNode == NULL || resultNode->content == NULL)) {
    result = Strdup((char*) resultNode->content);
  }
  xmlFreeNode(resultNode);
  Free(xpath_full);
  return result;
}

void xml_node_content_long(xmlNodePtr node, const char* xpath, int* result_code, long* result) {
  char *endptr;
  char *raw_result = xml_node_content(node, xpath, result_code);
  if (raw_result != NULL) {
    long result_long = strtol((char*) raw_result, &endptr, 10);
    if (*endptr != '\0') {
      mywarn("Unable to parse subtag content '%s' in tag '%s'. Value must be integer.", xpath, node->name);
      *result_code = XML_TYPE_ERROR;
    } else {
      *result = result_long;
      *result_code = XML_RESULT_FOUND;
    }
  } else {
    *result_code = XML_RESULT_NOT_FOUND;
  }
}

// reads a floating point or integer value, stores result as float in `result`
void xml_node_content_float(xmlNodePtr node, const char* xpath, int* result_code, float* result) {
  char *endptr;
  char *raw_result = xml_node_content(node, xpath, result_code);
  if (raw_result != NULL) {
    float result_float = strtof((char*) raw_result, &endptr);
    if (*endptr != '\0') {
      mywarn("Unable to parse subtag content '%s' in tag '%s'. Value must be float.", xpath, node->name);
      *result_code = XML_TYPE_ERROR;
    } else {
      *result = result_float;
      *result_code = XML_RESULT_FOUND;
    }
    Free(raw_result);
  } else {
    *result_code = XML_RESULT_NOT_FOUND;
  }
}

int xml_count_nodes(xmlNodePtr node, const char* childname) {
  int result = 0;
  int result_code = XML_RESULT_NOT_SET;
  char *xpath = NULL;
  String_createFormatedString(&xpath, "./%s", childname);
  xmlXPathObjectPtr resultObj = xml_find_nodes(node, xpath, &result_code);
  if (resultObj && resultObj->nodesetval) {
    result = resultObj->nodesetval->nodeNr;
  }
  if (resultObj) xmlXPathFreeObject(resultObj);
  Free(xpath);
  return result;
}

void xml_error_node_missing(const char* missing_element_name, FILE* logfile, xmlNodePtr parent, char* filename) {
    if (parent && filename) {
    mywarn("Error: Required element '%s' not found,"
           " expected in parent node '%s', starting on line '%d' (%s)", missing_element_name,
           parent->name, parent->line, filename);
    fprintf(logfile,
            "Configuration errorneous: Could not get required element '%s',"
            " expected in parent node '%s', starting on line '%d' (%s)", missing_element_name,
            parent->name, parent->line, filename);
  } else {
    mywarn("Error: Required element '%s' not found", missing_element_name);
    fprintf(logfile, "Configuration errorneous: Could not get required element '%s'\n", missing_element_name);
  }
}
