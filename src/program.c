/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include "xmlhelper.h"
#include "xmldef.h"
#include "program.h"

Program* Program_parse(xmlNodePtr parent_node) {
  Program *result = NULL;
  if (parent_node != NULL) {
    int parsing_result = XML_RESULT_NOT_SET;
    xmlNodePtr program_node = xml_find_node(parent_node, "program", &parsing_result);
    if (program_node != NULL) {
      result = Malloc(sizeof(Program));
      *result = (Program) {
        .path = NULL
      };
      char *program_name = xml_attribute(program_node, "name", &parsing_result);
      if (program_name != NULL) {
        result->path = program_name;
      }
      xmlFreeNode(program_node);
    }
  }
  return result;
}

void Program_deinit(Program* program) {
  if (program->path != NULL) Free(program->path);
}

void Program_toXml(Program* program, xmlNodePtr parent_node) {
  if (program != NULL && parent_node != NULL) {
    xmlNodePtr program_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "program");
    xmlAddChild(parent_node, program_node);

    xmlSetProp(program_node, BAD_CAST "name", BAD_CAST program->path);
  }
}
