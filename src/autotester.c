/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <libgen.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <wrapper.h>
#include <container/stringconv.h>
#include <fileop.h>
#include <parameter.h>

#include "autotesterlogfile.h"
#include "integrationtest.h"
#include "xmlhelper.h"
#include "resultdocument.h"

// maximum length of status messages (e.g. "VALIDATION TIMEOUT")
#define STATUS_MAX_WIDTH 18

// bash color codes for printing on stdout
#define COLOR_RED     31
#define COLOR_GREEN   32
#define COLOR_BLUE    34

static char *configpath        = NULL;
static char *singletestfile    = NULL;
static char *logfilename       = NULL;
static char *xmloutputfilename = NULL;
static char *regex             = NULL;
static char *overwrite         = NULL;


static long  recursive      = true;
static long  keeponsuccess  = false;
static long  deleteonerror  = false;
static long  stoponerror    = false;
static long  isdry          = false;
static long  nolocallog     = false;
static long  nocolour       = false;
static long  xmloutput      = false;

argument_t arguments[] = {
  {NULL,           ' ', PARAM_OPTIONAL, "",   NULL, "alternative folder to read the tests from",                                                           PARAM_FILEIN,  &configpath},
  {"single",       's', PARAM_OPTIONAL, "!c",  NULL, "Run a single .atc file.",                                                                            PARAM_STRING,  &singletestfile},
  {"inputdatapath", 'i', PARAM_OPTIONAL, "",   NULL, "alternative folder to read the inputdata from",                                                      PARAM_STRING,  &(environment[INPUTDATA_PATH_INDEX].value)},
  {"binpath",       'b', PARAM_OPTIONAL, "",   NULL, "alternative folder for the binarys to use for testing. (set in ~/.pace3d)",                          PARAM_STRING,  &(environment[BINARY_PATH_INDEX].value)},
  {"logfile",       'l', PARAM_OPTIONAL, "",   NULL, "Give a file to print the log into.",                                                                 PARAM_STRING,  &logfilename},
  {"regex",         'e', PARAM_OPTIONAL, "",   NULL, "fetch a given regex to test with multiple tools",                                                    PARAM_STRING,  &regex},
  {"overwrite",     'o', PARAM_OPTIONAL, "",   NULL, "ONLY FOR PACE3D: Give a list of key=value pairs separated by '|' for overwriting "
                                                    "or appending values in the infile. If '|' is used the list have to be surrounded by \". "
                                                    "This feature is usefulfor simulation series.",                                                       PARAM_STRING,  &overwrite},
  {"deleteonerror",     'x', PARAM_OPTIONAL, "",   NULL, "If tests fail delete the files from test execution.",                                          PARAM_FLAG,    &deleteonerror},
  {"keeponsuccess",     'k', PARAM_OPTIONAL, "",   NULL, "If tests succeed keep the files from test execution.",                                          PARAM_FLAG,    &keeponsuccess},
  {"nolocallog",        'n', PARAM_OPTIONAL, "",   NULL, "Do not create local logfiles. Print on stdout.",                                                 PARAM_FLAG,    &nolocallog},
  {"stoponerror",       'E', PARAM_OPTIONAL, "",   NULL, "Stop on first error",                                                                            PARAM_FLAG,    &stoponerror},
  {"dry",               'd', PARAM_OPTIONAL, "",   NULL, "Only prints the calls from .atc files. No execution.",                                           PARAM_FLAG,    &isdry},
  {"nocolour",          'C', PARAM_OPTIONAL, "",   NULL, "Output buffer will not be coloured",                                                             PARAM_FLAG,    &nocolour},
  {"xmloutput",         'X', PARAM_OPTIONAL, "",   NULL, "Write xml test results (to output.xml if no file name specified using -M)",                      PARAM_FLAG,    &xmloutput},
  {"xmloutputfilename", 'M', PARAM_OPTIONAL, "",   NULL, "Specify the file to print the xml test results into.",                                           PARAM_STRING,  &xmloutputfilename}
};

toolparam_t tool = {
  .description   = "A tool to automatically run system tests and output results.",
  .example       = "e.g.: autotester\n"
                   "Runs tests in this folder",
  .istested      = 100,
  .arguments     = arguments
};


static int cmpstring(const void *a, const void *b) {
  char *stra = (char*)(*(char**)a);
  char *strb = (char*)(*(char**)b);

  long deptha = 0;
  long depthb = 0;
  for (int i=0; stra[i] != '\0'; i++) {
    if (stra[i] == '/') deptha++;
  }
  for (int i=0; strb[i] != '\0'; i++) {
    if (strb[i] == '/') depthb++;
  }

  if (deptha < depthb) return -1;
  if (deptha > depthb) return  1;

  return strcmp(stra, strb);
}


static void printASCII(FILE* file) {
  fprintf(file, "\n\n             Autotester\n");
  fprintf(file, "\n\n");
  fprintf(file, "                           __..--.._\n");
  fprintf(file, "    .....              .--~  .....  `.\n");
  fprintf(file, "  .\":    \"`-..  .    .' ..-'\"    :\". `\n");
  fprintf(file, "  ` `._ ` _.'`\"(     `-\"'`._ ' _.' '  \n");
  fprintf(file, "       ~~~      `.          ~~~       \n");
  fprintf(file, "                .'                    \n");
  fprintf(file, "               /                      \n");
  fprintf(file, "              (                       \n");
  fprintf(file, "               ^---'                  \n\n\n");
}


/** @brief Check if Single .atc file, only one atc file allowed in folder
  */
static void enforceSingleAtcPerFolder(char** atclist, long atclist_count) {
  long  i;
  char *thisResolvedPath = NULL;
  char *thisfilename     = NULL;
  char *nextResolvedPath;
  char *nextfilename;

  resolvePath(&thisResolvedPath, &thisfilename, atclist[0]);
  for (i = 1; i < atclist_count; i++) {
    nextResolvedPath = NULL;
    nextfilename     = NULL;
    resolvePath(&nextResolvedPath, &nextfilename, atclist[i]);
    if (nextResolvedPath == NULL || nextfilename == NULL) {
      myexit(ERROR_BUG, "Could not get path and file from: '%s'", atclist[i]);
    }

    if (strcmp(thisResolvedPath, nextResolvedPath) == 0) {
      myexit(ERROR_BUG, "Only one .atc File (%s) allowed for each folder!", atclist[i]);
    }

    Free(thisResolvedPath);
    thisResolvedPath = nextResolvedPath;
    Free(thisfilename);
    thisfilename = nextfilename;
  }
  Free(thisResolvedPath);
  Free(thisfilename);
}

/** @brief Writes test result to logfile and xml (if the xmloutput option is set)
  */
static void print_test_result(FILE* logfile, char* message, long color_code, long test_number, char* description, char* execpath) {
  int padding = STATUS_MAX_WIDTH - strlen(message);
  int padding_right = (padding / 2);
  if (padding_right < 0) {
    padding_right = 0;
  }
  int padding_left = padding - padding_right;
  if (padding_left < 0) {
    padding_left = 0;
  }

  if (nocolour) {
    fprintf(logfile, "[%*s%s%*s] [Test: %2ld] %s (%s)\n", padding_left, "", message, padding_right, "", test_number, description, execpath);
  } else {
    fprintf(logfile, "[%*s\033[1;%ldm%s\033[30;0m%*s] [Test: %2ld] %s (%s)\n", padding_left, "", color_code, message, padding_right, "", test_number, description, execpath);
  }
}

// Produces a group name for the test based on the location of it's ATC path to the current directory
// Use the relative path of the atc (without the filename at the end)
//     as the group name, if possible. Use "/" for the top level atc.
static char* getGroupName(char *atcpath, char *cwd) {
  char *root_group_name = "/";
  char *group_name = NULL;

  char *path, *filename;
  resolvePath(&path, &filename, atcpath);

  // check if this is the top level atc
  size_t path_length = strlen(path);
  size_t cwd_length = strlen(cwd);
  if ((path_length - 1) - cwd_length <= 0) {
    group_name = Strdup(root_group_name);
  } else {
    group_name = Strdup(path);
    // cut off current directory (cwd) from the beginning
    if (strstr(group_name, cwd)) {
      size_t new_group_name_length = path_length - cwd_length;
      memmove(group_name, group_name + cwd_length, new_group_name_length);
      Realloc(group_name, new_group_name_length);
      group_name[new_group_name_length - 1] = '\0';
    }
  }
  Free(path);
  Free(filename);
  return group_name;
}

static long processATC(char *atcpath, char *execpath, long *testNumber, IntegrationTest *integrationtest,
                       AutotesterLog *autotesterlog, ResultDocument* resultDocument) {
  long rc = IT_OK;
  bool atc_failure = false;

  xmlDocPtr doc = NULL;
  xmlNode *root_element = NULL;

  char *atcpath_copy = Strdup(atcpath); // copy necessary because dirname() modifies the input string
  char *atcfolder = dirname(atcpath_copy);
  AutotesterLog_setLogFolder(autotesterlog, atcfolder);
  Free(atcpath_copy);

  if (access( atcpath, F_OK ) == -1) {
    mywarn("Can not read the configuration file: \"%s\"", atcpath);
    return IT_EXECUTIONCRASH;
  }
  doc = xmlReadFile(atcpath, NULL, 0);
  if (doc == NULL) {
    mywarn("Unable to parse format of configuration file: \"%s\"", atcpath);
    return IT_EXECUTIONCRASH;
  }
  root_element = xmlDocGetRootElement(doc);

  const char *expectedTagName = "test";
  autotesterlog->tests += xml_count_nodes(root_element, expectedTagName);

  xmlNode *cur_node = NULL;
  for (cur_node = root_element->children; cur_node; cur_node = cur_node->next) {
    if (strcmp((const char*) cur_node->name, expectedTagName) != 0
        || cur_node->type != XML_ELEMENT_NODE) {
      continue; // this is not a child <test> node
    }
    rc = IT_OK;
    atc_failure = false;

    rc = IntegrationTest_init(integrationtest, cur_node, atcpath, autotesterlog, overwrite);
    integrationtest->id = 0 + *testNumber;

    if (integrationtest->outputdir) {
      String_replace(atcpath, &execpath, configpath, ".");
    }
    if (xmloutput) {
      if (atcpath) {
        integrationtest->atc_path = Strdup(atcpath);
      }
      if (singletestfile) {
        integrationtest->atc_group = Strdup("/"); // no real group name since all test come from one file
      } else {
        char *group_name = getGroupName(atcpath, configpath);
        if (group_name) {
          integrationtest->atc_group = group_name;
        }
      }
    }

    if (regex && integrationtest->name) {
      if (!String_matchRegex(integrationtest->name, regex, false)) {
        const char *result = "SKIP";
        print_test_result(autotesterlog->logfile, (char*) result, COLOR_BLUE, *testNumber, integrationtest->name, execpath);
        integrationtest->result = Strdup(result);

        if (execpath) Free(execpath);
        IntegrationTest_toXml(resultDocument->rootelement, integrationtest);
        continue;
      }
    }

    if (rc != IT_OK) {
      const char *result = "ATC_FAIL";
      print_test_result(autotesterlog->logfile, (char*) result, COLOR_RED, *testNumber, integrationtest->name, execpath);
      integrationtest->result = Strdup(result);

      if (execpath) Free(execpath);
      autotesterlog->failedATCs++;

      atc_failure = true;
    }

    // execute
    if (!isdry && !atc_failure) {
      autotesterlog->executedTests++;
      rc = IntegrationTest_execute(integrationtest);
    }


    if (!isdry && !atc_failure) {
      if (rc != IT_OK) {
        autotesterlog->failedTests++;
        if (rc == IT_EXECUTIONCRASH) {
          const char *result = "EXECUTION FAIL";
          print_test_result(autotesterlog->logfile, (char*) result, COLOR_RED, *testNumber, integrationtest->name, execpath);
          integrationtest->result = Strdup(result);

          autotesterlog->failedExecutions++;
        } else if (rc == IT_EXECUTIONTIMEOUT) {
          const char *result = "EXECUTION TIMEOUT";
          print_test_result(autotesterlog->logfile, (char*) result, COLOR_RED, *testNumber, integrationtest->name, execpath);
          integrationtest->result = Strdup(result);

          autotesterlog->failedExecutions++;
        } else if (rc == IT_VALIDATIONFAIL) {
          const char* result = "VALIDATION FAIL";
          print_test_result(autotesterlog->logfile, (char*) result, COLOR_RED, *testNumber, integrationtest->name, execpath);
          integrationtest->result = Strdup(result);

          autotesterlog->failedValidations++;
        } else if (rc == IT_VALIDATIONTIMEOUT) {
          const char* result = "VALIDATION TIMEOUT";
          print_test_result(autotesterlog->logfile, (char*) result, COLOR_RED, *testNumber, integrationtest->name, execpath);
          integrationtest->result = Strdup(result);

          autotesterlog->failedValidations++;
        } else {
          myexit(ERROR_BUG, "Should not happen (Unknown return value %ld).", rc);
        }
      } else {
        const char* result = "PASS";
        print_test_result(autotesterlog->logfile, (char*) result, COLOR_GREEN, *testNumber, integrationtest->name, execpath);
        integrationtest->result = Strdup(result);

        autotesterlog->testsOK++;
      }
    }
    if (execpath) Free(execpath);

    // add result output to the resultDocument
    IntegrationTest_toXml(resultDocument->rootelement, integrationtest);

    // delete files and logs according to options
    if ((deleteonerror && rc != IT_OK) || (!keeponsuccess && rc == IT_OK)) {
      IntegrationTest_deleteOutputFiles(integrationtest);
    }

    IntegrationTest_deinit(integrationtest);
    if (!isdry && rc != IT_OK && stoponerror) {
      fprintf(autotesterlog->logfile, "-----------------------------------------------\n");
      fprintf(autotesterlog->logfile, "Aborting test on first error.\n");
      break;
    }
    (*testNumber)++;
  }
  xmlFreeDoc(doc);

  return rc;
}

int main(int argc, char *argv[]) {
  PACE3DMAIN(argv[0]);

  LIBXML_TEST_VERSION

  long status = IT_OK;

  long   i;
  char **atclist = NULL;
  long   atclist_count;

  char  *execpath = NULL;

  char   cwd[PATH_MAX];

  double systemtime;
  double usertime;
  double totaltime;

  IntegrationTest *integrationtest = Malloc(sizeof(IntegrationTest));
  *integrationtest = (IntegrationTest) {
    .description = NULL,
    .program = NULL,
    .call = NULL
  };
  AutotesterLog   *autotesterlog = Malloc(sizeof(AutotesterLog));
  ResultDocument *resultdocument = Malloc(sizeof(ResultDocument));

  initEnvironment();
  getParams(argc, argv, tool, ARGUMENT(arguments));

  AutotesterLog_init(autotesterlog, logfilename, nolocallog);

  ResultDocument_init(resultdocument, xmloutput, xmloutputfilename);

  if (configpath == NULL) {
    Getcwd(cwd, PATH_MAX);
    configpath = Strdup(cwd);
  }

  printASCII(autotesterlog->logfile);
  fprintf(autotesterlog->logfile, "-----------------------------------------------\n");
  fprintf(autotesterlog->logfile, "Date: %s\n", sprinttime());
  fprintf(autotesterlog->logfile, "Base path: %s\n", configpath);
  fprintf(autotesterlog->logfile, "-----------------------------------------------\n");

  // find atc files
  if (!singletestfile) {
    atclist_count = getFilesFiltered(&atclist, configpath, "^.*[.]atc$", recursive);
    if (atclist == NULL) {
      myexit(ERROR_BUG, "No '.atc' Files have been %sfound in Folders", recursive ? "recursively " : "");
    }
    if (atclist_count == -1) myexit(ERROR_PARAM, "Could not get .atc files.");

    qsort(atclist, atclist_count, sizeof(char**), cmpstring);

    // print list of atc to be executed
    fprintf(autotesterlog->logfile, "ATC files found:\n");
    for (i = 0; i < atclist_count; i++) {
      fprintf(autotesterlog->logfile, "%2ld: %s\n", i, atclist[i]);
    }
    fprintf(autotesterlog->logfile, "-----------------------------------------------\n");

    fprintf(autotesterlog->logfile, "Executing tests:\n");
  } else {
    atclist_count = 1;
    atclist = Malloc(1*sizeof(char*));
    atclist[0] = Strdup(singletestfile);
  }

  enforceSingleAtcPerFolder(atclist, atclist_count);

  systemtime = 0.0;
  usertime   = 0.0;
  totaltime  = 0.0;
  measureCPUTime(&systemtime, &usertime, &totaltime);
  long test_number = 0; // to count tests (atc files can contain multiple tests)
  for (i = 0; i < atclist_count; i++) {
    long returnCode = processATC(atclist[i], execpath, &test_number, integrationtest, autotesterlog, resultdocument);
    if (returnCode > status) {
      status = returnCode;
    }
  }
  measureCPUTime(&systemtime, &usertime, &totaltime);

  int failed = (int) autotesterlog->failedTests;
  autotesterlog->duration = totaltime / 100;
  AutotesterLog_print(autotesterlog);
  ResultDocument_write(resultdocument);

  AutotesterLog_deinit(autotesterlog);
  ResultDocument_deinit(resultdocument);
  deinitEnvironment();

  if (atclist_count)  FreeM(atclist, atclist_count);
  if (singletestfile) Free(singletestfile);
  if (configpath) Free(configpath);
  if (logfilename) Free(logfilename);
  if (regex) Free(regex);
  if (overwrite) Free(overwrite);
  Free(autotesterlog);
  Free(resultdocument);
  Free(integrationtest);
  xmlCleanupParser();

  return failed;
}
