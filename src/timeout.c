/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <stddef.h>
#include <libxml/tree.h>
#include <wrapper.h>
#include <container/stringconv.h>
#include "xmlhelper.h"
#include "xmldef.h"
#include "timeout.h"

void Timeout_deinit(Timeout* timeout) {
  if (timeout->time_unit != NULL) Free(timeout->time_unit);
}

Timeout* Timeout_parse(xmlNodePtr parent_node) {
  Timeout *result = NULL;
  if (parent_node != NULL) {
    int parsing_result = XML_RESULT_NOT_SET;
    xmlNodePtr timeout_node = xml_find_node(parent_node, "timeout", &parsing_result);

    if (timeout_node != NULL) {
      // parse max_time first, proceed only on success
      float max_time = 0;
      xml_node_content_float(parent_node, "timeout", &parsing_result, &max_time);
      if (parsing_result == XML_TYPE_ERROR) {
        mywarn("Warning: Could not parse max time (float) of the specified"
               " timeout configuration. Timeout not checked!");
        return NULL;
      }

      result = Malloc(sizeof(Timeout));
      *result = (Timeout) {
        .max_time = max_time,
        .kill_signal = DEFAULT_TIMEOUT_KILL_SIGNAL,
        .time_unit = DEFAULT_TIMEOUT_TIME_UNIT
      };

      result->time_unit = xml_attribute(timeout_node, "unit", &parsing_result);
      xml_attribute_long(timeout_node, "kill-signal", &parsing_result, &result->kill_signal);
      if (parsing_result == XML_TYPE_ERROR) {
        mywarn("Warning: Could not parse attribute kill-signal of the specified"
               " timeout configuration. Falling back to default value.");
      }
    }
    xmlFreeNode(timeout_node);
  }
  return result;
}

bool Timeout_occurred(Timeout* timeout, int return_code) {

  if (!timeout) {
    return false;
  }
  timeout->has_occurred = (return_code == 137 && timeout->kill_signal == 9)
      || (return_code == 124 && timeout->kill_signal != 9);
  return timeout->has_occurred;
}

void Timeout_toXml(Timeout* timeout, xmlNodePtr parent_node) {
  if (timeout != NULL && parent_node != NULL) {
    xmlNodePtr timeout_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "timeout");
    xmlAddChild(parent_node, timeout_node);
    if (timeout->time_unit && strlen(timeout->time_unit) > 0) {
      xmlSetProp(timeout_node, BAD_CAST "unit", BAD_CAST timeout->time_unit);
    } else {
      xmlSetProp(timeout_node, BAD_CAST "unit", BAD_CAST DEFAULT_TIMEOUT_TIME_UNIT);
    }
    char *kill_signal = NULL;
    String_createFormatedString(&kill_signal, "%ld", timeout->kill_signal);
    xmlSetProp(timeout_node, BAD_CAST "kill-signal", BAD_CAST kill_signal);
    Free(kill_signal);
    char *max_time = NULL;
    String_createFormatedString(&max_time, "%f", timeout->max_time);
    xmlNodeSetContent(timeout_node, BAD_CAST max_time);
    Free(max_time);
  }
}
