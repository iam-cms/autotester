/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_VALIDATION_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_VALIDATION_H

#include "call.h"
#include "variablescope.h"

#define VAL_NOT_SET          0
#define VAL_PASS             1
#define VAL_FAIL             2
#define VAL_TIMEOUT          3

struct validation_s;

typedef long (*func_init)     (struct validation_s* validationarguments, xmlNodePtr node, long index, FILE* validationlog, char* outputpath, VariableScope* parent_scope);
typedef long (*func_validate) (struct validation_s* validationarguments, FILE* validationlog, char* outputlog_name, double* systemtime, double* usertime, double* totaltime);
typedef void (*func_deinit)   (struct validation_s* validationarguments);


typedef struct validation_external_s {
  Call *call;
} ValidationExternal;

typedef struct validation_s {
  long           validation_type;
  long           validation_id;

  Description   *description;

  double         systemtime;
  double         usertime;
  double         totaltime;

  char          *outputpath;
  char          *validation_type_name;
  func_init      init;
  func_validate  validate;
  func_deinit    deinit;

  union {
    ValidationExternal external;
    // other validation types can be added here
  } arguments;

  VariableScope  *variable_scope;
  long result;
} Validation;

void Validation_deinitExternal(Validation* validation);
long
Validation_initExternal(Validation *validation, xmlNodePtr node, long index, FILE *validationlog, char *outputpath,
                        VariableScope *parent_scope);
long Validation_validateExternal(Validation* validation, FILE* validationlog, char* outputlog_name, double* systemtime, double* usertime, double* totaltime);

// add a result to the xml tree of the resultdocument
void Validation_toXml(Validation* validation, xmlNodePtr parent_node);

#endif
