/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_VARIABLESCOPE_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_VARIABLESCOPE_H

#include <libxml/tree.h>

struct integrationtest_s;
typedef struct variableScope_s {
  struct variableScope_s         *parent_scope;
  xmlNodePtr                      node;
  struct integrationtest_s       *integrationtest;
} VariableScope;

long VariableScope_replaceVariables(VariableScope* variable_scope, char** rawstring);
void VariableScope_deinit(VariableScope* variable_scope);

#endif
