/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include <container/stringconv.h>
#include "variablescope.h"
#include "xmlhelper.h"

static void warnMissingVariable(char* variablename) {
  mywarn("Variable '%s' could not be resolved in the current scope", variablename);
}

static bool VariableScope_lookUpVariable(VariableScope* variable_scope, char** resultbuffer, char* variablename) {

  const long environment_count = sizeof(environment)/sizeof(Environment);

  // look up in environment
  for (int i = 0; i < environment_count; i++) {

    if (environment[i].value == NULL || strlen(environment[i].value) == 0) {
      mywarn("Missing value for environment key %d", i);
      continue;
    }

    size_t variablelength = strlen(variablename);
    if (strncmp(environment[i].name, variablename, variablelength) == 0) {
      if (i == COMPARE_PATH_INDEX || i == INPUTDATA_PATH_INDEX || i == OUTPUT_PATH_INDEX) {
        // this is one of the environment variables which holds a folder name which must be relative
        // to the test's base path
        char *value = NULL;
        if (variable_scope->integrationtest && variable_scope->integrationtest->base_path) {
          value = Strdup(variable_scope->integrationtest->base_path);
        }
        String_append(&value, environment[i].value);
        *resultbuffer = Strdup(value);
        Free(value);
      } else {
        *resultbuffer = Strdup(environment[i].value);
      }
      return true;
    }
  }

  if (variable_scope == NULL) {
    warnMissingVariable(variablename);
    return false;
  }

  // to resolve the special variable $program$
  if (variable_scope->integrationtest != NULL && strcmp(variablename, BINARY_VARIABLE_NAME) == 0) {
    if (variable_scope->integrationtest->program->path != NULL) {
      *resultbuffer = Strdup(variable_scope->integrationtest->program->path);
      return true;
    } else {
      warnMissingVariable(variablename);
      return false;
    }
  }

  if (variable_scope->node != NULL) {
    char *variablevalue = parseVariableValue(variable_scope->node, variablename);
    if (variablevalue != NULL) {
      *resultbuffer = variablevalue;
      return true;
    }
  }

  if (variable_scope->parent_scope != NULL) {
    return VariableScope_lookUpVariable(variable_scope->parent_scope, resultbuffer, variablename);
  }

  warnMissingVariable(variablename);
  return false;
}

static long VariableScope_expandVariable(VariableScope *variable_scope, long *readpos, char **buffer, char *rawstring) {
  char *match;
  char  variablename[MAX_LINE_LENGTH];
  long  variablelength;
  char *start;
  long  rc;

  // search for next '$'
  start = &rawstring[*readpos] + 1; // jump over first '$'
  match = strchr(start, '$');
  if (match == NULL) {
    mywarn("Did not find second '$' in raw string.");
    return false;
  }

  // extract variablename
  variablelength = match-start;
  strncpy(variablename, start, variablelength);
  variablename[variablelength] = '\0';

  // move readpos
  *readpos += variablelength + 2; // add 2 for the '$'

  // look up
  rc = VariableScope_lookUpVariable(variable_scope, buffer, variablename);

  return rc;
}

long VariableScope_replaceVariables(VariableScope* variable_scope, char** rawstring) {
  if (!variable_scope || !rawstring || !*rawstring) {
    return false;
  }
  long   readpos;
  long   writepos;

  long   hadvariable;
  long   loopcount;
  long   maxloops = 5;

  char  *expandedvariable = NULL;
  size_t   expandedvariable_length;
  char   expandedstring[MAX_LINE_LENGTH];
  char  *workstring;

  // do at most `maxloops` iterations in variable replacement
  for (loopcount = 0; loopcount < maxloops; loopcount++) {
    workstring = *rawstring;
    readpos     = 0;
    writepos    = 0;
    hadvariable = false;
    while (workstring[readpos] != '\0') {
      if (workstring[readpos] == '$') {

        // expand a found '$'
        if (!VariableScope_expandVariable(variable_scope, &readpos, &expandedvariable, workstring)) {
          return false;
        } else {
          hadvariable = true;
        }

        // space left
        expandedvariable_length = strlen(expandedvariable);
        if (expandedvariable_length + writepos +1 > MAX_LINE_LENGTH) {
          mywarn("Testcase is broken. Variable expansion lead to too long string.");
          return false;
        }

        // copy string
        memcpy(&expandedstring[writepos], expandedvariable, expandedvariable_length);
        writepos += expandedvariable_length;

        if (expandedvariable) {
          Free(expandedvariable);
          expandedvariable = NULL;
        }
      } else {
        expandedstring[writepos] = workstring[readpos];
        writepos++;
        readpos++;
      }
    }
    expandedstring[writepos] = '\0';

    // replace original string
    Free(*rawstring);
    *rawstring = Strdup(expandedstring);

    // all done
    if (!hadvariable) break;
  }

  // endless replacing
  if (hadvariable) {
    mywarn("Testcase is broken. Variable expansion did not finish.\n"
           "No terminal found in %ld replacement iterations for string: '%s'.\n"
           "Did you creat variables inside other variables?", maxloops, *rawstring);
    return false;
  }
  return true;
}

void VariableScope_deinit(VariableScope* variable_scope) {
  if (variable_scope->node) xmlFreeNode(variable_scope->node);
}
