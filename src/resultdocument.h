/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <libxml/tree.h>

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_RESULTDOCUMENT_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_RESULTDOCUMENT_H

struct integrationtest_s;
struct validation_s;

typedef struct resultDocument_s {
  bool        enabled;

  const char *outputfile_name;
  xmlDocPtr   xmlDoc;
  xmlNodePtr  rootelement;
} ResultDocument;

void ResultDocument_init(ResultDocument* resultdocument, bool enabled, const char* outputfile_name);
void ResultDocument_deinit(ResultDocument* resultdocument);
void ResultDocument_write(ResultDocument* resultdocument);

#endif
