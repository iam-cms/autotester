/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_CALL_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_CALL_H

#include <libxml/tree.h>
#include "timeout.h"

#define CALL_RESULT_NOT_SET  0
#define CALL_RESULT_TIMEOUT  1
#define CALL_RESULT_FINISHED 2

typedef struct call_s {
  long     expected_return_code;
  char    *command;
  Timeout *timeout;

  // results
  long     result;
  long     actual_return_code;
} Call;

Call* Call_parse(xmlNodePtr parent_node);
long Call_execute(Call* call, double* systemtime, double* usertime, double* totaltime, char* outputlog_name);
void Call_deinit(Call* call);
void Call_toXml(Call* call, xmlNodePtr parent_node);

#endif //POSTPROCESSING_DEBUG_CALL_H
