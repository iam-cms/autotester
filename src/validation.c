/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include <container/stringconv.h>
#include "call.h"
#include "xmlhelper.h"
#include "xmldef.h"
#include "validation.h"


long
Validation_initExternal(Validation *validation, xmlNodePtr node, long index, FILE *validationlog, char *outputpath,
                        VariableScope *parent_scope) {
  long rc = true;

  validation->validation_id = index,
  validation->outputpath = Strdup(outputpath);

  validation->arguments.external.call = Call_parse(node);

  // set variable scope
  int parsing_result = XML_RESULT_NOT_SET;
  xmlNodePtr variable_node = xml_find_node(node, "variables", &parsing_result);
  validation->variable_scope = Malloc(sizeof(VariableScope));
  *validation->variable_scope = (VariableScope) {
    .node = variable_node,
    .parent_scope = parent_scope,
    .integrationtest = parent_scope->integrationtest
  };

  validation->description = Description_parse(node);

  char *namespace = NULL;
  String_createFormatedString(&namespace, "validation.%ld.", index);
  if (validation->arguments.external.call != NULL &&
      !VariableScope_replaceVariables(validation->variable_scope, &validation->arguments.external.call->command)) {
    mywarn("Testfile corrupt. Could not evaluate call expression of validation %ld", index);
    fprintf(validationlog, "Testfile corrupt. Could not evaluate validation call expression of validation %ld\n", index);
    rc = false;
  }
  Free(namespace);
  return rc;
}


void Validation_deinitExternal(Validation* validation) {
  if (validation->arguments.external.call) {
    Call_deinit(validation->arguments.external.call);
    Free(validation->arguments.external.call);
  }
  if (validation->description) {
    Description_deinit(validation->description);
    Free(validation->description);
  }
  if (validation->outputpath) Free(validation->outputpath);
  if (validation->validation_type_name) Free(validation->validation_type_name);
  if (validation->variable_scope) {
    VariableScope_deinit(validation->variable_scope);
    Free(validation->variable_scope);
  }
}


long Validation_validateExternal (Validation* validation, FILE* validationlog, char* outputlog_name, double* systemtime, double* usertime, double* totaltime) {
  char *outputStr;
  long rc = true;
  long iscall;


  outputStr = NULL;

  fprintf(validationlog, "[Validation ID         ]:       %ld\n"   , validation->validation_id);
  fprintf(validationlog, "[Validationtype        ]:       external\n");
  fprintf(validationlog, "[Call                  ]:       %s\n", validation->arguments.external.call->command);
  fprintf(validationlog, "-----------------------------------------------\n");

  outputStr = Strdup(validation->arguments.external.call->command);

  *systemtime = 0.0;
  *usertime   = 0.0;
  *totaltime  = 0.0;

  iscall = Call_execute(validation->arguments.external.call, systemtime, usertime, totaltime, outputlog_name);

  fprintf(validationlog, "Call:\n");
  fprintf(validationlog, "'%s'\n", outputStr);
  fprintf(validationlog, "Computation time: %s\n", makehumanreadabletime(*totaltime / 100));

  if (Timeout_occurred(validation->arguments.external.call->timeout, (int) iscall)) {
    fprintf(validationlog, "Test timed out.\n");
    rc = VAL_TIMEOUT;
  } else if (iscall == validation->arguments.external.call->expected_return_code) {
    fprintf(validationlog, "Test ok.\n");
    rc = VAL_PASS;
  } else {
    fprintf (validationlog, "Test failed. Expected return code %ld, got %ld.\n", validation->arguments.external.call->expected_return_code, validation->arguments.external.call->actual_return_code);
    rc = VAL_FAIL;
  }

  validation->result = rc;

  // validate
  fprintf(validationlog, "-----------------------------------------------\n\n");

  Free(outputStr);
  return rc;
}

void Validation_toXml(Validation* validation, xmlNodePtr parent_node) {
  if (parent_node != NULL && validation != NULL) {
    xmlNodePtr validation_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "validation");
    xmlAddChild(parent_node, validation_node);

    char *id_str = NULL;
    String_createFormatedString(&id_str, "%ld", validation->validation_id);
    xmlSetProp(validation_node, BAD_CAST "id", BAD_CAST id_str);
    Free(id_str);

    xmlNodePtr type_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "type");
    xmlNodeSetContent(type_node, BAD_CAST validation->validation_type_name);
    xmlAddChild(validation_node, type_node);

    Description_toXml(validation->description, validation_node);

    if (validation->validation_type == VALIDATION_EXTERNAL) {
      Call_toXml(validation->arguments.external.call, validation_node);
    }
    xmlNodePtr status_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "status");
    xmlAddChild(validation_node, status_node);

    char *result_str = NULL;
    if (validation->result == VAL_PASS) {
      result_str = "PASS";
    } else if (validation->result == VAL_FAIL) {
      result_str = "FAIL";
    } else if (validation->result == VAL_TIMEOUT) {
      result_str = "TIMEOUT";
    } else {
      result_str = "SKIP";
    }
    xmlNodeSetContent(status_node, BAD_CAST result_str);
  }
}
