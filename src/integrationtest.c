/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <dirent.h>
#include <pwd.h>

#include <libxml/tree.h>
#include <wrapper.h>
#include <container/stringconv.h>
#include <fileop.h>

#include "xmldef.h"
#include "autotesterlogfile.h"
#include "xmlhelper.h"
#include "resultdocument.h"

#include "integrationtest.h"

Environment environment[5];
const long environment_count = sizeof(environment)/sizeof(Environment);


void IntegrationTest_deleteOutputFiles(IntegrationTest *integrationtest) {
  DIR *outputdir;

  if (integrationtest->outputdir) {
    outputdir = opendir(integrationtest->outputdir);
    if (outputdir) {
      closedir(outputdir);
      Remove(integrationtest->outputdir);
    }
  }
}

void initEnvironment() {
  // setup environment with default values
  Environment *environmentPtr = environment;
  environmentPtr[COMPARE_PATH_INDEX].name  = "COMPARE_PATH";
  environmentPtr[INPUTDATA_PATH_INDEX].name  = "INPUTDATA_PATH";
  environmentPtr[OUTPUT_PATH_INDEX].name  = "OUTPUT_PATH";
  environmentPtr[BINARY_PATH_INDEX].name  = "BINARY_PATH";
  environmentPtr[VALIDATION_BINARY_PATH_INDEX].name  = "VALIDATION_BINARY_PATH";

  xmlDocPtr doc = NULL;
  char configPath[MAX_LINE_LENGTH];
  struct passwd *pw = getpwuid(getuid());
  snprintf(configPath, MAX_LINE_LENGTH, "%s/%s", pw->pw_dir, CONFIG_FILE);

  if (access( configPath, F_OK ) != -1) {
    doc = xmlReadFile(configPath, NULL, 0);
  }

  if (doc != NULL) {
    xmlNode *environment_node = NULL;
    environment_node = xmlDocGetRootElement(doc);

    environmentPtr[COMPARE_PATH_INDEX].value = parseVariableValue(environment_node, "compare_path");
    environmentPtr[INPUTDATA_PATH_INDEX].value = parseVariableValue(environment_node, "inputdata_path");
    environmentPtr[OUTPUT_PATH_INDEX].value = parseVariableValue(environment_node, "output_path");
    environmentPtr[BINARY_PATH_INDEX].value = parseVariableValue(environment_node, "binary_path");
    environmentPtr[VALIDATION_BINARY_PATH_INDEX].value = parseVariableValue(environment_node, "validation_binary_path");

    xmlFreeDoc(doc);
  }

  // set default values if necessary
  if (!environmentPtr[COMPARE_PATH_INDEX].value) environmentPtr[COMPARE_PATH_INDEX].value = Strdup("compare");
  if (!environmentPtr[INPUTDATA_PATH_INDEX].value) environmentPtr[INPUTDATA_PATH_INDEX].value = Strdup("input");
  if (!environmentPtr[OUTPUT_PATH_INDEX].value) environmentPtr[OUTPUT_PATH_INDEX].value = Strdup("output");
  if (!environmentPtr[BINARY_PATH_INDEX].value) environmentPtr[BINARY_PATH_INDEX].value = Strdup("/usr/bin");
  if (!environmentPtr[VALIDATION_BINARY_PATH_INDEX].value) environmentPtr[VALIDATION_BINARY_PATH_INDEX].value = Strdup("/usr/bin");
}

// deinit environment
void deinitEnvironment(void)  {
  long i;

  for (i = 0; i < environment_count; i++) {
    if (environment[i].value) Free(environment[i].value);
  }
}



long IntegrationTest_validate(IntegrationTest* integrationtest) {
  long i;

  long rc = VAL_PASS;
  for (i = 0; i < integrationtest->validationcount; i++) {
    Validation *validation = &integrationtest->validations[i];
    long validation_result = validation->validate(validation, integrationtest->autotesterlog->validationlog,
                                                  integrationtest->autotesterlog->outputlog_name,
                                                  &validation->systemtime,
                                                  &validation->usertime, &validation->totaltime);

    // use the worst occurred result as return code
    if (validation_result == VAL_FAIL) {
      rc = VAL_FAIL;
    } else if (validation_result == VAL_TIMEOUT && rc != VAL_FAIL) {
      rc = VAL_TIMEOUT;
    }
  }
  return rc;
}


static bool IntegrationTest_initValidations(IntegrationTest* integrationtest, xmlNodePtr parent_node) {
  long i = 0;
  long result = true;
  char *validation_type_name;

  if (parent_node == NULL) {
    return false;
  }
  // reset
  // allocate objects
  integrationtest->validations = Calloc(integrationtest->validationcount, sizeof(Validation));
  // create all validation objects
  xmlNodePtr cur_node = NULL;
  for (cur_node = parent_node->children; cur_node != NULL; cur_node = cur_node->next) {
    if (strcmp((const char*) cur_node->name, "validation") != 0
        || cur_node->type != XML_ELEMENT_NODE) {
      continue; // this is not a child <validation> node
    }

    // get type
    int parsing_result = XML_RESULT_NOT_FOUND;
    validation_type_name = xml_attribute(cur_node, "type", &parsing_result);
    if (validation_type_name == NULL) {
      validation_type_name = Strdup("external");
    }


    long validation_type;
    func_init f_init = 0;
    func_validate f_validate = 0;
    func_deinit f_deinit = 0;

    // init validation objects
    if (strcmp(validation_type_name, "external") == 0) {
      f_init = Validation_initExternal;
      f_validate = Validation_validateExternal;
      f_deinit = Validation_deinitExternal;
      validation_type = VALIDATION_EXTERNAL;
    } else {
      myexit(ERROR_BUG, "Unsupported validation type %s for validation %ld", validation_type_name, i);
    }
    integrationtest->validations[i] = (Validation) {
      .validation_id = i,
      .init     = f_init,
      .validate = f_validate,
      .deinit   = f_deinit,
      .validation_type = validation_type,
      .validation_type_name = validation_type_name
    };

    // init validation
    if (!integrationtest->validations[i].init(&integrationtest->validations[i], cur_node, i,
                                              integrationtest->autotesterlog->validationlog,
                                              integrationtest->outputdir, integrationtest->variable_scope)) {
      mywarn("Testfile corrupt.");
      result = false;
    }

    i++;
  }
  return result;
}

// Execute the Binary
long IntegrationTest_execute(IntegrationTest* integrationtest) {
  long rc = IT_OK;
  int  rc_mkdir;

  // create outputfolder
  rc_mkdir = mkdir(integrationtest->outputdir, S_IRWXU);
  if (rc_mkdir == -1) {
    if (errno == EEXIST) {
      myverbose(VERBOSE_CONTROL, verbose, "outputfolder already existed: '%s'", integrationtest->outputdir);
    } else {
      mywarn("Could not create outputfolder: '%s'", integrationtest->outputdir);
      return IT_EXECUTIONCRASH;
    }
  }
   if (strlen(integrationtest->call->command) == 0) {
    fprintf(integrationtest->autotesterlog->executionlog, "Teststring is empty!");
    return false;
  }


  integrationtest->systemtime = 0.0;
  integrationtest->usertime   = 0.0;
  integrationtest->totaltime  = 0.0;

  Call_execute(integrationtest->call, &integrationtest->systemtime, &integrationtest->usertime, &integrationtest->totaltime, integrationtest->autotesterlog->outputlog_name);

  if (integrationtest->call->result == CALL_RESULT_TIMEOUT) {
    fprintf(integrationtest->autotesterlog->executionlog, "Test failed: timeout exceeded\n");
    rc = IT_EXECUTIONTIMEOUT;
  } else if (integrationtest->call->actual_return_code != integrationtest->call->expected_return_code) {
    fprintf(integrationtest->autotesterlog->executionlog, "Test failed \n");
    rc = IT_EXECUTIONCRASH;
  }
  fprintf(integrationtest->autotesterlog->executionlog, "Call:\n");
  fprintf(integrationtest->autotesterlog->executionlog, "'%s'\n", integrationtest->call->command);
  fprintf(integrationtest->autotesterlog->executionlog, "Expected return code: %ld\n", integrationtest->call->expected_return_code);
  fprintf(integrationtest->autotesterlog->executionlog, "Actual return code: %ld\n", integrationtest->call->actual_return_code);
  fprintf(integrationtest->autotesterlog->executionlog, "CPU time: %s\n", makehumanreadabletime(integrationtest->totaltime / 100));

  // validate only if nothing went wrong so far
  if (rc == IT_OK) {
    long validation_result = IntegrationTest_validate(integrationtest);
    if (validation_result == VAL_FAIL) {
      rc = IT_VALIDATIONFAIL;
    } else if (validation_result == VAL_TIMEOUT) {
      rc = IT_VALIDATIONTIMEOUT;
    }
  }

  fprintf(integrationtest->autotesterlog->executionlog, "-----------------------------------------------\n");
  return rc;
}

void IntegrationTest_deinit(IntegrationTest *integrationtest) {
  long i;

  if (integrationtest->description) {
    Description_deinit(integrationtest->description);
    Free(integrationtest->description);
  }
  if (integrationtest->program) {
    Program_deinit(integrationtest->program);
    Free(integrationtest->program);
  }
  if (integrationtest->call) {
    Call_deinit(integrationtest->call);
    Free(integrationtest->call);
  }
  if (integrationtest->result) Free(integrationtest->result);
  if (integrationtest->atc_path) Free(integrationtest->atc_path);
  if (integrationtest->atc_group) Free(integrationtest->atc_group);
  if (integrationtest->group) Free(integrationtest->group);
  if (integrationtest->base_path) Free(integrationtest->base_path);
  if (integrationtest->outputdir) Free(integrationtest->outputdir);

  if (integrationtest->name) Free(integrationtest->name);
  if (integrationtest->validations) {
    for (i = 0; i < integrationtest->validationcount; i++) {
      if (&integrationtest->validations[i]) integrationtest->validations[i].deinit(&integrationtest->validations[i]);
    }
  }
  if (integrationtest->validationcount > 0 && integrationtest->validations) Free(integrationtest->validations);

  if (integrationtest->variable_scope) {
    VariableScope_deinit(integrationtest->variable_scope);
    Free(integrationtest->variable_scope);
  }
}

long IntegrationTest_init(IntegrationTest *integrationtest, xmlNodePtr integrationtest_node, char *atcfilename,
                          AutotesterLog *autotesterlog, char *overwrite) {
  long  rc = IT_OK;

  char *filename = NULL;
  char *path     = NULL;

  // set testpath to integrationtest testpath

  // open local log file
  resolvePath(&path, &filename, atcfilename);

  integrationtest->base_path = Strdup(path);
  integrationtest->outputdir = Strdup(path);
  String_append(&integrationtest->outputdir, environment[OUTPUT_PATH_INDEX].value);
  integrationtest->autotesterlog = autotesterlog;

  // former description key is now taken from the "name" attribute of each <test> tag
  int parsing_result = XML_RESULT_NOT_SET;
  integrationtest->name = xml_attribute(integrationtest_node, "name", &parsing_result);
  if (integrationtest->name == NULL) {
    mywarn("Testfile description is missing");
    fprintf(integrationtest->autotesterlog->executionlog, "Testfile description is missing \n");
    integrationtest->name = Strdup("");
  }
  integrationtest->group = xml_attribute(integrationtest_node, "group", &parsing_result);

  integrationtest->description = Description_parse(integrationtest_node);

  integrationtest->program = Program_parse(integrationtest_node);
  integrationtest->call = Call_parse(integrationtest_node);
  if (integrationtest->call == NULL) {
    xml_error_node_missing("call", integrationtest->autotesterlog->executionlog, integrationtest_node, atcfilename);
    rc = IT_EXECUTIONCRASH;
  } else if (integrationtest->call->command == NULL) {
    xml_error_node_missing("call/command", integrationtest->autotesterlog->executionlog, integrationtest_node,
                           atcfilename);
    rc = IT_EXECUTIONCRASH;
  } else {
    xmlNodePtr variable_node = xml_find_node(integrationtest_node, "variables", &parsing_result);
    integrationtest->variable_scope = Malloc(sizeof(VariableScope));
    *integrationtest->variable_scope = (VariableScope) {
      .node = variable_node,
      .integrationtest = integrationtest
    };

    if (!VariableScope_replaceVariables(integrationtest->variable_scope, &integrationtest->call->command)) {
      mywarn("Testfile corrupt. Could not expand variables in key 'call'");
      fprintf(integrationtest->autotesterlog->executionlog, "Testfile corrupt. Could not expand variables in key 'call'\n");
      rc = IT_EXECUTIONCRASH;
    }
  }

  if (rc != IT_EXECUTIONCRASH && rc != IT_EXECUTIONTIMEOUT) {

    if (overwrite) {
      String_append(&integrationtest->call->command, " ");
      String_append(&integrationtest->call->command, overwrite);
    }

    xmlNodePtr validations_node = xml_find_node(integrationtest_node, "validations", &parsing_result);
    integrationtest->validationcount = xml_count_nodes(validations_node, "validation");

    fprintf(integrationtest->autotesterlog->executionlog, "\n-----------------------------------------------\n");
    fprintf(integrationtest->autotesterlog->executionlog, "[ID             ]: %ld\n",  integrationtest->id);
    fprintf(integrationtest->autotesterlog->executionlog, "[Name           ]: %s\n",   integrationtest->name);
    if (integrationtest->program != NULL && integrationtest->program->path != NULL) {
      fprintf(integrationtest->autotesterlog->executionlog, "[Program        ]: %s\n", integrationtest->program->path);
    }
    fprintf(integrationtest->autotesterlog->executionlog, "[Command        ]: %s\n",   integrationtest->call->command);
    fprintf(integrationtest->autotesterlog->executionlog, "[OUTPUTDIR      ]: %s\n",   integrationtest->outputdir);
    fprintf(integrationtest->autotesterlog->executionlog, "[VALIDATIONS    ]: %ld\n",  integrationtest->validationcount);
    fprintf(integrationtest->autotesterlog->executionlog, "-----------------------------------------------\n");


    if (validations_node != NULL) {
      bool success = IntegrationTest_initValidations(integrationtest, validations_node);
      if (!success) {
        mywarn("Testfile corrupt. Could not initialize validations.");
        fprintf(integrationtest->autotesterlog->executionlog, "Testfile corrupt. Could not initialize validations.\n");
        rc = IT_EXECUTIONCRASH;
      }
      xmlFreeNode(validations_node);
    }
  }
  if (filename) Free(filename);
  if (path) Free(path);
  return rc;
}




void
IntegrationTest_toXml(xmlNodePtr parent, struct integrationtest_s* integrationtest) {
  if (parent != NULL && integrationtest != NULL) {
    xmlNodePtr testNode = xmlNewNode(XML_NAMESPACE, BAD_CAST "test");
    xmlAddChild(parent, testNode);
    char *id_str = NULL;
    String_createFormatedString(&id_str, "%ld", integrationtest->id);
    xmlSetProp(testNode, BAD_CAST "id", BAD_CAST id_str);
    Free(id_str);
    if (integrationtest->name) {
      xmlSetProp(testNode, BAD_CAST "name", BAD_CAST integrationtest->name);
    }
    if (integrationtest->atc_path) {
      xmlSetProp(testNode, BAD_CAST "atc", BAD_CAST integrationtest->atc_path);
    }
    if (integrationtest->group) {
      xmlSetProp(testNode, BAD_CAST "group", BAD_CAST integrationtest->group);
    } else if (integrationtest->atc_group) {
      // fall back to the atc_group which is basically the path to the atc file from the atc root
      xmlSetProp(testNode, BAD_CAST "group", BAD_CAST integrationtest->atc_group);
    }

    Description_toXml(integrationtest->description, testNode);
    Program_toXml(integrationtest->program, testNode);
    Call_toXml(integrationtest->call, testNode);

    if (integrationtest->validationcount > 0 && strcmp(integrationtest->result, "ATC FAIL") != 0 &&
        strcmp(integrationtest->result, "EXECUTION FAIL") != 0 &&
        strcmp(integrationtest->result, "EXECUTION TIMEOUT") != 0) {
      xmlNodePtr validations_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "validations");
      xmlAddChild(testNode, validations_node);
      for (int i = 0; i < integrationtest->validationcount; i++) {
        Validation_toXml(&integrationtest->validations[i], validations_node);
      }
    }

    xmlNodePtr status_node = xmlNewNode(XML_NAMESPACE, BAD_CAST "status");
    xmlAddChild(testNode, status_node);
    xmlNodeSetContent(status_node, BAD_CAST integrationtest->result);
  }
}
