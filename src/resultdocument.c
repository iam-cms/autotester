/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <stdlib.h>
#include <wrapper.h>
#include "resultdocument.h"
#include "xmldef.h"

void ResultDocument_init(ResultDocument* resultdocument, bool enabled, const char* outputfile_name) {
  *resultdocument = (ResultDocument) {
    .enabled = enabled,
    .outputfile_name = outputfile_name ? outputfile_name : "output.xml"
  };
  if (resultdocument->enabled) {
    resultdocument->xmlDoc = xmlNewDoc(BAD_CAST XML_VERSION);
    resultdocument->rootelement = xmlNewNode(NULL, BAD_CAST "autotester-results");
    xmlDocSetRootElement(resultdocument->xmlDoc, resultdocument->rootelement);
  }
}

void ResultDocument_deinit(ResultDocument* resultdocument) {
  if (resultdocument->xmlDoc != NULL) xmlFreeDoc(resultdocument->xmlDoc);
}

void ResultDocument_write(ResultDocument* resultdocument) {
  if (resultdocument != NULL && resultdocument->enabled) {
    xmlChar *xml_buffer;
    int buffer_size;
    FILE *file;

    xmlDocDumpFormatMemoryEnc(resultdocument->xmlDoc, &xml_buffer, &buffer_size, "UTF-8", 1);

    file = FOpen(resultdocument->outputfile_name, "w");
    fputs((char*) xml_buffer, file);
    Free(xml_buffer);
    FClose(file);
  }
}
