/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_AUTOTESTER_SRC_AUTOTESTERLOGFILE_H
#define POSTPROCESSING_DEBUG_AUTOTESTER_SRC_AUTOTESTERLOGFILE_H

#include <stdbool.h>

typedef struct autotesterlog_s {
  long tests;
  long executedTests;
  long failedTests;
  long testsOK;
  long failedExecutions;
  long failedValidations;
  long failedATCs;
  double duration;

  bool nolocallog;

  FILE *logfile;

  char *logpath;
  char *executionlog_name;
  FILE *executionlog;
  char *validationlog_name;
  FILE *validationlog;
  char *outputlog_name;
  FILE *outputlog;

  FILE *xmloutputfile;

} AutotesterLog;

void AutotesterLog_init(AutotesterLog *autotesterlog, char *logfilename, bool nolocallog);
void AutotesterLog_deinit(AutotesterLog* autotesterlog);
void AutotesterLog_deinitLogFiles(AutotesterLog* autotesterlog);
void AutotesterLog_print(AutotesterLog* autotesterlog);
void AutotesterLog_setLogFolder(AutotesterLog* autotesterlog, char* logfolder);
void AutotesterLog_deleteLogs(AutotesterLog* autotesterlog);

#endif
