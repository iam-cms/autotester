**Autotester** is a tool for automated black-box testing developed at the `Institute for Applied Materials (IAM-CMS) <https://www.iam.kit.edu/cms/english/>`__ of the `Karlsruhe Institute of Technology (KIT) <https://www.kit.edu/english/>`__.

The Source Code can be found `on the OpenCarp Gitlab Instance <https://git.opencarp.org/iam-cms/autotester>`__.

Copyright (C) 2019 IAM-CMS, licensed under the `GPLv3 <https://git.opencarp.org/iam-cms/autotester/-/blob/master/LICENSE>`__.
