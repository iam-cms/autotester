.. Autotester documentation master file, created by
   sphinx-quickstart on Mon Jul 26 10:15:16 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Autotester's documentation!
======================================

.. include:: intro.rst

.. toctree::
   :name: autotester
   :caption: About Autotester
   :maxdepth: 1

   about/motivation
   about/license
   about/authors
   about/funding

.. toctree::
   :name: setup
   :caption: Setup
   :maxdepth: 1

   setup/dependencies
   setup/building

.. toctree::
   :name: documentation
   :caption: Autotester Documentation
   :maxdepth: 2

   autotester/usage/create_tests.rst
   autotester/usage/run
   autotester/usage/run_docker
   autotester/usage/results
   autotester/usage/generate
   autotester/processing
