Build from Source
----------------------------

Build using make
~~~~~~~~~~~~~~~~

.. code:: bash

   make autotester

Build with debugging enabled:

.. code:: bash

   make autotester DEBUG=true

Build using cmake
~~~~~~~~~~~~~~~~~

.. code:: bash

   mkdir build/
   cmake -B build/
   make -C build/
