Dependencies
------------

Build dependencies
~~~~~~~~~~~~~~~~~~~

- make
- cmake (optional)
- gcc
- libxml2-dev

Build dependencies can be installed with:

.. code:: bash

   apt install make cmake gcc libxml2-dev

On systems using the RPM package manager, they can be installed with:

.. code:: bash

   yum install make cmake gcc libxml2-devel findutils

Runtime dependencies
~~~~~~~~~~~~~~~~~~~~~

- libxml2
- pandoc (when using markdown in test descriptions)
- libxml2-utils (necessary to run the example tests)

Runtime dependencies can be installed with:

.. code:: bash

   apt install pandoc libxml2-utils

On systems using the RPM package manager, they can be installed with:

.. code:: bash

   yum install pandoc libxml2
