Running Autotester using docker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::

  When using Autotester in a gitlab CI runner use the specialized image
  from
  `docker.opencarp.org/iam-cms/autotester/gitlab <docker.opencarp.org/iam-cms/autotester/gitlab>`__
  instead.
  It creates output files with the user permissions configured for the
  gitlab-runner.

The docker image provided at
`docker.opencarp.org/iam-cms/autotester <docker.opencarp.org/iam-cms/autotester>`__
wraps the Autotester binary and accepts the same options and arguments:

::

   docker run docker.opencarp.org/iam-cms/autotester --help

To run test suites or single test cases the local folder must be mapped
into the container. The following example illustrates how to call
Autotester from the current directory (cwd):

::

   docker run --rm -v "$(pwd):/shared" docker.opencarp.org/iam-cms/autotester

The folder ``/shared`` is Autotesters’ work-directory in the docker
container. It should be mounted to the folder where Autotester should
run (for example the ATC root folder). This way the output files can be
created as usual, given that read and write permissions are available.

Apart from the need to map a folder to the container, the usage is the
same as when using Autotester without docker. For example the ``-X``
switch can be used to let Autotester create the file ``output.xml``:

::

   docker run --rm -v "$(pwd):/shared" docker.opencarp.org/iam-cms/autotester -X
