Obtaining test results
~~~~~~~~~~~~~~~~~~~~~~

Results will be printed to the console (stdout) as well as a short
summary at the end. Additionally, the logfiles ``log_execution.txt``,
``log_validation.txt`` and ``log_output.txt`` will be created unless the
option ``-n|--nolocallog`` is specified. Note that ``log_output.txt``
will be empty as long as there was nothing printed on stderr by the
called program. This log file can be helpful to find out why a certain
test failed.

There is also the possibility to let Autotester write an XML output file
by specifying the option ``-X|--xmloutput``. This output will be written
to the file ``output.xml`` in the directory where Autotester is executed
and contains detailed information about the tests and validations as
well as their results. The XML format of this output is based on the ATC
format. For a detailed description of the format, see
``schema/output.xsd``. The file name and location of the output file can
be overwritten using the option ``-M|--xmloutputfile [FILE]``

