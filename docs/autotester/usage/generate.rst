Generating result summaries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When running Autotester with the ``-X`` option, a file with a result
summary in XML is created. This file can be used to generate a summary
in other formats. For both available transformations, XSLT is used, so
the command line utility ``xsltproc`` needs to be installed.

Generating an HTML result page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note: When generating the HTML output, the folders ‘css’ and ‘js’ need
to be available in the correct relative path.

::

   mkdir -p build/transformation
   xsltproc transformation/html.xsl output.xml > build/results.html
   cp -r transformation/css build/transformation/
   cp -r transformation/js build/transformation/

Generating a text summary
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   mkdir -p build/transformation
   xsltproc transformation/text.xsl output.xml > build/results.txt
