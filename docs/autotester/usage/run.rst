Running Autotester
~~~~~~~~~~~~~~~~~~

As mentioned in the previous section, there are two general ways to run
Autotester:

1. Recursively process all ATC files in a directory, for example:

.. code:: bash

  autotester examples/structured_tests

If no folder is specified, Autotester will start in the current directory.

2. Using the option ``-s|--single`` to run a single ATC file, for
   example:

.. code:: bash

  autotester -s examples/simple_test/main.atc

To get information about the command line options, run
``autotester --help``. It’s also possible to get an XML description of
Autotester and it’s options by running ``autotester --xmlhelp``.
