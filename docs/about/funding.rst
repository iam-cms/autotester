Funding
-------

This project is funded by the `DFG <https://www.dfg.de/>`__ as part of
the project SuLMaSS (Sustainable Lifecycle Management for Scientific
Software).
