/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_PARAMETER_BASE_H
#define LIB_PARAMETER_BASE_H

#include <stdbool.h>
#include <parameter.h>

#define PARAM_NONE             0
#define PARAM_LONG             1
#define PARAM_REAL             2
#define PARAM_VECTOR_LONG      3
#define PARAM_VECTOR_N_LONG    4
#define PARAM_LIST_LONG        5
#define PARAM_VECTOR_REAL      6
#define PARAM_FLAG             7
#define PARAM_STRING           8
#define PARAM_FILEIN           9
#define PARAM_FILEOUT         10

typedef struct argument_s argument_t;
void printIntervalWarning(char name);

void getValue_long(const char* prefix, const char* name, char* arg, void* value);
void getValue_real(const char* prefix, const char* name, char* arg, void* value);
void getValue_vector_long(const char* prefix, const char* name, char* arg, void* value);
void getValue_vector_n_long(const char* prefix, const char* name, char* arg, void* value);
void getValue_list_long(const char* prefix, const char* name, char* arg, void* value);
void getValue_vector_real(const char* prefix, const char* name, char* arg, void* value);
void getValue_flag(const char* prefix, const char* name, char* arg, void* value);
void getValue_string(const char* prefix, const char* name, char* arg, void* value);
void getValue_filein(const char* prefix, const char* name, char* arg, void* value);
void getValue_fileout(const char* prefix, const char* name, char* arg, void* value);

void printHelp_DEFAULT(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_long(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_real(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));

void printHelp_vector_n_long(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_list_long(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_vector_real(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_string(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_filein(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));
void printHelp_fileout(const argument_t* argument, bool extendedhelp, UNUSED(const char* delimiter));

void printXml_long(const void* parameter);
void printXml_real(const void* parameter);
void printXml_vector_long(const void* parameter);
void printXml_vector_n_long(const void* parameter);
void printXml_list_long(const void* parameter);
void printXml_vector_real(const void* parameter);
void printXml_flag(const void* parameter);
void printXml_string(const void* parameter);
void printXml_filein(const void* parameter);
void printXml_fileout(const void* parameter);

void warnParamMissing_DEFAULT(const char* name);
void warnParamMissing_filein(const char* name);
void warnParamMissing_fileout(const char* name);

void validate_DEFAULT(const argument_t* argument, char* givenargs, size_t argnumber);
void validate_long(const argument_t* argument, char* givenargs, size_t argnumber);
void validate_real(const argument_t* argument, char* givenargs, size_t argnumber);
void validate_vector_long(const argument_t* argument, char* givenargs, size_t argnumber);
void validate_vector_n_long(const argument_t* argument, char* givenargs, size_t argnumber);
void validate_vector_real(const argument_t* argument, char* givenargs, size_t argnumber);


#endif //LIB_PARAMETER_BASE_H
