/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_EXCEPTION_H
#define LIB_EXCEPTION_H

#if __APPLE__
  #define _XOPEN_SOURCE 600
#endif
#include <ucontext.h>

#define EXCEPTION_FLOAT       1
#define EXCEPTION_INTERRUPT   2
#define EXCEPTION_SIGSEGV     4
#define EXCEPTION_QUIT        8

void setupSignals(long signaltype);
void printBacktrace(ucontext_t* ucontext, long deepness);

#endif
