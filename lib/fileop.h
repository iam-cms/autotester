/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_FILEOP_H
#define LIB_FILEOP_H

/** @brief Find the start of the filename and return a pointer to it.
  *
  * @param fullfilename search for filename
  *
  * @return pointer to the beginning of the filename
  *
  */
static inline char* findFileName(char* fullfilename) {
  char *result;

  if (fullfilename == NULL) return NULL;

  result = strrchr(fullfilename, '/');

  if (result == NULL) return fullfilename;
  return ++result;
}

void resolvePath(char** path, char** filename, const char* const longfilename);
long getFilesFiltered(char *** foundfiles, const char *const path, const char *regex, bool recursive); // not thread save

#endif
