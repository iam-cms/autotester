/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <getopt.h>

#include <wrapper.h>
#include <container/stringconv.h>

#include "./version.h"
#include "parameter.h"


#define BLOCKLENGTH 90

paramtype_t paramtypes[2048];

static void parameterTypeUnregisteredError(long type);
static void parameterTypeMissingCallbackError(const paramtype_t* paramtype, const char* callback_description);

/** @brief Tokenize description or example text if it is not too large.
  */
void printHelpTextwithNewline(const char* text) {
  int oldlinepos = 0;
  int newlinepos = 0;

  while (text[oldlinepos] != '\0') {
    while (text[newlinepos] != '\0' && text[newlinepos] != '\n') newlinepos++;
    if (text[newlinepos] == '\n') newlinepos++;
    if (newlinepos-oldlinepos < BLOCKLENGTH) {
      fprintf(stdout, "                %.*s", newlinepos-oldlinepos, &text[oldlinepos]);
    } else {
      // printing the text if token with newline is too large.
      size_t linelength = 0;
      int oldtokenpos = oldlinepos;
      int newtokenpos = oldlinepos;

      fprintf(stdout, "                ");
      while (newtokenpos < newlinepos) {
        while (newtokenpos < newlinepos && text[newtokenpos] != ' ') newtokenpos++;

        linelength += newtokenpos - oldtokenpos;
        if (linelength >= BLOCKLENGTH) {
          if (oldtokenpos != oldlinepos) oldtokenpos++;
          fprintf(stdout, "\n                ");
          linelength = newtokenpos-oldtokenpos;
        }
        fprintf(stdout, "%.*s", newtokenpos-oldtokenpos, &text[oldtokenpos]);
        oldtokenpos = newtokenpos++;
      }
    }
    oldlinepos = newlinepos++;
  }
  fputc('\n', stdout);
  fputc('\n', stdout);
}

static const char* getTypeDescription(int paramtype) {
  return paramtypes[paramtype].description;
}

static const char verbosemsg[] = "enable the output (stderr) of some (helpful) log "
                                 "messages with log-level <i>, a higher level <i> will "
                                 "create more messages.";

static void printXMLUsage(const char* progname, const toolparam_t* tool, size_t count, char* bitfield) {
  size_t i, unnamed;

  char *escapedstr = NULL;

  printf("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
  String_mask(tool->description, &escapedstr, STRING_MASK, XML_masktable);
  printf("<program name=\"%s\" version=\""PACE3D_VERSION" (Release date: "PACE3D_RELEASE_DATE")\" istested=\"%ld\" description=\"%s\" ", progname, tool->istested, escapedstr);
  Free(escapedstr);

  String_mask(tool->example, &escapedstr, STRING_MASK, XML_masktable);
  printf("example=\"%s\" >\n", escapedstr);
  Free(escapedstr);

  for (i = 0, unnamed = 0; i < count; i++) {
    const argument_t *arg = &tool->arguments[i];

    printf("\t<param ");

    if (arg->longname != NULL) {
      printf("name=\"%s\" ", arg->longname);
    } else {
      printf("name=\"arg%zu\" ", unnamed++);
    }

    if (arg->name != ' ') {
      printf("char=\"%c\" ", arg->name);
    }

    if (arg->description) {
      String_mask(arg->description, &escapedstr, STRING_MASK, XML_masktable);
      printf("description=\"%s\" ", escapedstr);
      Free(escapedstr);
    }

    if (arg->option == PARAM_REQUIRED) {
      printf("required=\"true\" ");
    }

    printf("type=\"%s\" ", getTypeDescription(arg->flag));

    if (arg->andparams && arg->andparams[0] != '\0') {
      printf("relations=\"%s\" ", arg->andparams);
    }

    if (arg->interval && arg->interval[0] != '\0') {
      printf("interval=\"%s\" ", arg->interval);
    }

    long type = arg->flag;
    if (paramtypes[type].printHelp != NULL) {
      paramtypes[type].printXml(arg->parameter);
    } else if (!paramtypes[type].registered) {
      parameterTypeUnregisteredError(type);
    } else {
      parameterTypeMissingCallbackError(&paramtypes[type], "print xml");
    }

    if (getBit(bitfield, i) == true) {
      printf("enabled=\"true\" ");
    }

    printf("/>\n");
  }

  String_mask(verbosemsg, &escapedstr, STRING_MASK, XML_masktable);
  printf("\t<param name=\"verbose\" char=\"v\" description=\"%s\" type=\"%s\" default=\"%li\"/>\n",
          escapedstr,
          getTypeDescription(PARAM_LONG),
          verbose);
  Free(escapedstr);
  printf("\t<param name=\"help\" char=\"h\" description=\"print help\" type=\"%s\" />\n", getTypeDescription(PARAM_FLAG));

  printf("</program>\n");
}

void printParameter(const char shortname, const char* longname, char* hint) {
  if (isgraph(shortname)) {
    if (hint != NULL) {
      printf(" -%c%s", shortname, hint);
    } else {
      printf(" -%c", shortname);
    }
  }
  if (longname != NULL) {
    if (hint != NULL) {
      printf(" --%s=%s", longname, hint);
    } else {
      printf(" --%s", longname);
    }
  }
  if (!isgraph(shortname) && longname == NULL) {
    printf(" %s", hint);
  }
}

static void printArgumentHelptext(argument_t argument, bool extendedhelp) {
  const char *delimiter;

  if (argument.option == PARAM_REQUIRED) {
    delimiter = " (required)\n";
  } else {
    delimiter = "\n";
  }

  long type = argument.flag;
  if (paramtypes[type].printHelp != NULL) {
    paramtypes[type].printHelp(&argument, extendedhelp, delimiter);
  } else if (!paramtypes[type].registered) {
    parameterTypeUnregisteredError(type);
  } else {
    parameterTypeMissingCallbackError(&paramtypes[type], "print help");
  }

  printf("%s", delimiter);
  if (argument.description == NULL) {
    mydebug("!!!Help text is not given!!!");
  } else {
    printHelpTextwithNewline(argument.description);
  }
}

static void printDescriptionHelptext(toolparam_t tool) {
  printf("\nDESCRIPTION:\n");
  if (tool.description == NULL) {
    mydebug("description text is not given.");
  } else {
    String_writeBlocktext(stdout, 100, tool.description);
  }

  printf("Release Version "PACE3D_VERSION" ("PACE3D_RELEASE_DATE")\n");
}

static void printExampleHelptext(toolparam_t tool) {
  printf("EXAMPLE:\n");
  if (tool.example == NULL) {
    mydebug("example text is not given.");
  } else {
    String_writeBlocktext(stdout, 100, tool.example);
  }
  printf("\n");
}

static void printVerboseHelptext(void) {
  printf(" -v<i> --verbose=<i>\n");
  printHelpTextwithNewline(verbosemsg);
  printf(" -h --help\n                print this help (--helpall prints an extended help)\n\n");
}

static void printUsageInternal(const char* progname, toolparam_t tool, size_t count, bool extendedhelp) {
  size_t i;
  printDescriptionHelptext(tool);
  printf("\n\nUSAGE: %s [OPTIONS]\n\n\nOPTIONS:\n\n", progname);
  for (i = 0; i < count; i++) {
    printArgumentHelptext(tool.arguments[i], extendedhelp);
  }
  printVerboseHelptext();
  if (extendedhelp) {
    printf("    --xmlhelp\n                print this help as XML\n\n");
  }
  printExampleHelptext(tool);
}

void printUsage(const char* progname, toolparam_t tool, size_t count) {
  printUsageInternal(progname, tool, count, false);
}

#ifdef DEBUG
void printIntervalWarning(char name) {
  mydebug("Parameter '-%c' is not initialized by user.", name);
}
#else
void printIntervalWarning(UNUSED(char name)) {
}
#endif

static bool validateParams(toolparam_t tool, size_t argumentcount, char* givenargs) {
  size_t  i;


  for (i = 0; i < argumentcount; i++) {
    const argument_t *argument = &tool.arguments[i];

    if (argument->interval != NULL) {
      long type = argument->flag;
      if (paramtypes[type].validate != NULL) {
        paramtypes[type].validate(argument, givenargs, i);
      } else if (!paramtypes[type].registered) {
        parameterTypeUnregisteredError(type);
      } else {
        parameterTypeMissingCallbackError(&paramtypes[type], "validate");
      }
    }
  }

  return true;
}

static void getParameterValue(const char* parameterprefix, const char* parametername, char* optarg, void* parametervalue, long parameterflag) {
  long type = parameterflag;
  if (paramtypes[type].getValue != NULL) {
    paramtypes[type].getValue(parameterprefix, parametername, optarg, parametervalue);
  } else if (!paramtypes[type].registered) {
    parameterTypeUnregisteredError(type);
  } else {
    parameterTypeMissingCallbackError(&paramtypes[type], "get value");
  }
}

bool getParams(int argc, char *argv[], toolparam_t tool, size_t count) {
  int   opt;  // getopt

  opterr                   = 0;  // disable getopt error output
  size_t i, j;
  long   k, n;
  long   nopt              = 0;
  long   givennoptcount    = 0;
  long   noptcount         = 0;
  long   requirednoptcount = 0;
  long   optcount          = 0;
  long   requiredargs      = 0;
  char   args[count*2 + 4 + 1];
  char   requiredarglist[count+1];
  char   givenargs[count+2];
  char  *bitfield;               // for checking required arguments and already given arguments

#ifdef MPI
  int mpirank;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);

  if (mpirank == 0) {
#endif
    if (tool.istested == 0) {
      mywarn("PLEASE BE CAREFUL, THIS TOOL IS UNDER CONSTRUCTION AND NOT TESTED YET!");
    } else if (tool.istested < 0) {
      mywarn("PLEASE BE CAREFUL, THIS TOOL DOES NOT WORK CORRECTLY!!!");
    } else {
      if (tool.istested < 40) {
        fprintc(stderr, 1, 31, "The test process for this tool is completed by %ld%%", tool.istested);
      } else if (tool.istested < 90) {
        fprintc(stderr, 1, 33, "The test process for this tool is completed by %ld%%", tool.istested);
      } else if (tool.istested < 100) {
        fprintc(stderr, 1, 32, "The test process for this tool is completed by %ld%%", tool.istested);
      }
    }
#ifdef MPI
  }
#endif

  // First test for help message. If found do no further check of parameters.
  for (n = 0; n < argc; n++) {
    if (strcmp(argv[n], "-h") == 0 || strcmp(argv[n], "--help") == 0) {
      printUsage(argv[0], tool, count);
      exit(EXIT_OK);
    } else if (strcmp(argv[n], "--helpall") == 0) {
      // flag for extended help text
      printUsageInternal(argv[0], tool, count, true);
      exit(EXIT_OK);
    }
  }

  bitfield = mallocBitfield(count);

  // assemble the opt string
  args[0] = ':';
  args[1] = 'v';
  args[2] = ':';
  args[3] = 'V';
  for (j = 4, i = 0; i < count; i++) {
    const argument_t *argument = &tool.arguments[i];
    if (argument->name != ' ') {
      args[j++] = argument->name;
      if (!paramtypes[argument->flag].is_flag) {
        args[j++] = ':';
      }
    }
    if (argument->option == PARAM_REQUIRED) {
      requiredarglist[requiredargs] = argument->name;
      requiredargs++;
    }
  }
  args[j]                       = '\0';
  requiredarglist[requiredargs] = '\0';
  myverbose(VERBOSE_DEBUG, verbose, "argument string: %s required arguments: %s", args, requiredarglist);

  // Assemble long options
  struct option *long_options = Malloc((count+4)*sizeof(struct option));
  long_options[0] = (struct option){"verbose", required_argument, NULL, 'v'};
  long_options[1] = (struct option){"version", no_argument,       NULL, 'V'};
  long_options[2] = (struct option){"xmlhelp", no_argument,       NULL,  0 };

  for (j = 3, i = 0; i < count; i++) {
    const argument_t *argument = &tool.arguments[i];
    if (argument->longname) {
      long_options[j++] = (struct option) {
        .name    = argument->longname,
        .has_arg = (paramtypes[argument->flag].is_flag) ? no_argument : required_argument,
        .flag    = NULL,
        .val     = argument->name
      };
    }
  }
  long_options[j] = (struct option){NULL,      0,                 NULL,  0 };

  myverbose(VERBOSE_DEBUG, verbose, "parsing %d parameter%s...", argc-1, (argc-1>1)?"s":"");

  while (true) {
    int option_index = -1;
#if 0  // if only short opt are available
    opt = getopt(argc, argv, args);
#else
    opt = getopt_long(argc, argv, args, long_options, &option_index);
#endif
    if (opt == -1) break; // got last argument

    bool islongopt = (option_index != -1);

    const char shortname[] = {opt, '\0'};
    const char *parametername = islongopt ? long_options[option_index].name : shortname;
    const char *parameterprefix = islongopt ? "--" : "-";

    // Save and count given arguments by user.
    givenargs[optcount] = opt;
    optcount++;

    for (i = 0; i < count; i++) {
      if (opt == tool.arguments[i].name) {
        if (getBit(bitfield, i) == 1) {
          myexit(ERROR_PARAM, "Argument '%s%s' is already given.", parameterprefix, parametername);
        }
        setBit(bitfield, i);

        getParameterValue(parameterprefix, parametername, optarg, (void*)tool.arguments[i].parameter, tool.arguments[i].flag);
        break;
      } // IF
    } // FOR

    switch (opt) {
      case 'v':
        // extern verbose is by default set to VERBOSE_NORMAL
        if (String_makeLong(optarg, &verbose) == false) {
          myexit(ERROR_PARAM, "Could not get long from parameter '%s%s'.", parameterprefix, parametername);
        }
        break;
      case 'V':
        printf("Release Version %s - Date %s\n", PACE3D_VERSION, PACE3D_RELEASE_DATE);
        exit(EXIT_OK);
        break;
      case ':':
        printUsage(argv[0], tool, count);
        if (islongopt) {
          myexit(ERROR_PARAM, "Option '%s' requires an argument.", argv[optind - 1]);
        } else {
          myexit(ERROR_PARAM, "Option '-%c' requires an argument.", optopt);
        }
      case '?':
        printUsage(argv[0], tool, count);
        if (optopt == '\0') {
          myexit(ERROR_PARAM, "Unknown parameter '%s' is given.", argv[optind - 1]);
        } else {
          myexit(ERROR_PARAM, "Unknown parameter '-%c' is given.", optopt);
        }
    }

  } // WHILE
  givenargs[optcount] = '\0';
  Free(long_options);

  // XOR Parameters
  for (n = 0; n < optcount; n++) {
    for (i = 0; i < count; i++) {
      if (givenargs[n] == tool.arguments[i].name) {
        for (j = 0; j < strlen(tool.arguments[i].andparams); j++) {
          long xorflag = false;

          if (tool.arguments[i].andparams[j] == '!') {
            xorflag = true;
            j++;
          }
          for (k = 0; k < optcount; k++) {
            if (givenargs[k] != givenargs[n] &&
                givenargs[k] == tool.arguments[i].andparams[j]) {
              if (xorflag) {
                printUsage(argv[0], tool, count);
                myexit(ERROR_PARAM, "XOR  '-%c' is not allowed with '-%c'", givenargs[n], tool.arguments[i].andparams[j]);
              } else {
//                 mydebug("and parameter found");
                break;
              }
            }
          }
          if (!xorflag && k == optcount) {
            printUsage(argv[0], tool, count);
            myexit(ERROR_PARAM, "parameter '-%c' requires parameter '-%c'", givenargs[n], tool.arguments[i].andparams[j]);
          }
        }
      }
    }
  }

  // Get all required arguments (NOT Options)
  for (i = 0; i < count; i++) {
    if (tool.arguments[i].name == ' ') {
      noptcount++;
      if (tool.arguments[i].option == PARAM_REQUIRED) {
        requirednoptcount++;
      }
    }
  }

  givennoptcount = argc - optind;

  if (givennoptcount > noptcount) {
    printUsage(argv[0], tool, count);
    myexit(ERROR_PARAM, "Too many parameters are given.");
  }

  // Passing nopt params
  for (i = 0; i < count; i++) {
    if (tool.arguments[i].name == ' ') {
      if (nopt < givennoptcount) {
        getParameterValue("", "<no opt parameter>", argv[optind + nopt], (void*)tool.arguments[i].parameter, tool.arguments[i].flag);
        setBit(bitfield, i);
        nopt++;
      }
    }
  }

  for (n = 0; n < argc; n++) {
    if (strcmp(argv[n], "--xmlhelp") == 0) {
      printXMLUsage(argv[0], &tool, count, bitfield);
      exit(EXIT_OK);
    }
  }

  // check if parameter is required
  for (i = 0; i < count; i++) {
    // check again for 'AND'-parameters
    bool reallyrequired = true;
    for (j = 0; j < strlen(tool.arguments[i].andparams); j++) {
      bool xorflag = false;
      if (tool.arguments[i].andparams[j] == '!') {
        xorflag = true;
        j++;
      }
      bool found = false;
      for (k = 0; k < optcount; k++) {
        if (givenargs[k] == tool.arguments[i].andparams[j]) {
          found = true;
          break;
        }
      }
      if ((!xorflag && !found) || (xorflag && found)) {
        reallyrequired = false;
        break;
      }
    }
    if (reallyrequired) {
      if (getBit(bitfield, i) != 1 && tool.arguments[i].option == PARAM_REQUIRED && tool.arguments[i].name != ' ') {
        printUsage(argv[0], tool, count);
        myexit(ERROR_PARAM, "Parameter '-%c' is missing.", tool.arguments[i].name);
      }
    }
  }

  // check if enough or too many parameters are given
  if (givennoptcount < requirednoptcount) {
    printUsage(argv[0], tool, count);
    for (i = 0; i < count; i++) {
      if (getBit(bitfield, i) != 1 && tool.arguments[i].option == PARAM_REQUIRED) {

        long type = tool.arguments[i].flag;
        if (paramtypes[type].warnMissing != NULL) {
          paramtypes[type].warnMissing(&tool.arguments[i].name);
        } else if (!paramtypes[type].registered) {
          parameterTypeUnregisteredError(type);
        } else {
          parameterTypeMissingCallbackError(&paramtypes[type], "warn missing");
        }
      }
    }
    myexit(ERROR_PARAM, "Required Parameter%s not given.", requirednoptcount > 1 ? "s are": " is");
  }

  validateParams(tool, count, bitfield);

  Free(bitfield);

  return true;
}

void registerParamType(paramtype_t paramtype) {
  paramtype.registered = true;
  paramtypes[paramtype.type] = paramtype;
}

void setupParameterTypes() {
  register_base_types();
#ifndef __APPLE__
  if (register_extended_types != NULL) {
    register_extended_types();
  }
#endif
}

static void parameterTypeUnregisteredError(long type) {
  myexit(ERROR_PARAM, "Unregistered parameter type found: %ld", type);
}

static void parameterTypeMissingCallbackError(const paramtype_t* paramtype, const char* callback_description) {
  myexit(ERROR_PARAM, "Parameter type \"%s\" does not support the operation \"%s\"", paramtype->description, callback_description);
}
