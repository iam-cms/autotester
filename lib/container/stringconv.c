/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <regex.h>

#include <wrapper.h>
#include "stringconv.h"

const char *XML_masktable[][2]  = {
  { "&",  "&amp;" },
  { "<",  "&lt;"  },
  { ">",  "&gt;"  },
  { "\"", "&quot;"},
  { "\t", "&#09;" },
  { "\n", "&#10;" },
  { "",   ""      }
};

static long parseSizeParameter(long* value, const char* format, va_list* arg_list) {
  char *next_format_pos;

  if (strncmp(format, "[%i]", 4) == 0) {
    *value = (va_arg(*arg_list, long));
    return 4;
  } else {
    if (*format != '[') {
      myexit(ERROR_BUG, "missing opening brace '[' in format string");
    }
    errno = 0;
    *value = strtol(format+1, &next_format_pos, 10);

    if (errno) {
      myexit(ERROR_BUG, "error: reading an integer for a parameter value failed (%s).", strerror(errno));
    }
    if (format == next_format_pos) {    // no conversion was done
      myexit(ERROR_BUG, "error: reading an integer for a parameter value failed (%s).", ((*format)=='\0')?"end of line":"not a valid integer value");
    }

    if (*next_format_pos != ']') {
      myexit(ERROR_BUG, "missing closing brace ']' in format string");
    }
    return next_format_pos+1-format;
  }
}

static size_t getSliceParameters(const char* format, va_list* arg_list, long** slicevector, long* slicevectorsize) {
  size_t  length;

  // check for opening brace, indicating a slice-vector
  if (*format != '{') {
    *slicevector = NULL;
    return 0;
  }
  // TODO the input of vectors should also be possible like this: (2,3,...) and not only like that: %v
  if (strncmp(format+1, "%v", 2) != 0) {
    myexit(ERROR_BUG, "only long-arrays can define a sliceVector");
  }

  *slicevector = va_arg(*arg_list, long*);

  length = 3;
  length += parseSizeParameter(slicevectorsize, format+length, arg_list);
  if (*(format+length) != 'i') {
    myexit(ERROR_BUG, "only long-arrays can define a sliceVector");
  }

  length++;
  if (*(format + length) != '}') {
    myexit(ERROR_BUG, "missing closing brace '}' in format string");
  }
  return length + 1;
}

/** @brief Copy a string to new string and replace 'find' by 'replace'.
  *
  * @param oldstring   input string
  * @param newstring   reference to string where each appearance of 'find' is replaced by 'replace', could also be the referrence to the 'oldstring'
  * @param find        string which should be replaced
  * @param replace     string which replaces 'find'
  */
size_t String_replace(const char* oldstring, char** newstring, const char* find, const char* replace) {
  size_t    size = MAX_LINE_LENGTH;
  long      buffersizeleft = size-1; // reserving the '\0'-termination
  char     *cur_str_pos;
  size_t    strlenfind    = strlen(find);
  size_t    strlenreplace = strlen(replace);

  if (newstring == NULL) {
    myexit(ERROR_BUG, "Result pointer for new string is NULL.");
  }
  if (strlenfind == 0) {
    myexit(ERROR_BUG, "'find'-string can not be empty.");
  }

  // Allocate memory for the resulting new String
  char *bufferstring = MallocS(size);
  cur_str_pos = bufferstring;

  while (*oldstring != '\0') {
    while ((long)(buffersizeleft-strlenreplace) <= 0) {
      // newstring could not be completely filled
      buffersizeleft += size;
      size *= 2;
      size_t newpos = cur_str_pos-bufferstring;
      bufferstring = (char*)Realloc(bufferstring, size);
      cur_str_pos = bufferstring+newpos;
    }
    if (strncmp(oldstring, find, strlenfind) != 0) {
      // 'find' does not appear at the current string position => copy current char to newstring
      *cur_str_pos = *oldstring;
      cur_str_pos++;
      buffersizeleft--;
      oldstring++;
    } else {
      // 'find' appears at the current string position => copy 'replace' to newstring
      strcpy(cur_str_pos, replace);
      cur_str_pos    += strlenreplace;
      buffersizeleft -= strlenreplace;
      oldstring      += strlenfind;
    }
  }
  *cur_str_pos = '\0';

  size -= buffersizeleft;
  if (*newstring) Free(*newstring);
  *newstring = Realloc(bufferstring, size);
  return size;
}

/** @brief Mask a string by a given table.
  *
  * @note for masking of whole strings the order in this table is important, first matched rule is the one which is applied
  *
  * @param oldstring   input string
  * @param newstring   reference to string where each appearance of 'find' is replaced by 'replace', could also be the reference to the 'oldstring'
  * @param mask 1, each appearance of a string part in the first column will be replaced by the corresponding string in the right column,
  *             0 will unmask in the opposite manner
  * @param stringmask  table with character sequences to be masked, terminated by zero length string
  */
void String_mask(const char* oldstring, char** newstring, long mask, const char* stringmask[][2]) {
  size_t  i, maskindex;
  size_t  maskstrlen, unmaskstrlen;
  long    hit;

  size_t  writepos;
  size_t  resultsize = MAX_LINE_LENGTH;
  char   *result = MallocS(resultsize);

  // create fast lookup table for first char of all replacement rules
  char    fastlookup[256]; // one for every ascii char
  memset(fastlookup, 0x00, 256*sizeof(char));
  for (i = 0; stringmask[i][!mask][0] != '\0'; i++) {
    fastlookup[(int)(stringmask[i][!mask][0])] = '1'; // remember first char of all strings in masktable
  }

  // walk through the string replace check real table if fastlookup char is found
  writepos = 0;
  i        = 0;
  while (oldstring[i] != '\0') {
    // candidate for replacement
    if (fastlookup[(int)oldstring[i]] == '1') {
      // is candidate of a real hit
      hit = false;
      for (maskindex = 0; stringmask[maskindex][!mask][0] != '\0'; maskindex++) {
        maskstrlen = strlen(stringmask[maskindex][!mask]);
        if (strncmp(&(oldstring[i]), stringmask[maskindex][!mask], maskstrlen) == 0) {
          hit = true;
          break;
        }
      }

      if (hit) {
        // move future read pointer over replaced string
        i += maskstrlen;

        // write masktable string
        // make room
        unmaskstrlen = strlen(stringmask[maskindex][mask]);
        if (writepos + unmaskstrlen >= resultsize) {
          resultsize = unmaskstrlen + resultsize*2;
          result = Realloc(result, resultsize);
        }

        // copy
        strcpy(&(result[writepos]), stringmask[maskindex][mask]);
        writepos += unmaskstrlen;

        continue;
      }
    }

    // not interesting just copy char
    if (writepos >= resultsize) {
      resultsize *= 2;
      result = Realloc(result, resultsize);
    }
    result[writepos] = oldstring[i];
    writepos++;

    i++;
  }

  // trim and add null termination
  result = Realloc(result, writepos+1);
  result[writepos] = '\0';

  // store result
  if (oldstring == *newstring) Free(*newstring);
  *newstring = result;
}

/** @brief Separate a string separated by a char into single tokens.
  *
  * @param list      input string
  * @param result    result string array
  * @param count     number of tokens found
  * @param separator separating character
  */
void String_makeTokenList(const char* list, char*** result, ulong* count, char separator) {
  long  i;

  // filter invalid argument lists
  if (list == NULL || strlen(list) == 0) {
    myexit(ERROR_BUG, "No argument list to tokenize");
  }
  if (list[0] == separator) {
    myexit(ERROR_PARAM, "A '%c'-separated list must not start with a '%c'.", separator, separator);
  }
  if (list[strlen(list) - 1] == separator) {
    myexit(ERROR_PARAM, "A '%c'-separated list must not end with a '%c'", separator, separator);
  }
  if (result == NULL) {
    myexit(ERROR_BUG, "Result pointer for list is NULL.");
  }
  if (count == NULL) {
    myexit(ERROR_BUG, "Count pointer for list is NULL.");
  }

  *count = 1;
  // count separators, for i separators there are i+1 arguments in a valid list
  for (i = 0; list[i] != '\0'; i++) {
    if (list[i] == separator) {
      if (list[i + 1] == separator) {
        myexit(ERROR_PARAM, "A separated list must not contain two consecutive '%c'.", separator);
      }
      (*count)++;
    }
  }

  // copy the original argument list because parsing works destructive
  char *tokenizelist = Strdup(list);

  // Separate the token-separated values into the result array
  char  separatorstr[2] = { separator, '\0' };
  char *token = strtok(tokenizelist, separatorstr);

  if (token == NULL) {
    mywarn("empty string or not tokenizable (%s)", tokenizelist);
    *count = 0;
    *result = NULL;
  } else {

    // Allocate memory for the resulting array of strings
    *result = (char**) Malloc( (*count) * sizeof(char*) );

    i = 0;
    (*result)[i++] = Strdup(token);

    while ((token = strtok(NULL, separatorstr)) != NULL) {
      (*result)[i++] = Strdup(token);
    }
  }

  Free(tokenizelist);
}

/** @brief Separate a string separated by different characters into single tokens.
  *
  * @param list         input string
  * @param result       result string array
  * @param count        number of tokens found
  * @param separatorstr separating characters (stored as a string)
  */
void String_makeTokenList2(const char* list, char*** result, ulong* count, const char* separatorstr) {
  long  i;
  long  countflag = 0;  // flags whether a token was already counted

  // filter invalid argument lists
  if (list == NULL || strlen(list) == 0) {
    myexit(ERROR_BUG, "No argument list to tokenize");
  }
  if (result == NULL) {
    myexit(ERROR_BUG, "Result pointer for list is NULL.");
  }
  if (count == NULL) {
    myexit(ERROR_BUG, "Count pointer for list is NULL.");
  }

  *count = 0;

  // count tokens that are separated
  for (i = 0; list[i] != '\0'; i++) {
    // if character is no separator:
    if (strchr(separatorstr, list[i]) == NULL) {
      // count if token hasn't been counted yet
      if (countflag == 0) {
        (*count)++;
      }
      // set countflag
      countflag = 1;
    }
    // if character is a separator: reset countflag
    else {
      countflag = 0;
    }
  }

  // Allocate memory for the resulting array of strings
  *result = (char**) Malloc((*count) * sizeof(char*));

  // copy the original argument list because parsing works destructive
  char *tokenizelist = Strdup(list);

  // Separate the tokens into the result array
  char *token = strtok(tokenizelist, separatorstr);

  if (token == NULL) {
    myexit(ERROR_BUG, "empty string or not tokenizable (%s)", tokenizelist);
  } else {
    i = 0;
    (*result)[i++] = Strdup(token);

    while ((token = strtok(NULL, separatorstr)) != NULL) {
      (*result)[i++] = Strdup(token);
    }
  }

  Free(tokenizelist);
}


/** @brief Separate a string separated by several characters into single tokens.
  *
  * @param list         input string
  * @param result       result string array
  * @param count        number of tokens found
  * @param separatorstr separating characters (stored as a string)
  */
void String_makeTokenList3(const char* list, char*** result, ulong* count, const char* separatorstr) {
  long  i;
  size_t separatorstrsize = strlen(separatorstr);

  // filter invalid argument lists
  if (list == NULL || strlen(list) == 0) {
    myexit(ERROR_BUG, "No argument list to tokenize");
  }
  if (result == NULL) {
    myexit(ERROR_BUG, "Result pointer for list is NULL.");
  }
  if (count == NULL) {
    myexit(ERROR_BUG, "Count pointer for list is NULL.");
  }

  *count = 0;
  (*result) = NULL;

  const char *lasttoken = list;
  // count tokens that are separated
  for (i = 0; list[i] != '\0'; i++) {
    // if string part is a separator:
    if (strncmp(&list[i], separatorstr, separatorstrsize) == 0) {

      // Allocate memory for the resulting array of strings
      *result = (char**) Realloc(*result, ((*count)+1) * sizeof(char*));
      size_t size = &list[i]-lasttoken + 1;
      (*result)[(*count)++] = strncpy(MallocS(size), lasttoken, size-1);
      i += separatorstrsize;
      lasttoken = &list[i];
    }
  }
  if (*lasttoken != '\0') {
    // token list is not ending with the separatorstring
    // Allocate memory for the resulting array of strings
    *result = (char**) Realloc(*result, ((*count)+1) * sizeof(char*));
    size_t size = &list[i]-lasttoken + 1;
    (*result)[(*count)++] = strncpy(MallocS(size), lasttoken, size-1);
  }
}

void String_makeFrames(const char* list, long** frames, long* count) {
  //1. create long list from string
  String_makeDynamicLongList(list, frames, count);
  //2. modify frames to start at 0 (convert Fortran-style -> C-style numbering)
  for (long i = 0; i < *count; i++) {
    (*frames)[i]--;
    if ((*frames)[i] < 0) myexit(ERROR_PARAM, "All frame values need to be positive.");
  }
}

void String_makeDynamicLongList(const char* list, long** values, long* count) {
  char **result=NULL;
  ulong  tokencount, token;
  long   index = 0;
  long   value;
  long   var1, var2, step = 0;
  long   arguments;

  String_makeTokenList(list, &result, &tokencount, ',');

  *count = 0;

  for (token = 0; token < tokencount; token++) {
    // Get Format 1-2%3
    arguments = sscanf(result[token], "%ld-%ld%%%ld", &var1, &var2, &step);
    if (arguments <= 0) { // no match
      myexit(ERROR_PARAM, "No value regonized.");
    } else if (arguments == 3) { // 1-2%3
    } else if (arguments == 2) { // 1-2
      step = (var1 > var2) ? -1 : 1;
    } else if (arguments == 1) { // 1
      step = 1;
      var2 = var1;
    }

    if ( (step * (var1 - var2) > 0)) {
      myexit(ERROR_PARAM, "Step size for inverted intervals must be negativ, and positive for normal intervals.");
    }

    // Add long to list
    *count += labs((long)((labs(var2-var1)+1) / step)) + 1;
    (*values) = (long *)Realloc((*values), (*count) * sizeof(long));
    for (value = var1; step * value <= step * var2; value+=step) {
      (*values)[index] = value;
      index++;
    }
  }
  *count = index;

  FreeM(result, tokencount);
}

void String_makePositiveFrames(long** frames, long* count, long lastframe) {
  long i;
  long *frame = *frames;

  for (i = 0; i < *count; i++) {
    if (frame[i] < 0) {
      frame[i] += lastframe;
    }
  }
}

bool String_makeFloat(const char* paramstring, REAL* value) {
  char  *endptr = NULL;

  // reset errno to see conversion result
  errno = 0;

  *value = (REAL)strtod(paramstring, &endptr);

  if (errno != 0 || &paramstring[strlen(paramstring)] != endptr) {
    // error
    if (errno == 0) errno = EINVAL;
    return false;
  }

  // cast ok
  return true;
}

bool String_getGermanFloat(const char* str, REAL* value) {
  char *str2 = Strdup(str);

  // replace ',' if exists
  char *pos = strrchr(str2, ',');
  if (pos) {
    *pos = '.';
  }

  long rc = String_makeFloat(str2, value);
  Free(str2);

  return rc;
}

/** @brief Parses a string containing the coordinates into n float values.
  *
  * @param vectorstr string array containing the vector (like this: "[X,Y,...]")
  * @param vector    float array containing n parsed coordinates
  * @param n         number of vector elements (which should (and must) be parsed) within the string
  *
  * @return true if the analysis was successful, false otherwise
  */
bool String_makeVectorFloat(const char* vectorstr, REAL* vector, size_t n) {
  size_t  i;
  char   *endptr = NULL;

  if (*vectorstr++ != '[') {
    return false;
  }

  vector[0] = (REAL)strtod(vectorstr, &endptr);
  if (vectorstr == endptr) {
    return false;
  }
  vectorstr = endptr;

  for (i = 1; i < n; i++) {

    if (*vectorstr != ',') {
      return false;
    }
    vectorstr++;

    vector[i] = (REAL)strtod(vectorstr, &endptr);

    if (vectorstr == endptr) {
      return false;
    }
    vectorstr = endptr;
  }

  if (*vectorstr != ']' || (*(vectorstr+1)) != '\0') {
    return false;
  }

  return true;
}

bool String_makeLong(const char* paramstring, long* value) {
  char  *endptr = NULL;

  errno = 0;

  // convert to long, base = 10, or hex starting with 0x
  *value = strtol(paramstring, &endptr, 0);

  if (errno != 0 || &paramstring[strlen(paramstring)] != endptr) {
    // error
    if (errno == 0) errno = EINVAL;
    return false;
  }

  // cast ok
  return true;
}

bool String_makeUlong(const char* paramstring, unsigned long* value) {
  char  *endptr = NULL;

  errno = 0;

  // convert to unsigned long, base = 10, or hex starting with 0x
  *value = strtoul(paramstring, &endptr, 0);

  if (errno != 0 || &paramstring[strlen(paramstring)] != endptr) {
    // error
    if (errno == 0) errno = EINVAL;
    return false;
  }

  // cast ok
  return true;
}

/** @brief Parses a string containing a vector into n integer values.
  *
  * @param vectorstr  string array containing the vector elements like this: "[x0,x1,...]"
  * @param vector     integer array containing n parsed coordinates
  * @param n          number of vector elements (which should (and must) be parsed) within the string
  *
  * @return true if the analysis was successful, false otherwise
  */
bool String_makeVectorLong(const char* vectorstr, long* vector, size_t n) {
  size_t  i;
  char   *endptr = NULL;

  if (*vectorstr++ != '[') {
    return false;
  }

  vector[0] = strtol(vectorstr, &endptr, 10);

  if (vectorstr == endptr) {
    return false;
  }
  vectorstr = endptr;

  for (i = 1; i < n; i++) {

    if (*vectorstr != ',') {
      return false;
    }
    vectorstr++;

    vector[i] = strtol(vectorstr, &endptr, 10);

    if (vectorstr == endptr) {
      return false;
    }
    vectorstr = endptr;
  }

  if (*vectorstr != ']' || (*(vectorstr+1)) != '\0') {
    return false;
  }

  return true;
}

/** @brief Parses a string containing a matrixcoordinate.
 *
 * @param matrixstr    string array containing the matrixcoordinate (like this: "2x2")
 * @param matrixpoint  integer array containing parsed coordinate
 * @param n            matrix dimension (which should (and must) be parsed) within the string
 *
 * @return true if the analysis was successful, false otherwise
 */

/** @brief Convert input string to a vector with flags for periodic directions.
  *
  * @return true if the analysis was successful, false otherwise
  */

/** @brief convert the string representation to an
  * unsigned integer to perform shift operations
  * to extract the 3 color values for red, green and blue
  */
static bool String_makeColor(const char* hexcolor, unsigned long* color) {
  if (strlen(hexcolor) != 6) {
    return false;
  }

  errno = 0;
  *color = strtoul(hexcolor, NULL, 16);
  if (errno != 0) return false;
  return true;
}

/** @brief Extracts the rgb values out of a hexstring and converts them to REAL values
  *
  * @param hexcolor The hex string represnting a rgb color
  * @param rgb      The REAL array to pack the values into
  */

/** @brief Extracts the rgb values out of a hexstring and converts them to float values
  *
  * @param hexcolor The hex string represnting a rgb color
  * @param rgb      The REAL array to pack the values into
  */

/** @brief Extracts the rgb values out of a hexstring and converts them to char values
  *
  * @param hexcolor The hex string represnting a rgb color
  * @param rgb      The byte array to pack the values into
  */

/** @brief Extracts the rgb values out of a hexstring and converts them to long values
  *
  * @param hexcolor The hex string represnting a rgb color
  * @param rgb      The long array to pack the values into
  */

/** @brief Create a formated string buffer from a va_list.
  *
  * @code
  * myfunc(const char* format, ...) {
  *   va_list ap;                            // get argument list
  *   char   *infotext = NULL;
  *
  *   va_start(ap, format);
  *   String_createVAString(&infotext, format, &ap);
  *   va_end(ap);
  *   printf("%s", infotext);
  *   Free(infotext);
  * }
  * @endcode
  */
void String_createVAString(char** buffer, const char* format, va_list* ap) {
  va_list  apcopy;
  size_t   size, n;
  char    *str;

  size = MAX_LINE_LENGTH;                                      // default size for output
  str  = MallocS(size);                                        // get all memory needed
  va_copy(apcopy, *ap);                                        // this is a stack, if loop is used the stack would be empty, work with a copy
  while ((n = vsnprintf(str, size, format, apcopy)) >= size) { // and convert it into the format string
    size = n + 1;                                              // if the output has been truncated then
    str  = (char*)Realloc(str, size*sizeof(char));             // get more memory and try again
    va_end(apcopy);
    va_copy(apcopy, *ap);                                      // make a fresh copy
  }
  va_end(apcopy);

  size = n + 1;
  str  = (char*)Realloc(str, size*sizeof(char));               // reduce allocation to needed size

  if (*buffer) Free(*buffer);
  *buffer = str;                                               // return buffer
}

/** @brief Create a formated string buffer from a format string.
  *
  * @see String_createVAString
  */
void String_createFormatedString(char** buffer, const char* format, ...) {
  va_list ap;

  va_start(ap, format);
  String_createVAString(buffer, format, &ap);
  va_end(ap);
}

void String_appendFormatedVAString(char** dest, const char* format, va_list* ap) {
  char *toappend = NULL;

  String_createVAString(&toappend, format, ap);
  String_append(dest, toappend);
  Free(toappend);
}

void String_appendFormatedString(char** dest, const char* format, ...) {
  va_list ap;

  va_start(ap, format);
  String_appendFormatedVAString(dest, format, &ap);
  va_end(ap);
}

/** @brief Appends second string to first string.
  *
  * Given string is manipulated. Give at least a '\0' terminated string.
  *
  * @param dest has to be a pointer to a \\0 terminated string or a pointer to a NULL-pointer.
  * @param toappend may be NULL, in this case dest is not modified
  */
long String_append(char** dest, const char* toappend) {
#ifdef DEBUG
  if (dest == NULL) {
    myexit(ERROR_BUG, "dest is NULL but has to be at least a pointer to a \\0 terminated string.");
  }
#endif
  if (toappend == NULL) {
    return strlen(*dest);
  }

  long destlen = (*dest == NULL) ? 0 : strlen(*dest);
  long toappendlen = strlen(toappend);

  *dest = Realloc(*dest, destlen + toappendlen + 1);
  strcpy(*dest + destlen, toappend);

  return destlen + toappendlen;
}

void String_writeBlocktext(FILE* fp, size_t blocklength, const char* text) {
  // tokenize description or example text if it is not too large.
  int oldlinepos = 0;
  int newlinepos = 0;

  while (text[oldlinepos] != '\0') {
    while (text[newlinepos] != '\0' && text[newlinepos] != '\n') newlinepos++;
    if ((size_t)newlinepos-oldlinepos < blocklength) {
      fprintf(fp, "%.*s", newlinepos-oldlinepos, &text[oldlinepos]);
    } else {
      // printing the text if token with newline is too large.
      size_t linelength = 0;
      int oldtokenpos = oldlinepos;
      int newtokenpos = oldlinepos;

      while (newtokenpos < newlinepos) {
        while (newtokenpos < newlinepos && text[newtokenpos] != ' ') newtokenpos++;

        linelength += newtokenpos - oldtokenpos;
        if (linelength >= blocklength) {
          if (oldtokenpos != oldlinepos) oldtokenpos++;
          fputc('\n', fp);
          linelength = newtokenpos-oldtokenpos;
        }
        fprintf(fp, "%.*s", newtokenpos-oldtokenpos, &text[oldtokenpos]);
        oldtokenpos = newtokenpos++;
      }
    }
    oldlinepos = newlinepos++;
  }
  fputc('\n', fp);
}

#define SNPRINTF(cur_str_pos, n_, ...) \
  count += snprintf(cur_str_pos, n_, __VA_ARGS__); \
  if (count < n) { \
    cur_str_pos = str + count; \
  }

/** @brief Check if a given string matches a pattern.
  */
int String_matchRegex(const char* string, const char* pattern, bool casesensitive) {
  int status;
  regex_t re;
  int flags;
  int errorcode;

  if (casesensitive) {
    flags = REG_EXTENDED | REG_NOSUB;
  } else {
    flags = REG_EXTENDED | REG_NOSUB | REG_ICASE;
  }

  errorcode = regcomp(&re, pattern, flags);
  if (errorcode != 0) {
    char errbuf[MAX_LINE_LENGTH];
    regerror(errorcode, &re, errbuf, sizeof(errbuf));
    myexit(ERROR_BUG, "String_matchRegex(): Compilation of regex '%s' with flags %d failed (%s).", pattern, flags, errbuf);
  }
  status = regexec(&re, string, 0, NULL, 0);
  regfree(&re);

  return (status == 0);
}

void String_pandoc(const char* oldstring, char** newstring, char* format_from, char* format_to) {
  // sets default values
  if (format_from == NULL) format_from = "markdown";
  if (format_to == NULL) format_to = "html";

  char* call = "/usr/bin/pandoc";
  char* const argv[] = {call, "-f", format_from, "-t", format_to, NULL};

  int fd[2] = {0,0};
  pcreate(fd, call, argv);

  size_t length = strlen(oldstring);
  if (write(fd[1], oldstring, length) != length) {
    myexit(ERROR_BUG, "String_pandoc(): Could not write to pipe");
  }
  close(fd[1]);

  const int buf_size = 256;
  char buf[buf_size];
  int chars_read;
  do {
    memset(&buf, 0, buf_size);
    chars_read = read(fd[0], buf, buf_size - 1);
    if (!(chars_read > 0)) break;
    String_append(newstring, buf);
  } while (true);
}


