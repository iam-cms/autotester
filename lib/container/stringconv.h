/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_CONTAINER_STRINGCONV_H
#define LIB_CONTAINER_STRINGCONV_H

// switches for String_mask()
#define STRING_UNMASK 0
#define STRING_MASK   1

// switches for String_matchRegex()
#define REGEX_NOT_CASESENSITIVE 0
#define REGEX_CASESENSITIVE     1

extern const char *XML_masktable[][2];

size_t String_replace(const char* oldstring, char** newstring, const char* find, const char* replace);
void   String_mask(const char* oldstring, char **newstring, long mask, const char* stringmask[][2]);
void   String_makeTokenList (const char* list, char*** result, ulong* count, char separator); ///< separates a string into substrings
bool   String_makeFloat(const char* paramstring, REAL* value);                              ///< typecast with error msg string ->float, returns true if success
bool   String_makeVectorFloat(const char* vectorstr, REAL* vector, size_t n);               ///< get a vector array from a string, accepts eg.. [3.3,5.1,6]
bool   String_makeLong(const char* paramstring, long* value);                               ///< typecast with error msg string ->long, returns true if success
bool   String_makeVectorLong(const char* coordstr, long* coord, size_t n);                  ///< gets a coordinates array from a string, accepts eg.. [3,5,6]
void   String_makeDynamicLongList(const char* list, long** values, long* count);

void   String_createVAString(char** buffer, const char* format, va_list* ap) __attribute__ ((format (printf, 2, 0)));
void   String_createFormatedString(char** buffer, const char* format, ...) __attribute__ ((format (printf, 2, 3)));
void   String_appendFormatedVAString(char** dest, const char* format, va_list* ap) __attribute__ ((format (printf, 2, 0)));
void   String_appendFormatedString(char** dest, const char* format, ...) __attribute__ ((format (printf, 2, 3)));
long   String_append(char** dest, const char* toappend);

void   String_writeBlocktext(FILE* fp, size_t blocklength, const char* text);

int    String_matchRegex(const char* string, const char* pattern, bool casesensitive);        ///< Check if a given string matches a pattern

/** @brief Process a markdown string (or other formats) using pandoc
  */
void   String_pandoc(const char* oldstring, char** newstring, char* format_from, char* format_to);

#endif
