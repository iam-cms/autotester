/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dirent.h>
#include <locale.h>

#include <exception.h>
#include <wrapper.h>


/** Number of the boundary conditions where
      [0]=x(left),   [1]=x(right),
      [2]=y(bottom), [3]=y(top),
      [4]=z(back),   [5]=z(front)
  */
const char *boundary_str[6] = {
  "x(left), ",   "x(right), ",
  "y(bottom), ", "y(top), ",
  "z(back), ",   "z(front)."
};

/** array of strings identifing a side of the cube
  */
const char *side_str[6] = {
  "left",   "right",
  "bottom", "top",
  "back",   "front"
};

/** for use of printfm/printim
  */
char *direction_str[3] = {
  "x", "y", "z"
};

char  *progname;

/** @brief
  */
void fprintc(FILE* fp, long color1, long color2, const char* fmt, ...) {
  int         size, n;
  char       *str;
  const char *fmt_i18n = GETTEXT(fmt);

  va_list ap;
  size = MAX_LINE_LENGTH;
  str  = (char*)Malloc(size);
  while (true) {
    va_start(ap, fmt);
    n = vsnprintf(str, size, fmt_i18n, ap);
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {
      fprintf(fp, "\033[%ld;%ldm%s\033[0m\n", color1, color2, str);
      break;
    }
    size = n + 1;
    str  = (char*)Realloc(str, size);
  }
  free(str);                             /* use free not Free as its our own pointer */
}

/** @brief Print out an error message and exit with exit code and take a format string.
  */
void myexit(int exitcode, const char* const fmt, ...) {
  int         size, n;
  char       *str;
  const char *fmt_i18n = GETTEXT(fmt);

#ifndef DEBUG
  if (exitcode == ERROR_BUG)
#else
  if (exitcode != ERROR_SIGSEGV)
#endif
  {
    ucontext_t ucontext;
    mywarn("A bug was triggered. Stack trace is following:");
    getcontext(&ucontext);
    printBacktrace(&ucontext, 2);
  }

  va_list ap;                               /* get argument list                        */
  size = MAX_LINE_LENGTH;                   /* default size for output                  */
  str  = (char*)malloc(size);               /* get all memory needed                    */
  while (true) {
    if (str == NULL) {                      /* Malloc uses myexit so extra check here   */
      printwarn("internal error: not enough memory");
      break;
    }
    va_start(ap, fmt);                      /* get next argument from the list          */
    n = vsnprintf(str, size, fmt_i18n, ap); /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                         /* if the output hasn't been truncated then */
#ifdef INSIDESOLVER  // only used inside the solver
#ifdef MPI
      int rank;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      if (pinfo.logfp && pinfo.logfp != stderr) {  /* write message to a log file    */
        fprintf(pinfo.logfp, "\n (EE) (Node %i) : %s : %s\n\n", rank, sprinttime(), str);
        fflush(pinfo.logfp);
      }
      fprintf(stderr, "\n \033[1;31m(EE)\033[30;0m \033[01m(Node %i)\033[0m : %s : \033[1;31m%s\033[0m\n\n",
              rank, sprinttime(), str);
#else
      if (pinfo.logfp && pinfo.logfp != stderr) {  /* write message to a log file    */
        fprintf(pinfo.logfp, "\n (EE) %s : %s\n\n", sprinttime(), str);
        fflush(pinfo.logfp);
      }                                  /* and to stdout                            */
      fprintf(stderr, "\n \033[1;31m(EE)\033[0m %s : \033[1;31m%s\033[0m\n\n", sprinttime(), str);
#endif
      mymessage(MESSAGE_FEW, "SOLVER ABORT (exit code %d): %s", exitcode, str); // send a message to the user
#else  // outside of the solver
      fprintf(stderr, "\n \033[1;31m(EE) %s\033[0m\n\n", str);
#endif
      free(str);                         /* use free not Free as its our own pointer */
      break;
    }
    size = n + 1;
    str  = (char*)realloc(str, size);    /* or get more memory and try again         */
  }
#ifdef INSIDESOLVER  // only used inside the solver
  printProcessUsageTime();               /* get system and process time parameters   */
  /* close logfile before exiting to ensure all messages will be written to file     */
  if ((pinfo.logfp != NULL) && (pinfo.logfp != stdout)) {
    if (fclose(pinfo.logfp) != 0) {      /* do not use FClose because it uses myexit */
      printwarn("could not close log-file");
    }
  }
#endif
#ifdef MPI
  MPI_Abort(MPI_COMM_WORLD, exitcode);
#endif
  exit(exitcode);                        /* bye bye and fade away with exit code     */
}

/** @brief Print out a formated info message. (MPI: only rank 1)
  *
  * @sa printinfo
  */
void myinfo(const char* const fmt, ...) {
  int         size, n;
  char       *str;
  const char *fmt_i18n = GETTEXT(fmt);

  va_list ap;                               /* get argument list                        */
  size = MAX_LINE_LENGTH;                   /* default size for output                  */
  str  = (char*)malloc(size);               /* get all memory needed                    */

  while (str != NULL) {
    va_start(ap, fmt);                      /* get next argument from the list          */
    n = vsnprintf(str, size, fmt_i18n, ap); /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                         /* if the output hasn't been truncated then */
      printinfo(str);                       /* print info                               */
      free(str);                            /* use free not Free as its our own pointer */
      return;
    }
    size = n + 1;
    str  = (char*)realloc(str, size);       /* or get more memory and try again         */
  }

  /* if we reach this point we got not enough memory */
  printwarn("internal error: not enough memory");
}

/** @brief Print out a formated warning.
  *
  * @sa printwarn
  */
void mywarn(const char* const fmt, ...) {
  int     size, n;
  char   *str;
  const char *fmt_i18n = GETTEXT(fmt);

  va_list ap;                            /* get argument list                        */
  size = MAX_LINE_LENGTH;                /* default size for output                  */
  str  = (char*)malloc(size);            /* get all memory needed                    */

  while (str != NULL) {
    va_start(ap, fmt);                   /* get next argument from the list          */
    n = vsnprintf(str, size, fmt_i18n, ap);   /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                      /* if the output hasn't been truncated then */
      printwarn(str);                    /* print warning                            */
      free(str);                         /* use free not Free as its our own pointer */
      return;
    }
    size = n + 1;
    str  = (char*)realloc(str, size);    /* or get more memory and try again         */
  }

  /* if we reach this point we got not enough memory */
  printwarn("internal error: not enough memory");
}

/** @brief Print out a formated message for debugging.
  *
  */
void mydebug_internal(const char *file, int line, const char *func, const char* const fmt, ...) {
  int     size, n;
  char   *str;

  va_list ap;                            /* get argument list                        */
  size = MAX_LINE_LENGTH;                /* default size for output                  */
  str  = (char*)malloc(size);            /* get all memory needed                    */

  while (str != NULL) {
    va_start(ap, fmt);                   /* get next argument from the list          */
    n = vsnprintf(str, size, fmt, ap);   /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                      /* if the output hasn't been truncated then */
#ifdef INSIDESOLVER  // only used inside the solver

#ifdef MPI

      if (pinfo.logfp && pinfo.logfp != stderr) {
        fprintf(pinfo.logfp, " (DD) (Node %i) : %s : (%20s:% 4d)%s() %s\n",
                mpiinfo.world.rank, sprinttime(),
                file, line, func, str);
        fflush(pinfo.logfp);
      }
      fprintf(stderr, "\033[44;1m (DD) (Node %i) : %s : (%20s:% 4d)%s() %s\033[0m\n",
              mpiinfo.world.rank, sprinttime(),
              file, line, func, str);

#else

      if (pinfo.logfp && pinfo.logfp != stderr) {
        fprintf(pinfo.logfp, " (DD) %s : (%20s:% 4d)%s() %s\n",
                sprinttime(), file, line, func, str);
        fflush(pinfo.logfp);
      }
      fprintf(stderr, "\033[44;1m (DD) %s : (%20s:% 4d)%s() %s \033[30;0m\n",
              sprinttime(), file, line, func, str);

#endif

#else  // outside of the solver
      fprintf(stderr, "\033[44;1m (DD) (%20s:% 4d)%s() %s\033[30;0m\n",
              file, line, func, str);
#endif
      free(str);                         /* use free not Free as its our own pointer */
      return;
    }
    size = n + 1;
    str  = (char*)realloc(str, size);    /* or get more memory and try again         */
  }

  /* if we reach this point we got not enough memory */
  printwarn("internal error: not enough memory");
}

/** @brief Send messages to the user by calling a message-script.
  *
  * level:@n
  * - MESSAGE_FEW only start stop and exit will be send
  * - MESSAGE_ALL all available messages will be send
  */
#ifdef INSIDESOLVER
void mymessage(long level, const char* const fmt, ...) {
  int         callsize, size, n;
  char       *call, *str;
  const char *fmt_i18n = GETTEXT(fmt);

  va_list ap;                                     /* get argument list                        */

  if (pinfo.messagescript == NULL || level > pinfo.messagelevel) {
    return;
  }

  size = MAX_LINE_LENGTH;                         /* default size for output                  */
  callsize = strlen(pinfo.messagescript) + strlen(pinfo.basename) + 11;
  call = (char*)malloc(callsize+size+2);          /* get all memory needed                    */
  snprintf(call, callsize, "%s \"SIM: %s : ", pinfo.messagescript, pinfo.basename);
  str = call + callsize - 1;

  while (str != NULL) {
    va_start(ap, fmt);                            /* get next argument from the list          */
    n = vsnprintf(str, size, fmt_i18n, ap);       /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                               /* if the output hasn't been truncated then */
      strcat(str, "\"&");
      int rc = System(call);                      /* sending message                          */
      if (rc != 0) {     // != 0 aborting
//         if (rc == 32512) {
          mywarn("Disabling messaging service (Could not run command '%s').", pinfo.messagescript);
          Free(pinfo.messagescript);
//         }
      }
      free(call);                                 /* use free not Free as its our own pointer */
      return;
    }
    size = n + 1;
    call = (char*)realloc(call, callsize+size+2); /* or get more memory and try again */
    str = call + callsize - 1;
  }

  /* if we reach this point we got not enough memory */
  printwarn("internal error: not enough memory");
}
#endif

long verbose = VERBOSE_NORMAL;

/** @brief Print out a formated string dependent on the current verbose level.
  *
  * @sa printverbose
  *
  * @param msglevel
  *     The minimum level when the current message is printed ( > 0 ).
  * @param verboselevel
  *     The verboselevel of the current program. level 0 means no output.
  * @param fmt
  *     Format string like in printf()
  *
  *  A message will be printed to stderr if msglevel <= verboselevel.
  *  Additional user levels can be defined in this manner.
  *
  *  level                 description @n
  *  - 0 (VERBOSE_QUIET)   no messages are printed
  *  - 1 (VERBOSE_NORMAL)  print informative messages
  *  - 2 (VERBOSE_CONTROL) print control messages of process flow
  *  - 3 (VERBOSE_DEBUG)   print additional debug messages
  */
void myverbose(ulong msglevel, ulong verboselevel, const char* const fmt, ...) {
  int         size, n;
  char       *str;
  const char *fmt_i18n = GETTEXT(fmt);

  if (msglevel > verboselevel) return;

  va_list ap;                               /* get argument list                        */
  size = MAX_LINE_LENGTH;                   /* default size for output                  */
  str  = (char*)malloc(size);               /* get all memory needed                    */
  while (str != NULL) {
    va_start(ap, fmt);                      /* get next argument from the list          */
    n = vsnprintf(str, size, fmt_i18n, ap); /* and convert it into the format string    */
    va_end(ap);
#ifdef DEBUG
    if (n < 0) {
      myexit(ERROR_BUG, "error on vsnprintf, fmt string=%s\n", fmt);
    }
#endif
    if (n < size) {                         /* if the output hasn't been truncated then */
      printverbose(str);                    /* print verbose                            */
      free(str);                            /* use free not Free as its our own pointer */
      return;
    }
    size = n + 1;
    str  = (char*)realloc(str, size);       /* or get more memory and try again         */
  }
  /* if we reach this point we got not enough memory */
  printwarn("internal error: not enough memory");
}

/** @brief Create recursively a directory if it does not already exist.
 */
int Mkdir(const char* const pathname, mode_t mode) {
  int         rc;
  struct stat dirstat;

  if ((rc = mkdir(pathname, mode)) != 0) {
    if (errno == EEXIST) {
      memset(&dirstat, 0x00, sizeof(dirstat));
      Stat(pathname, &dirstat);
      if (S_ISDIR(dirstat.st_mode)) {
        errno = 0;
        return 0;
      }
    } else if (errno == ENOENT) {
      char *copypathname = Strdup(pathname);
      char *subpathname  = dirname(copypathname);

      if (strcmp(subpathname, ".") == 0) {
        Free(copypathname);
        errno = 0;
        return 0;
      }
      rc = Mkdir(subpathname, mode);
      Free(copypathname);
      if (rc != 0) {
        return rc;
      }
      rc = Mkdir(pathname, mode);
    }
    return rc;
  }

  return 0;
}

/** @brief Remove recursively a directory and its contents.
  */
void Remove(const char* const dirname) {
  struct stat statbuf;
  char filename[PATH_MAX];
  DIR           *dir;
  struct dirent *entry;

  if ((dir = opendir(dirname)) == NULL) {
    myexit(ERROR_FILEIO, "Could not delete directory '%s' (%s).", dirname, strerror(errno));
  }
  while ((entry = readdir(dir)) != NULL) {
    snprintf(filename, sizeof(filename), "%s/%s", dirname, entry->d_name);
    /* get file informations */
    Stat(filename, &statbuf);
    if (S_ISDIR(statbuf.st_mode)) {
      if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
        Remove(filename);
      }
    } else {
      Unlink(filename);
    }
  }
  closedir(dir);

  rmdir(dirname);
}

/** @brief Check if file is writable.
  *
  * @return true if file is writable or is createable, else false
  */
bool checkFileWritable(const char* const filename) {
  if ( (access(filename, F_OK )) == -1 ) {
    // File does not exist, so we will check if we
    // could create a new one in this directory

    char *path = Strdup(filename);

    long rc = checkFileWritable(dirname(path));

    Free(path);

    return rc;
  }

  // file does exist
  // is it writable
  return ( access(filename, W_OK) != -1 );
}

void AtExit(void (*function)(void)) {
  if (atexit(function) != 0) {
    myexit(EXIT_ERROR, "error calling atexit() function. (%s).", strerror(errno));
  }
}

/** @brief Calculate the time between two calls.
  */
void measureCPUTime(REAL* systemtime, REAL* usertime, REAL* totaltime) {
  struct rusage selfusage;
  struct rusage childusage;
  REAL          user;
  REAL          sys;

  /* what time did parent take */
  if (getrusage(RUSAGE_SELF,&selfusage) < 0) {
    printwarn("internal error: getrusage error.");
    return;
  }
  /* what time did children take */
  if (getrusage(RUSAGE_CHILDREN,&childusage) < 0) {
    printwarn("internal error: getrusage error.");
    return;
  }
  /* sum up my cpu time taken and the time the children took */
  user  = (double) selfusage.ru_utime.tv_sec +  selfusage.ru_utime.tv_usec/1000000.0;
  user += (double)childusage.ru_utime.tv_sec + childusage.ru_utime.tv_usec/1000000.0;
  /* sum up the time the system took handeling parent and his children */
  sys   = (double) selfusage.ru_stime.tv_sec +  selfusage.ru_stime.tv_usec/1000000.0;
  sys  += (double)childusage.ru_stime.tv_sec + childusage.ru_stime.tv_usec/1000000.0;
  /* subtract old time from new time to get the time take since the last call */
  *systemtime = sys-(*systemtime);
  *usertime   = user-(*usertime);
  *totaltime  = (*systemtime)+(*usertime);
}

/** @brief Print information about cpu time taken from system and user space.
  */
void printProcessUsageTime(void) {
  REAL systemtime = 0.0;
  REAL usertime   = 0.0;
  REAL totaltime  = 0.0;

  measureCPUTime(&systemtime, &usertime, &totaltime);

  /* print out the time */
#ifdef MPI
#ifdef INSIDESOLVER
  if (mpiinfo.world.rank < 2 || verbose >= VERBOSE_CONTROL)
#else
  if (mpiinfo.world.rank < 1 || verbose >= VERBOSE_CONTROL)
#endif
#endif
  if (verbose >= VERBOSE_NORMAL) {
    myinfo("system time = %"REALLENGTH"gs, user time = %"REALLENGTH"gs, total time = %"REALLENGTH"gs", systemtime, usertime, totaltime);
  }
}

/** @brief Make human readable time from double in the format "DDd hh:mm:ss".
  */
const char* makehumanreadabletime(double eta) {
  static char rtime[20];
  long day;
  long hour;
  long min;
  long sec;

  if (eta < 0.0) {
    snprintf(rtime, sizeof(rtime), "   -d --:--:--");
  } else {

#if 0
    long _eta = (long)floor(eta);
    sec = _eta%60;
    _eta /= 60;
    min = _eta%60;
    _eta /= 60;
    hour = _eta%24;
    day = _eta/24;
#else
    ldiv_t dseconds = ldiv((long)floor(eta), 60);
    sec = dseconds.rem;
    ldiv_t dminutes = ldiv(dseconds.quot, 60);
    min = dminutes.rem;
    ldiv_t dhour = ldiv(dminutes.quot, 24);
    hour = dhour.rem;
    day = dhour.quot;
#endif
    snprintf(rtime, sizeof(rtime), "%4ldd %02ld:%02ld:%02ld"/*.%.3ld"*/, day, hour, min, sec); // , (long)((eta-floor(eta))*1000));
  }
  return rtime;
}

/** @brief Return a formated time string with the local time in a human readable manner.
  */
const char* sprinttime(void) {
  time_t       t;
  static char  datestr[27];

  time(&t);
  strftime(datestr, sizeof(datestr), "%a, %d. %b %Y %H:%M:%S", localtime(&t));
  return datestr;
}
