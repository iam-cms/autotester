/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_H
#define LIB_WRAPPER_H


#include <errno.h>

#ifdef __cplusplus
#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>
#include <cstdlib>
extern "C" {
#else
#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#endif

#include <ctype.h>
#include <limits.h>

#include <sys/types.h>
/* fstat */
#include <sys/stat.h>
#include <unistd.h>
#include <libgen.h>

/* memset */
#include <string.h>

/* open */
#include <fcntl.h>

#include <float.h>
#include <math.h>

#ifdef OPENMP
#include <omp.h>
#endif

#ifdef _FREEBSD
#define ulong unsigned long
#define uint unsigned int
#endif

// data type size
#if 0
  #define REAL long double
  #define REALLENGTH "L"
  #define PACE_MPI_REAL MPI_LONG_DOUBLE
  #define REALEPSILON LDBL_EPSILON
#elif 1
  #define REAL double
  #define REALLENGTH "l"
  #define PACE_MPI_REAL MPI_DOUBLE
  #define REALEPSILON DBL_EPSILON
#else
  #define REAL float
  #define REALLENGTH ""
  #define PACE_MPI_REAL MPI_FLOAT
  #define REALEPSILON FLT_EPSILON
#endif

// helpers for booleans (represented through ints)
// fix until we use this macro nomore
#ifndef TRUE
#define TRUE                            1
#endif
#ifndef FALSE
#define FALSE                           0
#endif

#if !defined(__cplusplus) && !defined(__clang__)
#ifndef inline
#  define inline __inline__ __attribute__ ((always_inline))
#endif
#endif

/// maximum size of buffers for textlines
#define MAX_LINE_LENGTH              2048

/// do not print warning (gcc) when function parameter is not used
#define UNUSED(x) x __attribute__ ((__unused__))

#define likely(x)   __builtin_expect(!!(x),1)
#define unlikely(x) __builtin_expect(!!(x),0)

// Translate a Macro to a string
#define STRING(s) INTERNAL_STRING(s)
#define INTERNAL_STRING(s) #s

// Must be three calls to expand the macros correctly
#define __INTERNAL_UNIQUE_2(a, b, c) a ## b ## c
#define __INTERNAL_UNIQUE_1(a, b, c) __INTERNAL_UNIQUE_2(a, b, c)
#define UNIQUE __INTERNAL_UNIQUE_1(__func__, __LINE__, __COUNTER__)

#define MIN(x,y) (((x)<(y)) ? (x) : (y))
#define MAX(x,y) (((x)>(y)) ? (x) : (y))

#define SWAP(a, b)  do { a ^= b; b ^= a; a ^= b; } while ( 0 )


// helpers for coordinates
#ifdef __cplusplus
const int X = 0;
const int Y = 1;
const int Z = 2;
const int UNSPECIFIED = -1;
#else
#define X                               0
#define Y                               1
#define Z                               2
#define UNSPECIFIED                    -1
#endif

#if __APPLE__
typedef unsigned char           unchar;
typedef unsigned short          ushort;
typedef unsigned int            uint;
typedef unsigned long           ulong;
#endif

extern const char *boundary_str[6];
extern const char *side_str[6];
extern char       *direction_str[3];

extern char *progname;


/* functions with variable parameters can not be inlined */
void fprintc(FILE* fp, long color1, long color2, const char* fmt, ...) __attribute__ ((format (printf, 4, 5)));
void myexit(int exitcode, const char *fmt, ...) __attribute__ ((noreturn, format (printf, 2, 3)));
void myinfo(const char* const fmt, ...) __attribute__ ((format (printf, 1, 2)));

void mywarn(const char* const fmt, ...) __attribute__ ((format (printf, 1, 2)));
void mydebug_internal(const char *file, int line, const char *func, const char* const fmt, ...) __attribute__ ((format (printf, 4, 5)));

#define mydebug(...) mydebug_internal(__FILE__, __LINE__, __func__, __VA_ARGS__)

#define MESSAGE_FEW     0
#define MESSAGE_ALL     1

void mymessage(long level, const char* const fmt, ...) __attribute__ ((format (printf, 2, 3)));

#define VERBOSE_QUIET   0
#define VERBOSE_NORMAL  1
#define VERBOSE_CONTROL 2
#define VERBOSE_DEBUG   3

extern long verbose;

void  myverbose(ulong msglevel, ulong verboselevel, const char* const fmt, ...) __attribute__ ((format (printf, 3, 4)));


#ifdef INTERNATIONALIZATION

#include <libintl.h>
#include <locale.h>

#define _(String) gettext(String)
#define gettext_noop(String) String
#define N_(String) gettext_noop(String)

#define GETTEXT(String) gettext(String)

#define I18N   \
  setlocale(LC_MESSAGES, ""); \
  setlocale(LC_TIME, ""); \
  bindtextdomain("pace3d", STRING(PACE3D_DIR) "/locale"); \
  bind_textdomain_codeset("pace3d", "UTF-8"); \
  textdomain("pace3d");

#else //INTERNATIONALIZATION

#define GETTEXT(String) String

#define I18N /**/

#endif //INTERNATIONALIZATION


int   Mkdir(const char* const pathname, mode_t mode);
void  Remove(const char* const dirname);
bool  checkFileWritable(const char* const filename);
void  AtExit(void (*function)(void));

void  measureCPUTime(REAL* systemtime, REAL* usertime, REAL* totaltime);
void  printProcessUsageTime(void);

#ifdef TIMEMEASUREMENT
#define START_TIMEMEASUREMENT(name)   struct timeval __FILE__##__func__##name##actualtime; \
gettimeofday(&__FILE__##__func__##name##actualtime, NULL); \
double __FILE__##__func__##name##s_time = (double)__FILE__##__func__##name##actualtime.tv_sec+((double)__FILE__##__func__##name##actualtime.tv_usec/1000000.0); \
do{} while (0)

#define END_TIMEMEASUREMENT(name, res) gettimeofday(&__FILE__##__func__##name##actualtime, NULL); \
res = (double)__FILE__##__func__##name##actualtime.tv_sec+((double)__FILE__##__func__##name##actualtime.tv_usec/1000000.0) -__FILE__##__func__##name##s_time; \
do{} while (0)
#else
#define START_TIMEMEASUREMENT(name)
#define END_TIMEMEASUREMENT(name, res)
#endif

const char* makehumanreadabletime(double eta);

const char* sprinttime(void);

#include "wrapper/errorcodes.h"

#include "wrapper/mem.h"
#include "wrapper/fileio.h"
#include "wrapper/bitfield.h"
#ifdef MPI
#include "wrapper/mpiwrapper.h"
#endif

#include "exception.h"
#include "wrapper/stdio.h"


#ifdef INSIDESOLVER
#define PACE3DMAIN(x) \
do { \
  progname=x; \
  setupSignals(EXCEPTION_SIGSEGV); \
  setupParameterTypes(); \
  I18N \
} while (0)
#else
#ifdef MPI
#define PACE3DMAIN(x) \
do { \
  progname=x; \
  setupSignals(EXCEPTION_SIGSEGV); \
  setupParameterTypes(); \
  setupMPI(&argc, &argv); \
  I18N \
  AtExit(deinitMPI); \
  AtExit(printProcessUsageTime); \
} while (0)
#else
#define PACE3DMAIN(x) \
do { \
  progname=x; \
  setupSignals(EXCEPTION_SIGSEGV); \
  setupParameterTypes(); \
  I18N \
  AtExit(printProcessUsageTime); \
} while (0)
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif
