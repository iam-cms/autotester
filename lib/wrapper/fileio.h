/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_FILEIO_H
#define LIB_WRAPPER_FILEIO_H

static inline FILE* FOpen(const char* filename, const char* mode) {
  FILE *fp;

  if ((fp = fopen(filename,mode)) == NULL) {
    myexit(ERROR_FILEIO, "FOpen(): error during opening file \"%s\" with mode [%s] (%s).", filename, mode, strerror(errno));
  }
  return fp;
}

static inline void  FClose(FILE* fp) {
  if (fclose(fp) != 0) {
    mywarn("error: closing file with fclose() (%s).", strerror(errno));
  }
}


static inline void  Stat(const char* filename, struct stat* buf) {
  if (stat(filename, buf) < 0) {
    myexit(ERROR_FILEIO, "Stat error for file '%s' (%s).", filename, strerror(errno));
  }
}

static inline char* Getcwd(char* dirname, size_t dirNameSize) {
  char *cwd;
  if ( (cwd = getcwd(dirname, dirNameSize)) == NULL ) {
    myexit(ERROR_FILEIO, "error calling Getcwd() (%s).", strerror(errno));
  }
  return cwd;
}

/** @brief Delete a file if it exists.
  */
static inline void Unlink(const char* pathname) {
  if (unlink(pathname) < 0) {
    if (errno != 2) {  /* no error if file does not exist */
      myexit(ERROR_FILEIO, "error calling Unlink(%s) (%s).", pathname, strerror(errno));
    } else {
      errno = 0;
    }
  }
}

/** @brief Check if file exist.
  */
static inline long checkFileAccess(const char* filename) {
  if (access(filename, F_OK) == -1) {
    return false;
  }
  return true;
}

static inline int System(const char* commando) {
  if (system(NULL) == 0) {
    myexit(ERROR_FILEIO, "System call '%s' could not be executed (no shell available).", commando);
  }

  return system(commando);
}

/** @brief Run a command in a separate process and open a pipe for communication. Use fds[0] to read from the child process, and fds[1] to write to it.
  *
  * @param fds Array of two integers
  * @param command Name of the executable
  * @param argv List of arguments. Must start with the name of the executable (same as path) and end with NULL.
  *
  * https://stackoverflow.com/a/3884402
  */
static inline pid_t pcreate(int fds[2], const char* command, char* const* argv) {

  pid_t pid;
  int pipes[4];

  if (pipe(&pipes[0]) == -1) { /* Parent read/child write pipe */
    myexit(ERROR_FILEIO, "error calling pcreate(%s) for parent process (%s).", command, strerror(errno));
  }
  if (pipe(&pipes[2]) == -1) { /* Child read/parent write pipe */
    myexit(ERROR_FILEIO, "error calling pcreate(%s) for child process (%s).", command, strerror(errno));
  }

  if ((pid = fork()) > 0) {
    /* Parent process */
    fds[0] = pipes[0];
    fds[1] = pipes[3];

    close(pipes[1]);
    close(pipes[2]);

    return pid;

  } else {
    /* Child process */
    close(pipes[0]);
    close(pipes[3]);

    dup2(pipes[2], STDIN_FILENO);
    dup2(pipes[1], STDOUT_FILENO);
    execv(command, argv);
  }
  return -1;
}

#endif
