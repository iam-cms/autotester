/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_BITFIELD_H
#define LIB_WRAPPER_BITFIELD_H

/** @brief Get the number of bytes for a given number of bits for a bitfield.
 *
 */
static inline size_t getBitfieldBytes(size_t bits) {
  return (bits>>3) + ((bits % 8) ? 1 : 0);
}

/** @brief Allocate memory for a bitfield. All bits will be zero.
  *
  * @return a char pointer to the bitfield with 8 bits in a char
  */
static inline char* mallocBitfield(size_t bits) {
  return MallocS(getBitfieldBytes(bits));
}

/** @brief Set the specified bit to 1.
  *
  */
static inline void setBit(char* bitfield, size_t bit) {
  bitfield[bit>>3] |= 1<<(bit&7);
}

/** @brief Get the value of the specified bit.
  *
  * @return 1 if the bit is set, 0 if it was not set.
  */
static inline bool getBit(char* bitfield, size_t bit) {
  return ((bitfield[bit>>3] & (1<<(bit&7)))) ? 1 : 0;
}

#endif
