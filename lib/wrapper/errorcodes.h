/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_ERRORCODES_H
#define LIB_WRAPPER_ERRORCODES_H


#define OK                               1
#define EXIT_OK                          0
#define USER_EXIT                       -1
#define EXIT_ERROR                      -2

#define ERROR_PARAM                    -10
#define ERROR_INIT                     -11
#define ERROR_SIGNAL                   -20
#define ERROR_SIGSEGV                  -21
#define ERROR_FILEIO                   -30
#define ERROR_NUMERIC                  -40
#define ERROR_MPI                      -50
#define ERROR_INFILE                   -60
#define ERROR_RESPAWN                  -70

#define ERROR_SIMGEO                  -300
#define ERROR_SCALARDATA              -400
#define ERROR_VECTORDATA              -500
#define ERROR_BLOCKDATA               -600

#define ERROR_STREAM                  -900
#define ERROR_FILEHEADER              -901
#define ERROR_PIPELINE                -902
#define ERROR_SIMBENCH               -1000

#define ERROR_MENU                  -11000

#define ERROR_INTERNAL              -99999
#define ERROR_BUG                  -999999

#endif
