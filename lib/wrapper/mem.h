/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_MEM_H
#define LIB_WRAPPER_MEM_H

static inline __attribute__ ((malloc)) void* Malloc_internal(size_t size, const char* const file, int line, const char* const function) {
  void *ptr;

#ifdef DEBUG
  if (size == 0) {
    mydebug("%s() (File: %s Line: %d): trying to allocate 0 bytes.", function, file, line);
    return NULL;
  }
#endif
#ifdef VALGRIND_DEBUG
  size += 8 - (size % 8);
  if ((ptr=calloc(size, 1)) == NULL) {
#else
  if ((ptr=malloc(size)) == NULL) {
#endif
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): error allocating %zd bytes.", function, file, line, size);
  }

  return ptr;
}

#define Malloc(size) Malloc_internal(size, __FILE__, __LINE__, __func__)

static inline void* Realloc_internal(void* ptr, size_t size, const char* const file, int line, const char* const function) {
#ifdef VALGRIND_DEBUG
  size += 8 - (size % 8);
#endif
  if ((ptr=realloc(ptr,size)) == NULL) {
#ifdef DEBUG
    if (size == 0) {
      mydebug("%s() (File: %s Line: %d): trying to reallocate 0 bytes.", function, file, line);
      return NULL;
    }
#endif
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): error reallocating %zd bytes.", function, file, line, size);
  }

  return ptr;
}

#define Realloc(ptr, size) Realloc_internal(ptr, size, __FILE__, __LINE__, __func__)

static inline __attribute__ ((malloc)) void* Calloc_internal(size_t n, size_t size, const char* const file, int line, const char* const function) {
  void *ptr;

#ifdef DEBUG
  if (size == 0) {
    mydebug("%s() (File: %s Line: %d): trying to allocate 0 bytes.", function, file, line);
    return NULL;
  }
#endif
#ifdef VALGRIND_DEBUG
  size += 8 - (size % 8);
#endif
  if ((ptr=calloc(n,size)) == NULL) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): error allocating %zd blocks with %zd bytes (%zd bytes total).", function, file, line, n, size, n*size);
  }

  return ptr;
}

#define Calloc(n, size) Calloc_internal((n), size, __FILE__, __LINE__, __func__)

#define MallocFV(n)     (REAL*)Calloc((n), sizeof(REAL))
#define MallocIV(n)     (long*)Calloc((n), sizeof(long))
#define MallocULV(n)   (ulong*)Calloc((n), sizeof(ulong))
#define MallocS(n)      (char*)Calloc((n), sizeof(char))


static inline __attribute__ ((malloc)) void** MallocM_internal(size_t n, size_t m, size_t typesize, size_t ptrsize, const char* const file, int line, const char* const function) {
  size_t   i;
  void   **ptr;

  ptr = (void**)Malloc_internal(n*ptrsize, file, line, function);
  for (i=0; i<n; i++) {
    ptr[i] = (void*)Calloc_internal(m, typesize, file, line, function);
  }
  return ptr;
}

#define MallocM(n, m, type) (type**)MallocM_internal(n, m, sizeof(type), sizeof(type*), __FILE__, __LINE__, __func__)

#define MallocFM(n, m) MallocM(n, m, REAL)
#define MallocIM(n, m) MallocM(n, m, long)
#define MallocSM(n, m) MallocM(n, m, char)


static inline __attribute__ ((malloc)) void*** Malloc3M_internal(size_t n, size_t m, size_t o, size_t typesize, size_t ptrsize, size_t ptrsize2, const char* const file, int line, const char* const function) {
  size_t    i;
  void   ***ptr;

  ptr = (void***)Malloc_internal(n*ptrsize2, file, line, function);
  for (i=0; i<n; i++) {
    ptr[i] = MallocM_internal(m, o, typesize, ptrsize, file, line, function);
  }

  return ptr;
}

#define Malloc3M(n, m, o, type) (type***)Malloc3M_internal(n, m, o, sizeof(type), sizeof(type*), sizeof(type**), __FILE__, __LINE__, __func__)

#define MallocF3M(n, m, o) Malloc3M(n, m, o, REAL)
#define MallocI3M(n, m, o) Malloc3M(n, m, o, long)
#define MallocS3M(n, m, o) Malloc3M(n, m, o, char)


static inline __attribute__ ((malloc)) void**** Malloc4M_internal(size_t n, size_t m, size_t o, size_t p, size_t typesize, size_t ptrsize, size_t ptrsize2, size_t ptrsize3, const char* const file, int line, const char* const function) {
  size_t     i;
  void   ****ptr;

  ptr = (void****)Malloc_internal(n*ptrsize3, file, line, function);
  for (i=0; i<n; i++) {
    ptr[i] = Malloc3M_internal(m, o, p, typesize, ptrsize, ptrsize2, file, line, function);
  }

  return ptr;
}

#define Malloc4M(n, m, o, p, type) (type****)Malloc4M_internal(n, m, o, p, sizeof(type), sizeof(type*), sizeof(type**), sizeof(type***), __FILE__, __LINE__, __func__)

#define MallocF4M(n, m, o, p) Malloc4M(n, m, o, p, REAL)
#define MallocI4M(n, m, o, p) Malloc4M(n, m, o, p, long)
#define MallocS4M(n, m, o, p) Malloc4M(n, m, o, p, char)


static inline void Free_internal(void* ptr, const char* const file, int line, const char* const function) {
  if (ptr == NULL) {
    mydebug("%s() (File: %s Line: %d): pointer already freed.", function, file, line);
  } else {
    free(ptr);
  }
}

#define Free(ptr) \
do { \
  Free_internal(ptr, __FILE__, __LINE__, __func__); \
  ptr = NULL; \
} while (0)


static inline void FreeM_internal(void** ptr, size_t n, const char* const file, int line, const char* const function) {
  size_t i;

  if (ptr == NULL) {
    mydebug("%s() (File: %s Line: %d): pointer already freed.", function, file, line);
  } else {
    for (i=0; i<n; i++) {
      Free_internal(ptr[i], file, line, function);
    }
    free(ptr);
  }
}

#define FreeM(ptr, n) \
do { \
  FreeM_internal((void**)ptr, n, __FILE__, __LINE__, __func__); \
  ptr = NULL; \
} while (0)


static inline void Free3M_internal(void*** ptr, size_t n, size_t m, const char* const file, int line, const char* const function) {
  size_t i;

  if (ptr == NULL) {
    mydebug("%s() (File: %s Line: %d): pointer already freed.", function, file, line);
  } else {
    for (i=0; i<n; i++) {
      FreeM_internal(ptr[i], m, file, line, function);
    }
    free(ptr);
  }
}

#define Free3M(ptr, n, m) \
do { \
  Free3M_internal((void***)ptr, n, m, __FILE__, __LINE__, __func__); \
  ptr = NULL; \
} while (0)


static inline void Free4M_internal(void**** ptr, size_t n, size_t m, size_t o, const char* const file, int line, const char* const function) {
  size_t i;

  if (ptr == NULL) {
    mydebug("%s() (File: %s Line: %d): pointer already freed.", function, file, line);
  } else {
    for (i=0; i<n; i++) {
      Free3M_internal(ptr[i], m, o, file, line, function);
    }
    free(ptr);
  }
}

#define Free4M(ptr, n, m, o) \
do { \
  Free4M_internal((void****)ptr, n, m, o, __FILE__, __LINE__, __func__); \
  ptr = NULL; \
} while (0)

#if defined(DEBUG) || defined(VALGRIND_DEBUG)
/** @brief Copy a string and returns a pointer to the newly allocated memory.
  *
  * The allocated memory should be freed with Free()
  */
static inline char* Strdup_internal(const char* src, const char* const file, int line, const char* const function) {
  char *dst;

  if (src == NULL) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): tried to duplicate string which is NULL.", function, file, line);
  }

#ifdef VALGRIND_DEBUG
  dst = strcpy(MallocS(strlen(src)+1), src);
#else
  if ((dst = strdup(src)) == NULL) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): error allocating %zd bytes.", function, file, line, strlen(src)+1);
  }
#endif

  return dst;
}

#define Strdup(src) Strdup_internal(src, __FILE__, __LINE__, __func__)

/** @brief Copy a string and returns a pointer to the newly allocated memory.
  *
  * The allocated memory should be freed with Free()
  */
static inline char* Strndup_internal(const char* src, size_t n, const char* const file, int line, const char* const function) {
  char *dst;

  if (src == NULL) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): tried to duplicate string which is NULL.", function, file, line);
  }

#ifdef VALGRIND_DEBUG
  dst = strncpy(MallocS(n+1), src, n);
#else
  if ((dst = strndup(src, n)) == NULL) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d): error allocating %zd bytes.", function, file, line, n+1);
  }
#endif

  return dst;
}

#define Strndup(src, n) Strndup_internal(src, n, __FILE__, __LINE__, __func__)

#else

#define Strdup(src) strdup(src)
#define Strndup(src, n) strndup(src, n)

#endif

#ifdef DEBUG

static inline void* Memmove_internal(void *dst, const void *src, size_t n, const char* const file, int line, const char* const function) {
  if (!dst) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d) : Memmove: dst == NULL", function, file, line);
  }
  if (!src) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d) : Memmove: src == NULL", function, file, line);
  }
  if (n == 0) {
    mydebug("%s() (File: %s Line: %d) : Memmove: n == 0", function, file, line);
  }

  return memmove(dst, src, n);
}

#define Memmove(dst, src, n) Memmove_internal(dst, src, n, __FILE__, __LINE__, __func__)

static inline void* Memcpy_internal(void *dst, const void *src, size_t n, const char* const file, int line, const char* const function) {
  if (!dst) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d) : Memcpy: dst == NULL", function, file, line);
  }
  if (!src) {
    myexit(ERROR_BUG, "%s() (File: %s Line: %d) : Memcpy: src == NULL", function, file, line);
  }
  if (n == 0) {
    mydebug("%s() (File: %s Line: %d) : Memcpy: n == 0", function, file, line);
  }

  return memcpy(dst, src, n);
}

#define Memcpy(dst, src, n) Memcpy_internal(dst, src, n, __FILE__, __LINE__, __func__)

#else

#define Memmove(dst, src, n) memmove(dst, src, n)
#define Memcpy(dst, src, n) memcpy(dst, src, n)

#endif

#endif
