/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_WRAPPER_STDIO_H
#define LIB_WRAPPER_STDIO_H

/** @brief Print out an info line.
  *
  * Output format is " (II) <<date and time>> : <<string>>"
  *
  * If compiled with MPI support the format is
  * " (II) (Node <<rank>>): <<date and time>> : <<string>>"
  *
  * @param str string to be printed.
  */
static inline void printinfo(const char* str) {

#ifdef INSIDESOLVER  // only used inside the solver

#ifdef MPI

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (II) (Node %i) : %s : %s\n", mpiinfo.world.rank, sprinttime(), str);
    fflush(pinfo.logfp);
  } else {
#ifndef DEBUG
    if (!(isMasterRank(mpiinfo.world.rank) || (mpiinfo.workers.rank == 0))) return; // output only by the server and/or the first worker
#endif
    fprintf(stderr, " \033[1;32m(II)\033[30;0m \033[01m(Node %i)\033[0m : %s : %s\033[m\n",
                    mpiinfo.world.rank, sprinttime(), str);
  }

#else

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (II) %s : %s\n", sprinttime(), str);
    fflush(pinfo.logfp);
  } else {
    fprintf(stderr, " \033[1;32m(II)\033[30;0m %s : %s\n", sprinttime(), str);
  }

#endif

#else  // outside of the solver
  fprintf(stderr, " \033[1;32m(II)\033[30;0m %s\n", str);
#endif

}

/** @brief Print out an info line.
  *
  * Output format is " (II) <<date and time>> : <<string>>"
  *
  * If compiled with MPI support the format is
  * " (II) (Node <<rank>>): <<date and time>> : <<string>>"
  *
  * @param str string to be printed.
  */
static inline void printinfo_all(const char* str) {

#ifdef INSIDESOLVER  // only used inside the solver

#ifdef MPI

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (II) (Node %i) : %s : %s\n", mpiinfo.world.rank, sprinttime(), str);
    fflush(pinfo.logfp);
  } else {
    fprintf(stderr, " \033[1;32m(II)\033[30;0m \033[01m(Node %i)\033[0m : %s : %s\033[m\n",
                    mpiinfo.world.rank, sprinttime(), str);
  }

#else

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (II) %s : %s\n", sprinttime(), str);
    fflush(pinfo.logfp);
  } else {
    fprintf(stderr, " \033[1;32m(II)\033[30;0m %s : %s\n", sprinttime(), str);
  }

#endif

#else  // outside of the solver
  fprintf(stderr, " \033[1;32m(II)\033[30;0m %s\n", str);
#endif

}

/** @brief Print out a warning and continue.
  *
  * Output format is " (WW) <<date and time>> : <<string>>"
  *
  * If compiled with MPI support the format is
  * " (WW) (Node <<rank>>): <<date and time>> : <<string>>"
  *
  * @param str string to be printed.
  */
static inline void printwarn(const char* str) {

#ifdef INSIDESOLVER  // only used inside the solver

#ifdef MPI

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (WW) (Node %i) : %s : %s\n", mpiinfo.world.rank, sprinttime(), str);
    fflush(pinfo.logfp);
  }
  fprintf(stderr, " \033[1;35m(WW)\033[30;0m \033[01m(Node %i)\033[0m : %s : \033[1;35m%s\033[30;0m\n",
                  mpiinfo.world.rank, sprinttime(), str);

#else

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (WW) %s : %s\n", sprinttime(), str);
    fflush(pinfo.logfp);
  }
  fprintf(stderr, " \033[1;35m(WW)\033[30;0m %s : \033[1;35m%s\033[30;0m\n", sprinttime(), str);

#endif

#else // outside of the solver
  fprintf(stderr, " \033[1;35m(WW) %s\033[30;0m\n", str);
#endif
}

/** @brief Print out a (verbose) message and continue.
  *
  * Output format is " (VV) <<date and time>> : <<string>>"
  *
  * If compiled with MPI support the format is
  * " (VV) (Node <<rank>>): <<date and time>> : <<string>>"
  *
  * @param str string to be printed.
  */
static inline void printverbose(const char* str) {

#ifdef INSIDESOLVER  // only used inside the solver

#ifdef MPI

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (VV) (Node %i) : %s : %s\n", mpiinfo.world.rank, sprinttime(), str);
    fflush(pinfo.logfp);
  }
  fprintf(stderr, " \033[1;01m(VV) (Node %i)\033[0m : %s : \033[1;01m%s\033[30;0m\n",
                  mpiinfo.world.rank, sprinttime(), str);

#else

  if (pinfo.logfp && pinfo.logfp != stderr) {
    fprintf(pinfo.logfp, " (VV) %s : %s\n", sprinttime(), str);
    fflush(pinfo.logfp);
  }
  fprintf(stderr, " \033[1;01m(VV) %s : %s\033[30;0m\n", sprinttime(), str);

#endif

#else // outside of the solver
  fprintf(stderr, " \033[1;01m(VV) %s\033[30;0m\n", str);
#endif
}


/** @brief Print a vector of values with spezified type.
  *
  * Output is in the format "<<vectorname>> = ( elem1, elem2, ..., elemn )."
  *
  * @param vectorname    string with name of the vector.
  * @param vector        pointer to the vector.
  * @param formatstring  how to print the value of an element of the matrix with printf.
  * @param n             count of the vector elements.
  */
#define PRINTVECTOR(vectorname, vector, n, formatstring) \
  do { \
    size_t size; \
    char   str[MAX_LINE_LENGTH] = ""; \
    for (size_t i=0; i<n; i++) { \
      size = strlen(str); \
      snprintf(str+size, MAX_LINE_LENGTH-size, "" formatstring" ", vector[i]); \
    } \
    myinfo("%s = ( %s).", vectorname, str); \
  } while (0)

/** @brief Print a vector of REALs.
  *
  * Output is in the format "<<vectorname>> = ( elem1, elem2, ..., elemn )."
  *
  * @param vectorname string with name of the vector.
  * @param vector     pointer to the vector.
  * @param n          count of the vector elements.
  */
static inline void printfv(const char* vectorname, REAL* vector, size_t n) {
  // a space between the string literal and the macro is needed since
  // GCC 4.7 to compile this file under C++ with the C++11 standard
  PRINTVECTOR(vectorname, vector, n, "%" REALLENGTH"f");
}

/** @brief Print a vector of integers.
  *
  * Output is in the format "<<vectorname>> = ( elem1, elem2, ..., elemn )."
  *
  * @param vectorname string with name of the vector.
  * @param vector     pointer to the vector.
  * @param n          count of the vector elements.
  */
static inline void printiv(const char* vectorname, long* vector, size_t n) {
  PRINTVECTOR(vectorname, vector, n, "%ld");
}


/** @brief Print a matrix of values with spezified type.
  *
  * Output is in the format:
  *
  * <table border="0">
  * <tr> <td>\<\<matrixname\>\></td> </tr>
  * <tr> <td></td>       <td>mname1  </td>  <td>mname2  </td>  <td>...</td> <td>mnamen  </td>  </tr>
  * <tr> <td>nname1</td> <td>element1</td>  <td>element2</td>  <td>...</td> <td>elementn</td>  </tr>
  * <tr> <td>...</td> </tr>
  * <tr> <td>nnamen</td> <td>element1</td>  <td>element2</td>  <td>...</td> <td>elementn</td>  </tr>
  * </table>
  *
  * @param matrixname    string with name of the matrix.
  * @param matrix        pointer to the matrix.
  * @param n             count of the n elements of the matrix.
  * @param m             count of the m elements of the matrix.
  * @param nname         string vector with names for rows.
  * @param mname         string vector with names for colums.
  * @param formatstring  how to print the value of an element of the matrix with printf.
  * @param elementgetter how to get the element of the matrix
  */
#define PRINTMATRIX(matrixname, matrix, n, m, nname, mname, formatstring, elementgetter) \
  do { \
    char   str[MAX_LINE_LENGTH] = ""; \
    size_t size; \
    for (size_t j=0; j<m; j++) { \
      size = strlen(str); \
      snprintf(str+size, MAX_LINE_LENGTH-size, "%-12s   ", mname[j]); \
    } \
    for (size_t i=0; i<n; i++) { \
      size = strlen(str); \
      snprintf(str+size, MAX_LINE_LENGTH-size, "\n%31s", nname[i]); \
      for (size_t j=0; j<m; j++) { \
        size = strlen(str); \
        snprintf(str+size, MAX_LINE_LENGTH-size, "" formatstring"   ", elementgetter); \
      } \
    } \
    myinfo("%s\n                                   %s", matrixname, str); \
  } while (0)

/** @brief Print a matrix of REALs.
  *
  * @sa PRINTMATRIX
  *
  * @param matrixname string with name of the matrix.
  * @param matrix     pointer to the matrix.
  * @param n          count of the n elements of the matrix.
  * @param m          count of the m elements of the matrix.
  * @param nname      string vector with names for rows.
  * @param mname      string vector with names for colums.
  */
static inline void printfm(const char* matrixname, REAL** matrix, size_t n, size_t m, char** nname, char** mname) {
  // a space between the string literal and the macro is needed since
  // GCC 4.7 to compile this file under C++ with the C++11 standard
  PRINTMATRIX(matrixname, matrix, n, m, nname, mname, "%12.6" REALLENGTH"f", matrix[i][j]);
}

/** @brief Print a linear matrix of REALs.
  *
  * @sa PRINTMATRIX
  *
  * @param matrixname string with name of the matrix.
  * @param matrix     pointer to the linear matrix.
  * @param n          count of the n elements of the matrix.
  * @param m          count of the m elements of the matrix.
  * @param nname      string vector with names for rows.
  * @param mname      string vector with names for colums.
  */
static inline void printfmlinear(const char* matrixname, REAL* matrix, size_t n, size_t m, char** nname, char** mname) {
  // a space between the string literal and the macro is needed since
  // GCC 4.7 to compile this file under C++ with the C++11 standard
  PRINTMATRIX(matrixname, matrix, n, m, nname, mname, "%12.6" REALLENGTH"f", *(matrix + i * n + j));
}

/** @brief Print a matrix of integers.
  *
  * @sa PRINTMATRIX
  *
  * @param matrixname string with name of the matrix.
  * @param matrix     pointer to the matrix.
  * @param n          count of the n elements of the matrix.
  * @param m          count of the m elements of the matrix.
  * @param nname      string vector with names for rows.
  * @param mname      string vector with names for colums.
  */
static inline void printim(const char* matrixname, long** matrix, size_t n, size_t m, char** nname, char** mname) {
  PRINTMATRIX(matrixname, matrix, n, m, nname, mname, "%-12ld", matrix[i][j]);
}

#endif
