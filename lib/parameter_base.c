/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <wrapper.h>
#include <container/stringconv.h>
#ifndef DISABLE_PARAM_VALIDATION
#include <validator.h>
#include "datafiles/parse.h"
#endif
#include "parameter_base.h"
#include "structures.h"

void register_base_types() {
  paramtype_t type_none = {
    .type = PARAM_NONE,
    .description = "none",

    .printHelp = &printHelp_DEFAULT,
    .warnMissing = &warnParamMissing_DEFAULT
  };
  registerParamType(type_none);

  paramtype_t type_long = {
    .type = PARAM_LONG,
    .description = "long",

    .getValue = &getValue_long,
    .printHelp = &printHelp_long,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_long
  };
  registerParamType(type_long);

  paramtype_t type_real = {
    .type = PARAM_REAL,
    .description = "real",

    .getValue = &getValue_real,
    .printHelp = &printHelp_real,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_real
  };
  registerParamType(type_real);

  paramtype_t type_vector_long = {
    .type = PARAM_VECTOR_LONG,
    .description = "vector_long",

    .getValue = &getValue_vector_long,
    .printHelp = &printHelp_real,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_vector_long
  };
  registerParamType(type_vector_long);

  paramtype_t type_vector_n_long = {
    .type = PARAM_VECTOR_N_LONG,
    .description = "vector_n_long",

    .getValue = &getValue_vector_n_long,
    .printHelp = &printHelp_vector_n_long,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_vector_n_long
  };
  registerParamType(type_vector_n_long);

  paramtype_t type_list_long = {
    .type = PARAM_LIST_LONG,
    .description = "list_long",

    .getValue = &getValue_list_long,
    .printHelp = &printHelp_list_long,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_list_long
  };
  registerParamType(type_list_long);

  paramtype_t type_vector_real = {
    .type = PARAM_VECTOR_REAL,
    .description = "vector_real",

    .getValue = &getValue_vector_real,
    .printHelp = &printHelp_vector_real,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_vector_real
  };
  registerParamType(type_vector_real);

  paramtype_t type_flag = {
    .type = PARAM_FLAG,
    .description = "flag",
    .is_flag = true,

    .getValue = &getValue_flag,
    .printHelp = &printHelp_DEFAULT,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_flag
  };
  registerParamType(type_flag);

  paramtype_t type_string = {
    .type = PARAM_STRING,
    .description = "string",

    .getValue = &getValue_string,
    .printHelp = &printHelp_string,
    .warnMissing = &warnParamMissing_DEFAULT,
    .printXml = &printXml_string
  };
  registerParamType(type_string);

  paramtype_t type_filein = {
    .type = PARAM_FILEIN,
    .description = "filein",

    .getValue = &getValue_filein,
    .printHelp = &printHelp_filein,
    .warnMissing = &warnParamMissing_filein,
    .printXml = &printXml_filein
  };
  registerParamType(type_filein);

  paramtype_t type_fileout = {
    .type = PARAM_FILEOUT,
    .description = "fileout",

    .getValue = &getValue_fileout,
    .printHelp = &printHelp_fileout,
    .warnMissing = &warnParamMissing_fileout,
    .printXml = &printXml_fileout
  };
  registerParamType(type_fileout);

#ifndef DISABLE_PARAM_VALIDATION
  type_none.validate = &validate_DEFAULT;
  type_long.validate = &validate_long;
  type_real.validate = &validate_real;
  type_vector_long.validate = &validate_vector_long;
  type_vector_n_long.validate = &validate_vector_n_long;
  type_list_long.validate = &validate_vector_real;
  type_vector_real.validate = &validate_vector_real;
  type_string.validate = &validate_DEFAULT;
  type_flag.validate = &validate_DEFAULT;
  type_fileout.validate = &validate_DEFAULT;
  type_filein.validate = &validate_DEFAULT;
#endif
}

void getValue_long(const char* prefix, const char* name, char* arg, void* value) {
  if (String_makeLong(arg, (long*)value) == false) {
    myexit(ERROR_PARAM, "Could not get long from parameter '%s%s'.", prefix, name);
  }
}

void getValue_real(const char* prefix, const char* name, char* arg, void* value) {
  if (String_makeFloat(arg, (REAL*)value) == false) {
    myexit(ERROR_PARAM, "Could not get float from parameter '%s%s'.", prefix, name);
  }
}

void getValue_vector_long(const char* prefix, const char* name, char* arg, void* value) {
  if (String_makeVectorLong(arg, (long*)value, 3) == false) {
    myexit(ERROR_PARAM, "Could not get long-vector from parameter '%s%s'.", prefix, name);
  }
}

void getValue_vector_n_long(const char* prefix, const char* name, char* arg, void* value) {
  long size = (*(vectorl_n_t*)value).size;
  (*(vectorl_n_t*)value).vector = MallocIV(size);
  if (String_makeVectorLong(arg, ((vectorl_n_t*)value)->vector, size) == false) {
    myexit(ERROR_PARAM, "Could not get long-vector of size %ld from parameter '%s%s'.", size, prefix, name);
  }
}

void getValue_list_long(UNUSED(const char* prefix), UNUSED(const char* name), char* arg, void* value) {
  vectorl_n_t *longlist;
  longlist = (vectorl_n_t*) (value);
  String_makeDynamicLongList(arg, &(longlist->vector), &(longlist->size));
}

void getValue_vector_real(const char* prefix, const char* name, char* arg, void* value) {
  if (String_makeVectorFloat(arg, (REAL*)value, 3) == false) {
    myexit(ERROR_PARAM, "Could not get real-vector from parameter '%s%s'.", prefix, name);
  }
}

void getValue_flag(UNUSED(const char* prefix), UNUSED(const char* name), UNUSED(char* arg), void* value) {
  *(long*)value = !(*(long*)value);
}

void getValue_string(const char* prefix, const char* name, char* arg, void* value) {
  if (strlen(arg) == 0) {
    myexit(ERROR_PARAM, "Empty string for parameter '%s%s'.", prefix, name);
  }
  *(char**)(value) = Strdup(arg);
}

void getValue_filein(const char* prefix, const char* name, char* arg, void* value) {
  getValue_string(prefix, name, arg, value);
}

void getValue_fileout(const char* prefix, const char* name, char* arg, void* value) {
  getValue_string(prefix, name, arg, value);
}


void printHelp_DEFAULT(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, NULL);
}

void printHelp_long(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<i>");
  if (argument->option == PARAM_OPTIONAL) {
    printf(" (default=%li) ", *(long *)argument->parameter);
  }
}

void printHelp_real(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<f>");
  if (argument->option == PARAM_OPTIONAL) {
    printf(" (default=%"REALLENGTH"f) ", *(REAL *)argument->parameter);
  }
}

void printHelp_vector_n_long(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  long size = (*(vectorl_n_t*)argument->parameter).size;
  char *str = Strdup("[<i>");
  for (int i=1; i < size; i++) {
    String_append(&str, ",<i>");
  }
  String_append(&str, "]");
  printParameter(shortname, longname, str);
  if (argument->option == PARAM_OPTIONAL) {
    printf("vector of size %ld ", size);
  }
  Free(str);
}

void printHelp_list_long(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<i>,<i>-<i>,<i>-<i>%<i>");
}

void printHelp_vector_real(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "[<f>,<f>,<f>]");
  if (argument->option == PARAM_OPTIONAL) {
    printf(" (default=[%"REALLENGTH"f,%"REALLENGTH"f,%"REALLENGTH"f]) ",
      (*(REAL (*)[3])argument->parameter)[0],
      (*(REAL (*)[3])argument->parameter)[1],
      (*(REAL (*)[3])argument->parameter)[2]);
  }
}

void printHelp_string(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<s>");
  if (argument->option == PARAM_OPTIONAL && *(char **)argument->parameter != NULL) {
    printf(" (default=%s) ", *(char **)argument->parameter);
  }
}

void printHelp_filein(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<inputfile>");
}

void printHelp_fileout(const argument_t* argument, UNUSED(bool extendedhelp), UNUSED(const char* delimiter)) {
  const char  shortname = argument->name;
  const char *longname  = argument->longname;
  printParameter(shortname, longname, "<outputfile>");
}

void printXml_long(const void* parameter) {
  printf("default=\"%li\" ", *(long *)parameter);
}

void printXml_real(const void* parameter) {
  printf("default=\"%"REALLENGTH"f\" ", *(REAL *)parameter);
}

void printXml_vector_long(const void* parameter) {
  printf("default=\"[%li,%li,%li]\" ",
         (*(long (*)[3])parameter)[0],
         (*(long (*)[3])parameter)[1],
         (*(long (*)[3])parameter)[2]);
}

void printXml_vector_n_long(UNUSED(const void* parameter)) {
  printf("default=\"NA\" ");
}

void printXml_list_long(UNUSED(const void* parameter)) {
  printf("default=\"NA\" ");
}

void printXml_vector_real(const void* parameter) {
  printf("default=\"[%"REALLENGTH"f,%"REALLENGTH"f,%"REALLENGTH"f]\" ",
         (*(REAL (*)[3])parameter)[0],
         (*(REAL (*)[3])parameter)[1],
         (*(REAL (*)[3])parameter)[2]);
}

void printXml_flag(const void* parameter) {
  printf("default=\"%s\" ", *(long *)parameter ? "true" : "false");
}

void printXml_string(const void* parameter) {
  char *escapedstr = NULL;
  if (*(char **)parameter) {
    String_mask(*(char **)parameter, &escapedstr, STRING_MASK, XML_masktable);
    printf("default=\"%s\" ", escapedstr);
    Free(escapedstr);
  }
}

void printXml_filein(const void* parameter) {
  printXml_string(parameter);
}

void printXml_fileout(const void* parameter) {
  printXml_string(parameter);
}

void warnParamMissing_DEFAULT(const char* name) {
  mywarn("Parameter '-%c' is missing.", *name);
}

void warnParamMissing_filein(UNUSED(const char* name)) {
  mywarn("Input File is missing.");
}

void warnParamMissing_fileout(UNUSED(const char* name)) {
  mywarn("Output File is missing.");
}

#ifndef DISABLE_PARAM_VALIDATION
void validate_DEFAULT(const argument_t* argument, UNUSED(char* givenargs), UNUSED(size_t argnumber)) {
  myexit(ERROR_BUG, "Unknown type '%s' (%ld) for parameter '-%c'/'--%s' to validate.",
         paramtypes[argument->flag].description, argument->flag, argument->name, argument->longname);
}

void validate_long(const argument_t* argument, char* givenargs, size_t argnumber) {
  Condition condition;
  long parameterlong;

  Condition_init(&condition, argument->interval, (stringParseFunc)parseInt, compare_long_ascending, NULL);
  parameterlong = *(long*) (argument->parameter);

  if (!Condition_validate(&condition, &parameterlong)) {
    if (getBit(givenargs, argnumber) == true) {
      myexit(ERROR_PARAM, "Parameter '-%c' is out of range. Interval = %s", argument->name, argument->interval);
    } else {
      printIntervalWarning(argument->name);
    }
  }
  Condition_deinit(&condition);
}

void validate_real(const argument_t* argument, char* givenargs, size_t argnumber) {
  Condition condition;
  REAL parameterfloat;

  Condition_init(&condition, argument->interval, (stringParseFunc)parseREAL, compare_REAL_ascending, NULL);
  parameterfloat = *(REAL*) (argument->parameter);

  if (!Condition_validate(&condition, &parameterfloat)) {
    if (getBit(givenargs, argnumber) == true) {
      myexit(ERROR_PARAM, "Parameter '-%c' is out of range. Interval = %s", argument->name, argument->interval);
    } else {
      printIntervalWarning(argument->name);
    }
  }
  Condition_deinit(&condition);
}

void validate_vector_long(const argument_t* argument, char* givenargs, size_t argnumber) {
  Condition condition;
  ulong size;
  long *parameterlongvector;

  Condition_init(&condition, argument->interval, (stringParseFunc)parseInt, compare_long_ascending, NULL);
  if ((argument->flag == PARAM_VECTOR_N_LONG)) {
    size = ((vectorl_n_t*)argument->parameter)->size;
    parameterlongvector = ((vectorl_n_t*) argument->parameter)->vector;
  } else {
    size = 3;
    parameterlongvector = ((long*) argument->parameter);
  }

  for (ulong j = 0; j < size; j++) {
    if (!Condition_validate(&condition, &parameterlongvector[j])) {
      if (getBit(givenargs, argnumber) == true) {
        myexit(ERROR_PARAM, "Parameter '-%c'[%ld] is out of range. Interval = %s", argument->name, j, argument->interval);
      } else {
        printIntervalWarning(argument->name);
      }
    }
  }
  Condition_deinit(&condition);
}

void validate_vector_n_long(const argument_t* argument, char* givenargs, size_t argnumber) {
  validate_vector_long(argument, givenargs, argnumber);
}

void validate_vector_real(const argument_t* argument, char* givenargs, size_t argnumber) {
  Condition condition;
  ulong size;
  REAL parameterfloat;
  Condition_init(&condition, argument->interval, (stringParseFunc)parseREAL, compare_REAL_ascending, NULL);
  size = 3;

  for (ulong j = 0; j < size; j++) {
    parameterfloat = ((REAL*) (argument->parameter))[j];
    if (!Condition_validate(&condition, &parameterfloat)) {
      if (getBit(givenargs, argnumber) == true) {
        myexit(ERROR_PARAM, "Parameter '-%c'[%ld] is out of range. Interval = %s", argument->name, j, argument->interval);
      } else {
        printIntervalWarning(argument->name);
      }
    }
  }
  Condition_deinit(&condition);
}
#endif
