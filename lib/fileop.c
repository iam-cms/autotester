/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#include <dirent.h>
#include <regex.h>

#include <wrapper.h>

#include "fileop.h"

long getFilesFiltered(char*** foundfiles, const char* const path, const char* regex, bool recursive) {
  struct dirent *dircontent;
  DIR   *dir;
  struct stat    statbuf;
  char   filename[PATH_MAX];
  long   foundfilescount = 0;
  long   i;

  regex_t re;
  int flags = REG_EXTENDED | REG_NOSUB;
  int errorcode;

  errorcode = regcomp(&re, regex, flags);
  if (errorcode != 0) {
    char errbuf[MAX_LINE_LENGTH];
    regerror(errorcode, &re, errbuf, sizeof(errbuf));
    myexit(ERROR_BUG, "getFilesFiltered(): Compilation of regex '%s' with flags %d failed (%s).", regex, flags, errbuf);
  }

  if ((dir = opendir(path)) == NULL) {
    mywarn("Could not open directory %s", path);
    return -1;
  }

  *foundfiles = NULL;

  while ((dircontent = readdir(dir)) != NULL) {
    snprintf(filename, PATH_MAX, "%s/%s", path, dircontent->d_name);
    Stat(filename, &statbuf);

    if (recursive && S_ISDIR(statbuf.st_mode)) { // RECURSION
      if ((strcmp(dircontent->d_name, ".") != 0 && strcmp(dircontent->d_name, "..") != 0)) {
        char **tmpfoundfiles;
        long   tmpfoundfilescount = getFilesFiltered(&tmpfoundfiles, filename, regex, true);
        if (tmpfoundfilescount > 0) {
          *foundfiles = Realloc(*foundfiles, (foundfilescount+tmpfoundfilescount)*sizeof(char**));
          for (i=0; i<tmpfoundfilescount; i++) {
            (*foundfiles)[foundfilescount+i] = tmpfoundfiles[i];
          }
          foundfilescount += tmpfoundfilescount;
          Free(tmpfoundfiles);
        }
      }
    } else if (S_ISREG(statbuf.st_mode) || S_ISLNK(statbuf.st_mode)) {
      if (regexec(&re, dircontent->d_name, 0, NULL, 0) == 0) {
        foundfilescount++;
        *foundfiles = Realloc(*foundfiles, (foundfilescount)*sizeof(char**));
        (*foundfiles)[foundfilescount-1] = Strdup(filename);
      }
    }
  }
  regfree(&re);
  closedir(dir);

  return foundfilescount;
}
/** @brief Split given filename in full path and filename.
  *
  * If the long filename to be resolved is "-"
  * path will be NULL and the filename will be "-"
  * to mark a piped file stream.
  *
  * @note Allocated memory for the path and the filename has to be freed by the caller.
  */
void resolvePath(char** path, char** filename, const char* const longfilename) {
  char *pos;
  char  resolvedfilename[PATH_MAX];

  // extend longfilename to complete path + filename
  resolvedfilename[0] = '\0';              // mark string as empty
  if (realpath(longfilename,resolvedfilename) == NULL) {
    if (errno != ENOENT) {
      myexit(ERROR_FILEIO, "resolvePath(): error fetching path info for file %s. (%s)", longfilename, strerror(errno));
    }
    char unresolved[PATH_MAX];
    strncpy(resolvedfilename, longfilename, PATH_MAX);
    strncpy(unresolved, dirname(resolvedfilename), PATH_MAX);
    // resolve path correct anyway
    resolvedfilename[0] = '\0';              // mark string as empty
    if (realpath(unresolved, resolvedfilename) == NULL) {
      myexit(ERROR_FILEIO, "resolvePath(): error fetching path info for file %s. (%s)", longfilename, strerror(errno));
    }
    strcat(resolvedfilename, "/");

    *path = Strdup(resolvedfilename);

    if (filename != NULL) {
      strncpy(resolvedfilename, longfilename, PATH_MAX);
      *filename = Strdup(basename(resolvedfilename));
    }
  } else {
    // extract short filename
    pos = findFileName(resolvedfilename);
    if (pos == NULL) {
      myexit(ERROR_BUG, "resolvePath(): Could not resolve path and filename.");
    }
    if (filename != NULL) {
      *filename = Strdup(pos);
    }
    // extract path
    *pos = '\0';
    *path = Strdup(resolvedfilename);
  }
  // mydebug("path %s filename %s", *path, *filename);
}
