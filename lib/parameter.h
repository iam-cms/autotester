/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/

#ifndef LIB_PARAMETER_H
#define LIB_PARAMETER_H

#include <stddef.h>
#include <parameter_base.h>

#define PARAM_REQUIRED      1024
#define PARAM_OPTIONAL      2048

#define ARGUMENT(arg)  (sizeof(arg)/sizeof(arg[0]))


typedef struct argument_s {
  const char *longname;
  const char  name;
  const long  option;
  const char *andparams;
  const char *interval;
  const char *description;
  const long  flag;
  const void *parameter;
} argument_t;

typedef struct toolparam_s {
  const char        *description;
  const char        *example;
  const argument_t  *arguments;
  const long         istested;
} toolparam_t;

typedef struct paramtype_s {
  long                type;
  const char         *description;

  // flags
  bool is_flag;
  bool registered;

  // callbacks
  void (*getValue)      (const char* prefix, const char* name, char* arg, void* value);
  void (*warnMissing)   (const char* name);
  void (*printHelp)     (const argument_t* argument, bool extendedhelp, const char* delimiter);
  void (*printXml)      (const void* argument);
  void (*validate)      (const argument_t* argument, char* givenargs, size_t argnumber);
} paramtype_t;

void register_base_types(void);
void register_extended_types(void) __attribute__((weak));

void registerParamType(paramtype_t paramtype);
void setupParameterTypes(void);

void printHelpTextwithNewline(const char* text);
void printParameter(const char shortname, const char* longname, char* hint);
bool  getParams(int argc, char* argv[], toolparam_t tool, size_t count);
void  printUsage(const char* progname, toolparam_t tool, size_t count);

#endif
