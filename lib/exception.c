/********************************************************************//**
 *
 * Autotester: A tool to automatically run system tests and output results
 * Copyright (C) 2021  IAM-CMS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 ********************************************************************/


#define _GNU_SOURCE 1

#include <fenv.h>
#include <execinfo.h>

#include <signal.h>
#include <dlfcn.h>

#include <wrapper.h>

#include "exception.h"

// #define SIGSEGV_STACK_BACKTRACE  //print execution path with backtrace
// #define SIGSEGV_STACK_DLADDR     //print calling path. Use ip form ucontext and manual trace, get relative adress with dladdr and print with addr2line
#define SIGSEGV_STACK_BOTH       //print calling and execution path. Execution path like backtrace, calling path like dladdr except trace is from backtrace

#ifdef SIGSEGV_STACK_DLADDR

#if defined(REG_RIP)
# define SIGSEGV_STACK_IA64
#elif defined(REG_EIP)
# define SIGSEGV_STACK_X86
#endif

#endif


/** @brief Use popen to execute addr2line and read its output
  */
static void callAddr2Line(char adresslist[]) {
  long    i;
  char    call[1024];
  size_t  num_bytes_left;
  char    function[256];
  char    filename_lineno[256];
  FILE   *output;
  int     ret;

  // Construct the arguments to addr2line
  output = popen("which addr2line", "r");
  if (fscanf(output, "%s", call) != 1) {
    fprintf(stderr, "Internal error in signal handler (%s:%d).\n",  __FILE__, __LINE__);
    return;
  }
  ret = pclose(output);

  // error
  if (ret != 0 && ret != 13 && ret != 36096) {
    fprintf(stderr, "[bt] could not execute addr2line (return code = %d, %s:%d).\n", ret, __FILE__, __LINE__);
    return;
  }

  // add args for addr2line
  num_bytes_left = sizeof(call) - strlen(call) - 6;
  strncat(call, " -ifCe ", num_bytes_left);

  // add name of the binary
  char prognamescan[PATH_MAX];
  snprintf(prognamescan, sizeof(prognamescan), "which %s", progname);
  output = popen(prognamescan, "r");
  if (fscanf(output, "%s", prognamescan) != 1) {
    fprintf(stderr, "Internal error in signal handler (%s:%d).\n",  __FILE__, __LINE__);
    return;
  }
  ret = pclose(output);

  // error
  if (ret != 0 && ret != 13 && ret != 36096) {
    fprintf(stderr, "[bt] could not find executable %s (return code = %d, %s:%d).\n", progname, ret, __FILE__, __LINE__);
    return;
  }
  num_bytes_left -= strlen(prognamescan);
  strncat(call, prognamescan, num_bytes_left);

  // add adresses
  num_bytes_left--;
  strncat(call, " ", num_bytes_left);
  num_bytes_left--;
  strncat(call, adresslist, num_bytes_left);

//   printf("running program: %s\n", call);
  output = popen(call, "r");
  fprintf(stderr, "[bt] Calling path:\n");
  for (i = 0; !feof(output); i++) {
    int elmcount = fscanf(output, "%255[^\n\r]\n%255[^\n\r]\n", function, filename_lineno);
    if (elmcount != 2) {
      fprintf(stderr, "Internal error in signal handler (%s:%d).\n",  __FILE__, __LINE__);
      pclose(output);
      return;
    }
    char *ptr = filename_lineno + strlen(filename_lineno);
    while (ptr != filename_lineno) {
      if (*ptr == '/') {
        if (--ptr == filename_lineno) break;
        if (*ptr == 'm') {
          if (--ptr == filename_lineno) break;
          if (*ptr == 'i') {
            if (--ptr == filename_lineno) break;
            if (*ptr == 's') {
              ptr += 4;
              break;
            }
          }
        }
      }
      ptr--;
    }
    fprintf(stderr, "[bt] % 3ld: %s at %s\n", i+1, function, ptr);
  }
  pclose(output);
}

void printBacktrace(ucontext_t* ucontext, long deepness) {
#if defined(SIGSEGV_STACK_DLADDR) || defined(SIGSEGV_STACK_BOTH)
  char  adresslist[1024] = "";
  int   num_bytes_left = sizeof(adresslist)-1; // reserve space for termination
  Dl_info dlinfo;
#endif

#if defined(SIGSEGV_STACK_BACKTRACE) || defined(SIGSEGV_STACK_BOTH)
  (void)ucontext;
#ifdef INSIDESOLVER
  if (pinfo.logfp) {
    fprintf(pinfo.logfp, "[bt] Execution path:\n");
  }
#endif
  fprintf(stderr, "[bt] Execution path:\n");

  void  *trace[20];
  char **messages;
  size_t trace_size;
  size_t i;

  trace_size = backtrace(trace, 20);
  messages = backtrace_symbols(trace, trace_size);

#ifdef INSIDESOLVER
  // skip first stack frame (points here)
  if (pinfo.logfp) {
    for (i=deepness; i<trace_size-2; i++) {
      fprintf(pinfo.logfp, "[bt] % 3zd: %s\n", trace_size-i-2, messages[i]);
    }
  }
#endif
  for (i=deepness; i<trace_size-2; i++) {
    fprintf(stderr, "[bt] % 3zd: %s\n", trace_size-i-2, messages[i]);

#ifdef SIGSEGV_STACK_BOTH
    char addr[256];
    if (!dladdr(trace[i], &dlinfo)) break;

    num_bytes_left -= snprintf(addr, sizeof(addr), " %p", (char*)trace[i] - (unsigned long)dlinfo.dli_fbase-1);
    strncat(adresslist, addr, num_bytes_left);
#endif
  }
#else
  (void)deepness;
  void **bp;
  void  *ip;
  long   i = 1;

#if defined(SIGSEGV_STACK_IA64)
  ip = (void*) ucontext->uc_mcontext.gregs[REG_RIP];
  bp = (void**)ucontext->uc_mcontext.gregs[REG_RBP];
#elif defined(SIGSEGV_STACK_X86)
  ip = (void*) ucontext->uc_mcontext.gregs[REG_EIP];
  bp = (void**)ucontext->uc_mcontext.gregs[REG_EBP];
#endif

//   //ucontext is one level up
//   wont work with signals
//   for (int i = 0; i < deepness - 1; i++) {
//     ip = bp[1];
//     bp = (void**)bp[0];
//   }

  char addr[256];
  while (bp && ip) {
    if (!dladdr(ip, &dlinfo)) break;

#ifdef INSIDESOLVER
    if (pinfo.logfp) {
      fprintf(pinfo.logfp, "[bt] % 3ld: %p <%s+%#lx> (%s)\n",
              i,
              ip,
              dlinfo.dli_sname,
              (unsigned long)ip - (unsigned long)dlinfo.dli_saddr,
              dlinfo.dli_fname);
    }
#endif
    fprintf(stderr, "[bt] % 3ld: %p <%s+%#lx> (%s)\n",
            i,
            ip,
            dlinfo.dli_sname,
            (unsigned long)ip - (unsigned long)dlinfo.dli_saddr,
            dlinfo.dli_fname);

    num_bytes_left -= snprintf(addr, sizeof(addr), " %p", (char*)ip - (unsigned long)dlinfo.dli_fbase-1);
    strncat(adresslist, addr, num_bytes_left);

    if (dlinfo.dli_sname && !strcmp(dlinfo.dli_sname, "main")) break;

    i++;
    ip = bp[1];
    bp = (void**)bp[0];
  }
#endif

#if defined(SIGSEGV_STACK_DLADDR) || defined(SIGSEGV_STACK_BOTH)
  callAddr2Line(adresslist);
#endif
}

/** If DEBUG is defined this function prints out the backtrace of the actual stack.
  * Make sure that the compiler optimations are off as they corrupt the output.
  */
static void printtrace(int signum, siginfo_t* info, void* data) {
  static const char *si_codes[3] = {"", "SEGV_MAPERR", "SEGV_ACCERR"};

  // Do something useful with siginfo_t
  // do not use myinfo or mywarn to reduce the risk of memory allocations since something bad happend already
#ifdef INSIDESOLVER
  if (pinfo.logfp) {
#ifdef MPI
    fprintf(pinfo.logfp, "Got signal %s, faulty address is %p (node = %d)\n", strsignal(signum), info->si_addr, mpiinfo.world.rank);
#else
    fprintf(pinfo.logfp, "Got signal %s, faulty address is %p\n", strsignal(signum), info->si_addr);
#endif
    fprintf(pinfo.logfp, "info.si_signo = %d\n", signum);
    fprintf(pinfo.logfp, "info.si_errno = %d\n", info->si_errno);
    fprintf(pinfo.logfp, "info.si_code  = %d (%s)\n", info->si_code, si_codes[info->si_code]);
    fprintf(pinfo.logfp, "info.si_addr  = %p\n", info->si_addr);
  }
#ifdef MPI
    fprintf(stderr, "Got signal %s, faulty address is %p (node = %d)\n", strsignal(signum), info->si_addr, mpiinfo.world.rank);
#else
    fprintf(stderr, "Got signal %s, faulty address is %p\n", strsignal(signum), info->si_addr);
#endif
#else
    fprintf(stderr, "Got signal %s, faulty address is %p\n", strsignal(signum), info->si_addr);
#endif
  fprintf(stderr, "info.si_signo = %d\n", signum);
  fprintf(stderr, "info.si_errno = %d\n", info->si_errno);
  fprintf(stderr, "info.si_code  = %d (%s)\n", info->si_code, si_codes[info->si_code]);
  fprintf(stderr, "info.si_addr  = %p\n", info->si_addr);

#ifdef DEBUG

  printBacktrace((ucontext_t*)data, 4);

#else  // not debug mode
  (void)data;
  mywarn("For more information rebuild with 'make DEBUG=true'");
#endif
}

/** @brief This is the handler for the signal interrupt.
  *
  * If the user pushes CTRL-C then this function sets the last step for the simulation
  * to the actual one. After this step the simulation exits normally. A second press
  * stops the simulation immediately.
  */
static void SigInterrupt_handler(UNUSED(int signum), UNUSED(siginfo_t* info), UNUSED(void* data)) {
#ifdef INSIDESOLVER
#ifdef MPI
  if (mpiinfo.emergencyexit) return;
  mpiinfo.emergencyexit = 1; // announce exit
#ifdef DEBUG
  mywarn("Got emergency exit signal in timestep %ld", SimInfo.timestep);
#else
  if (mpiinfo.world.rank == 0) {
    mywarn("*************************************************************************************");
    mywarn("*                            Simulation Interrupt                                   *");
    mywarn("*************************************************************************************");
  }
#endif
#else
  static long forceinterrupt;    // Used to indicate if the user pushed CTRL-C once or twice.
  mywarn("*************************************************************************************");
  if (forceinterrupt == FALSE) {
    mywarn("*                            Simulation Interrupt                                   *");
    mywarn("*         pressing Ctrl-C again forces the simulation to stop immediately           *");
    mywarn("*************************************************************************************");
    AreaInfo.numTSteps = SimInfo.timestep+1;
    forceinterrupt = TRUE;
  } else {
    mywarn("*                     forcing to interrupt simulation                               *");
    mywarn("*************************************************************************************");
    myexit(USER_EXIT, "user forced to exit simulation");
  }
#endif
#endif
}

/** This is the handler for the floating point exception.
  * If a math exeption is caused, it is handled in this routine and it should give out a warning.
  */
static void SigFPE_handler(UNUSED(int signum), siginfo_t* info, UNUSED(void* data)) {
  char   *msg;
  fenv_t  save_env;

  // print a message if errno is set
  if (info->si_errno != 0) {
    mywarn("Math FP errno set %d (%s).", info->si_errno, strerror(info->si_errno));
  }
  // freeze FPEs
  feupdateenv(&save_env);
  if (feholdexcept(&save_env) != 0) {
    mywarn("Math FP feholdexcept failed.");
  }
  switch (info->si_code) {
    case FPE_FLTINV:
      msg = "REAL invalid operation";
      break;
    case FPE_FLTSUB:
      msg = "REAL subscript out of range operand";
      break;
    case FPE_FLTRES:
      msg = "REAL inexact";
      break;
    case FPE_FLTDIV:
      msg = "REAL division-by-zero";
      break;
    case FPE_FLTUND:
      msg = "REAL underflow";
      break;
    case FPE_FLTOVF:
      msg = "REAL overflow";
      break;
    case FPE_INTDIV:
      msg = "integer division-by-zero";
      break;
    case FPE_INTOVF:
      msg = "integer overflow";
      break;
    default:
      msg = "unknown fpe";
      break;
  }

  feclearexcept(info->si_code);
  feupdateenv(&save_env);

  myexit(EXIT_ERROR, "Math FP exception '%s' (0x%x) caught.", msg, info->si_code);
}

/** This is the handler for sigsegV (memory violation).
  * It prints a backtrace and exits the program.
  */
static void SigSegV_handler(int signum, siginfo_t* info, void* data) {
  printtrace(signum, info, data);

  myexit(ERROR_SIGSEGV, "Signal: memory violation");
}

/** This function registers the signal handler for floating point exception, the interrupt signal and
  * sigsegV (memory violation).
  */
void setupSignals(long signaltype) {
  struct sigaction action;
  memset(&action, 0x00, sizeof(action));
  sigemptyset(&action.sa_mask);
  action.sa_flags = SA_SIGINFO|SA_RESTART;

  /* floating point exception */
  if (signaltype&EXCEPTION_FLOAT) {
    action.sa_sigaction = SigFPE_handler;
    if (sigaction(SIGFPE,&action,NULL) != 0) {
      myexit(ERROR_SIGNAL, "failed to register signal handler for FPE (%s).", strerror(errno));
    }
  }

  /* signal interrupt */
  if (signaltype&EXCEPTION_INTERRUPT) {
    action.sa_sigaction = SigInterrupt_handler;
#ifdef MPI
#ifdef HC3CLUSTER
    if ( (sigaction(SIGTERM,&action,NULL) != 0) ||
         (sigaction(SIGXCPU,&action,NULL) != 0) ) {
#elif defined(OPEN_MPI)
    if (sigaction(SIGTERM,&action,NULL) != 0) {
#else
    if (sigaction(SIGUSR1,&action,NULL) != 0) {
#endif
#else
    if (sigaction(SIGINT,&action,NULL) != 0) {
#endif
      myexit(ERROR_SIGNAL, "failed to register signal handler for Interrupt (%s).", strerror(errno));
    }
  }

  /* signal memory violation */
  if (signaltype&EXCEPTION_SIGSEGV) {
    action.sa_sigaction = SigSegV_handler;
    if (sigaction(SIGSEGV,&action,NULL) != 0) {
      myexit(ERROR_SIGNAL, "failed to register signal handler for memory violation (%s).", strerror(errno));
    }
  }
}
