Autotester
==========

A tool for automated black-box testing.

Copyright (C) 2019 IAM-CMS, licensed under the `GPLv3 <LICENSE>`__.


About Autotester
----------------

- `Goals and Motivation <docs/about/motivation.rst>`__
- `Authors <docs/about/authors.rst>`__
- `Funding <docs/about/funding.rst>`__

Setup
-----

- `Dependencies <docs/setup/dependencies.rst>`__
- `Building from Source <docs/setup/building.rst>`__

Usage Documentation
-------------------
Find the latest documentation `here <https://autotester.readthedocs.io/en/latest/>`_.

.. toctree::
   :name: documentation
   :caption: Autotester Documentation
   :maxdepth: 1

- `Creating test cases <docs/autotester/usage/create_tests.rst>`__
- `Running Autotester <docs/autotester/usage/run.rst>`__
- `Running Autotester using docker <docs/autotester/usage/run_docker.rst>`__
- `Obtaining test results <docs/autotester/usage/results.rst>`__
- `Generating result summaries <docs/autotester/usage/generate.rst>`__
- `How Autotester runs the Tests <docs/autotester/processing.rst>`__






