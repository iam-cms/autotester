FROM debian:buster-slim

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN addgroup --gid $GROUP_ID autotester || true
RUN useradd --shell /bin/bash --uid $USER_ID --gid $GROUP_ID autotester || true

RUN apt-get update
RUN apt-get install libxml2 pandoc libxml2-utils --yes

VOLUME ["/shared"]
WORKDIR /shared
COPY autotester /usr/bin/autotester
USER $USER_ID:$GROUP_ID

ENTRYPOINT ["autotester"]
