/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       test_autotester.c
 *  @ingroup
 *  @brief
 *
 ********************************************************************
 *
 *  @lastmodified 04.08.20 Philipp Zschumme
 *
 ********************************************************************/

#include <assert.h>

// TODO comment
#define main _main_autotester
#include "../src/autotester.c"
#undef main

void test_cmpstring(void) {
  char *test[] = {"alpha", "beta", "gamma"};

  assert(cmpstring(&test[0], &test[1]) < 0);
  assert(cmpstring(&test[1], &test[1]) == 0);
  assert(cmpstring(&test[0], &test[2]) < 0);
  assert(cmpstring(&test[2], &test[0]) > 0);
  assert(cmpstring(&test[1], &test[2]) < 0);
  assert(cmpstring(&test[2], &test[1]) > 0);
}

void test_autotester(void) {
  test_cmpstring();
}
