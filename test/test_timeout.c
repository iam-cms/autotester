/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       test_autotester.c
 *  @ingroup    
 *  @brief      
 *
 ********************************************************************
 *
 *  @lastmodified 29.07.20 Philipp Zschumme
 *
 ********************************************************************/
#include <assert.h>
#include <stdbool.h>
#include <wrapper.h>
#include "../src/timeout.h"
#include "../src/call.h"

#define MAX_RETURN_CODE 255
#define MAX_SIGNAL 64 // maximum according to `kill -l`

static void assert_timeout_only_for(Timeout* timeout, int return_code_timeout) {
  for (int return_code = 0; return_code < MAX_RETURN_CODE; return_code++) {
    bool result = Timeout_occurred(timeout, return_code);
    assert((return_code == return_code_timeout && result) || (return_code != return_code_timeout && !result));
  }
}

static void test_timeout_occurred(void) {
  Timeout *timeout = Malloc(sizeof(Timeout));
  *timeout = (Timeout) {
    .time_unit = Strdup("s"),
    .max_time = 0.01f
  };

  // expect timeout to occur when return code is 124 for all signals but 9 (KILL)
  // when signal is 9, expect timeout to occur when return is 128+9
  for (timeout->kill_signal = 0; timeout->kill_signal < MAX_SIGNAL; timeout->kill_signal++) {
    int return_code_timeout_expected = 124;
    if (timeout->kill_signal == 9) return_code_timeout_expected = 128+9;
    assert_timeout_only_for(timeout, return_code_timeout_expected);
  }

  // test integration with Call
  Call *call = Malloc(sizeof(Call));
  *call = (Call) {
    .command = Strdup("sleep 5"),
    .timeout = timeout
  };
  double systemtime, usertime, totaltime;
  char *output_log = "/dev/null";
  long rc = Call_execute(call, &systemtime, &usertime, &totaltime, output_log);
  assert(Timeout_occurred(timeout, rc));

  Free(call->command);
  timeout->max_time = 1.0f;
  call->command = Strdup("true"); // bash program 'true' should take less than 1 second
  rc = Call_execute(call, &systemtime, &usertime, &totaltime, output_log);
  assert(!Timeout_occurred(timeout, rc));

  Call_deinit(call);
}

void test_timeout(void) {
  test_timeout_occurred();
}
