/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       test_variablescope.c
 *  @ingroup    
 *  @brief      
 *
 ********************************************************************
 *
 *  @lastmodified 30.07.20 Philipp Zschumme
 *
 ********************************************************************/
#include <libxml/xmlstring.h>
#include <libxml/tree.h>
#include <wrapper.h>
#include <assert.h>
#include "../src/variablescope.h"
#include "../src/integrationtest.h"

static void test_replaceVariables_parent(VariableScope* parent);

static void test_replaceVariables(void) {
  xmlChar *test_xml = BAD_CAST
  "<variables>"
    "<variable name=\"test_non_empty\">test123</variable>" // valid variable
    "<variable name=\"test_empty\"/>" // empty value
    "<variable name=\"\">test</variable>" // empty name
    "<variable name=\"test_unicode\">öÖäÄüÜèÉàÁôÔ</variable>" // utf-8
    "<variable name=\"test_recursive\">This is a recursive test: $test_non_empty$</variable>"
    "<variable name=\"test_recursive_inf\">Testing recursion boundaries: $test_recursive_inv$</variable>"
  "</variables>";
  xmlDocPtr doc = xmlParseDoc(test_xml);
  xmlNodePtr testNode = xmlDocGetRootElement(doc);

  VariableScope *testScope = Malloc(sizeof(VariableScope));
  *testScope = (VariableScope) {
    .node = testNode,
  };

  char *test_str_simple = Strdup("Test $test_non_empty$ ");
  assert(VariableScope_replaceVariables(testScope, &test_str_simple));
  assert(strcmp(test_str_simple, "Test test123 ") == 0);
  Free(test_str_simple);

  char *test_str_empty = Strdup("Contains $test_empty$ empty variable");
  assert(!VariableScope_replaceVariables(testScope, &test_str_empty));
  Free(test_str_empty);

  char *test_str_syntax_fail = Strdup("Test $variable");
  assert(!VariableScope_replaceVariables(testScope, &test_str_syntax_fail));
  Free(test_str_syntax_fail);

  char *test_str_unicode = Strdup("Test $test_unicode$");
  assert(VariableScope_replaceVariables(testScope, &test_str_unicode));
  assert(strcmp(test_str_unicode, "Test öÖäÄüÜèÉàÁôÔ") == 0);
  Free(test_str_unicode);

  char *test_str_recursive = Strdup("Test $test_recursive$");
  assert(VariableScope_replaceVariables(testScope, &test_str_recursive));
  assert(strcmp(test_str_recursive, "Test This is a recursive test: test123") == 0);
  Free(test_str_recursive);

  char *test_str_recursive_inf = Strdup("Test $test_recursive_inf$");
  assert(!VariableScope_replaceVariables(testScope, &test_str_recursive_inf));
  Free(test_str_recursive_inf);

  // reuse the VariableScope of this test as parent
  test_replaceVariables_parent(testScope);

  Free(testScope);
  xmlFreeDoc(doc);
}

// ensure that the special variable $program$ will be replaced
static void test_replaceVariables_program(void) {
  xmlChar *test_xml = BAD_CAST
    "<variables>"
      "<variable name=\"test_non_empty\">test123</variable>" // valid variable
    "</variables>";
  xmlDocPtr doc = xmlParseDoc(test_xml);
  xmlNodePtr testNode = xmlDocGetRootElement(doc);

  Program *program = Malloc(sizeof(Program));
  *program = (Program) {
    .path = Strdup("testProgram")
  };
  IntegrationTest *test = Malloc(sizeof(IntegrationTest));
  *test = (IntegrationTest) {
    .program = program
  };
  VariableScope *testScope = Malloc(sizeof(VariableScope));
  *testScope = (VariableScope) {
    .node = testNode,
    .integrationtest = test
  };

  char *test_str_program = Strdup("Test $test_non_empty$ $program$ Test");
  assert(VariableScope_replaceVariables(testScope, &test_str_program));
  assert(strcmp(test_str_program, "Test test123 testProgram Test") == 0);
  Free(test_str_program);

  Program_deinit(program);
  Free(test->program);
  Free(test);
  Free(testScope);
  xmlFreeDoc(doc);
}

static void test_replaceVariables_parent(VariableScope* parent) {
  xmlChar *test_xml = BAD_CAST
    "<variables>"
      "<variable name=\"test_child_var\">ABC_5 da0jio</variable>"
    "</variables>";
  xmlDocPtr doc = xmlParseDoc(test_xml);
  xmlNodePtr testNode = xmlDocGetRootElement(doc);

  VariableScope *childScope = Malloc(sizeof(VariableScope));
  *childScope = (VariableScope) {
    .node = testNode,
    .parent_scope = parent
  };

  char *test_str_simple = Strdup("Test $test_non_empty$ $test_child_var$");
  assert(VariableScope_replaceVariables(childScope, &test_str_simple));
  assert(strcmp(test_str_simple, "Test test123 ABC_5 da0jio") == 0);
  Free(test_str_simple);

  Free(childScope);
  xmlFreeDoc(doc);
}

void test_variablescope(void) {

  initEnvironment(); // to avoid warnings about missing environment keys

  test_replaceVariables();
  test_replaceVariables_program();

  deinitEnvironment();
}
