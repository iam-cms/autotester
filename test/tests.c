/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       test_main.c
 *  @ingroup    
 *  @brief      
 *
 ********************************************************************
 *
 *  @lastmodified 29.07.20 Philipp Zschumme
 *
 ********************************************************************/
#include <stdio.h>
#include "tests.h"

int main(int argc, char** argv) {
  test_timeout();
  test_xmlhelper();
  test_variablescope();
  test_autotester();
  printf("ALL TESTS PASSED\n");
  return 0;
}
