/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       tests.h
 *  @ingroup    
 *  @brief      
 *
 ********************************************************************
 *
 *  @lastmodified 29.07.20 Philipp Zschumme
 *
 ********************************************************************/

#ifndef POSTPROCESSING_DEBUG_TESTS_H
#define POSTPROCESSING_DEBUG_TESTS_H

void test_timeout(void);
void test_xmlhelper(void);
void test_variablescope(void);
void test_autotester(void);

#endif //POSTPROCESSING_DEBUG_TESTS_H
