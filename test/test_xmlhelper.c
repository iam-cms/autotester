/********************************************************************//**
 *
 *  @pace3d     Parallel Algorithms for Crystal Evolution in 3D
 *
 *  @copyright  2013 IMP - Institute of Materials and Processes @n
 *                   University of Applied Sciences @n
 *                   Karlsruhe @n
 *
 *  @file       test_xmlhelper.c
 *  @ingroup    
 *  @brief      
 *
 ********************************************************************
 *
 *  @lastmodified 29.07.20 Philipp Zschumme
 *
 ********************************************************************/
#include <libxml/parser.h>
#include <assert.h>
#include <string.h>
#include <stddef.h>
#include <wrapper.h>
#include "../src/xmlhelper.h"

static void test_parseVariableValue(void) {
  xmlChar *test_xml = BAD_CAST
    "<variables>"
      "<variable name=\"test_non_empty\">test123</variable>" // valid variable
      "<variable name=\"test_empty\"/>" // empty value
      "<variable name=\"\">test</variable>" // empty name
      "<variable name=\"test_unicode\">öÖäÄüÜèÉàÁôÔ</variable>" // utf-8
    "</variables>";
  xmlDocPtr doc = xmlParseDoc(test_xml);
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  assert(strcmp(parseVariableValue(testNode, "test_non_empty"), "test123") == 0);
  assert(parseVariableValue(testNode, NULL) == NULL);
  assert(parseVariableValue(testNode, "") == NULL);
  assert(strcmp(parseVariableValue(testNode, "test_unicode"), "öÖäÄüÜèÉàÁôÔ") == 0);

  assert(parseVariableValue(testNode, "test_not_defined") == NULL);
  assert(parseVariableValue(NULL, NULL) == NULL);
  xmlFreeDoc(doc);
}

static void test_xml_attribute(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  char *result_attribute1 = xml_attribute(testNode, "attribute1", &resultCode);
  assert(strcmp(result_attribute1, "test") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_attribute1);

  char *result_attribute2 = xml_attribute(testNode, "attribute2", &resultCode);
  assert(strcmp(result_attribute2, "127") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_attribute2);

  assert(xml_attribute(testNode, "not_defined", &resultCode) == NULL);
  assert(resultCode == XML_RESULT_NOT_FOUND);

  char *result_attribute3 = xml_attribute(testNode, "attribute3", &resultCode);
  assert(strcmp(result_attribute3, "0.05") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_attribute3);

  char *result_attribute4 = xml_attribute(testNode, "attribute4", &resultCode);
  assert(strcmp(result_attribute4, "") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_attribute4);
}

static void test_xml_attribute_long(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  long result_attribute1 = 1010;
  xml_attribute_long(testNode, "attribute1", &resultCode, &result_attribute1);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_attribute1 == 1010);

  long result_attribute2 = 1010;
  xml_attribute_long(testNode, "attribute2", &resultCode, &result_attribute2);
  assert(resultCode == XML_RESULT_FOUND);
  assert(result_attribute2 == 127);

  long result_attribute_not_defined = 1010;
  xml_attribute_long(testNode, "not_defined", &resultCode, &result_attribute_not_defined);
  assert(resultCode == XML_RESULT_NOT_FOUND);
  assert(result_attribute_not_defined == 1010);

  long result_attribute3 = 1010;
  xml_attribute_long(testNode, "attribute3", &resultCode, &result_attribute3);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_attribute3 == 1010);

  long result_attribute4 = 1010;
  xml_attribute_long(testNode, "attribute4", &resultCode, &result_attribute4);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_attribute4 == 1010);
}


static void test_xml_find_nodes(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  xmlXPathObjectPtr result = xml_find_nodes(testNode, "a", &resultCode);
  assert(result);
  assert(result->nodesetval->nodeNr == 3);
  assert(resultCode == XML_RESULT_FOUND);
}

static void test_xml_find_node(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  xmlNodePtr result = xml_find_node(testNode, "a[@id=\"1\"]", &resultCode);
  assert(result);
  assert(resultCode == XML_RESULT_FOUND);

  result = xml_find_node(testNode, "a[@id=\"4\"]", &resultCode);
  assert(!result);
  assert(resultCode == XML_RESULT_NOT_FOUND);

  result = xml_find_node(testNode, "a[@id=\"3\"]/b/d/e", &resultCode);
  assert(result);
  assert(resultCode == XML_RESULT_FOUND);

  result = xml_find_node(testNode, "a/b/c", &resultCode);
  assert(result);
  assert(resultCode == XML_RESULT_FOUND);

  result = xml_find_node(testNode, "a/b/e", &resultCode);
  assert(!result);
  assert(resultCode == XML_RESULT_NOT_FOUND);

  result = xml_find_node(testNode, "a/b", &resultCode);
  assert(result);
  assert(resultCode == XML_RESULT_FOUND_AMBIGUOUS);
}


static void test_xml_content(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  char *result_test1 = xml_node_content(testNode, "test1", &resultCode);
  assert(strcmp(result_test1, "Some content") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_test1);

  char *result_test2 = xml_node_content(testNode, "test2", &resultCode);
  assert(strcmp(result_test2, "127") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_test2);

  char *result_test3 = xml_node_content(testNode, "test3", &resultCode);
  assert(strcmp(result_test3, "0.05") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_test3);

  char *result_test4 = xml_node_content(testNode, "test4", &resultCode);
  assert(!result_test4);
  assert(resultCode == XML_RESULT_NOT_FOUND);

  char *result_test5 = xml_node_content(testNode, "test5", &resultCode);
  // this is expected to return only whitespace characters
  for (char *i = result_test5; *i != '\0'; i++) {
    assert(*i == ' ');
  }
  assert(resultCode == XML_RESULT_FOUND || resultCode == XML_RESULT_FOUND_AMBIGUOUS);
  Free(result_test5);

  char *result_test6 = xml_node_content(testNode, "test5/test6", &resultCode);
  assert(strcmp(result_test6, "Test 123!") == 0);
  assert(resultCode == XML_RESULT_FOUND);
  Free(result_test6);
}

static void test_xml_content_long(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  long result_test1 = 1010;
  xml_node_content_long(testNode, "test1", &resultCode, &result_test1);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_test1 == 1010);

  long result_test2 = 1010;
  xml_node_content_long(testNode, "test2", &resultCode, &result_test2);
  assert(resultCode == XML_RESULT_FOUND);
  assert(result_test2 == 127);

  long result_test3 = 1010;
  xml_node_content_long(testNode, "test3", &resultCode, &result_test3);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_test3 == 1010);

  long result_test4 = 1010;
  xml_node_content_long(testNode, "test4", &resultCode, &result_test4);
  assert(resultCode == XML_RESULT_NOT_FOUND);
  assert(result_test4 == 1010);
}

static void test_xml_content_float(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  float result_test1 = 10.10f;
  xml_node_content_float(testNode, "test1", &resultCode, &result_test1);
  assert(resultCode == XML_TYPE_ERROR);
  assert(result_test1 == 10.10f);

  float result_test2 = 10.10f;
  xml_node_content_float(testNode, "test2", &resultCode, &result_test2);
  assert(resultCode == XML_RESULT_FOUND);
  assert(result_test2 == 127.0f);

  float result_test3 = 10.10f;
  xml_node_content_float(testNode, "test3", &resultCode, &result_test3);
  assert(resultCode == XML_RESULT_FOUND);
  assert(result_test3 == 0.05f);

  float result_test4 = 10.10f;
  xml_node_content_float(testNode, "test4", &resultCode, &result_test4);
  assert(resultCode == XML_RESULT_NOT_FOUND);
  assert(result_test4 == 10.10f);
}


static void test_xml_count_nodes(xmlDocPtr doc) {
  xmlNodePtr testNode = xmlDocGetRootElement(doc);
  int resultCode = XML_RESULT_NOT_SET;

  assert(xml_count_nodes(testNode, "a") == 3);
  assert(xml_count_nodes(testNode, "a[@id]") == 3);
  assert(xml_count_nodes(testNode, "a[@id >= 1]") == 2);
  assert(xml_count_nodes(testNode, "a[@id = 3]") == 1);
  assert(xml_count_nodes(testNode, "a/b") == 2);
  assert(xml_count_nodes(testNode, "a/b/c") == 1);
  assert(xml_count_nodes(testNode, "a/b/c/d") == 0);
  assert(xml_count_nodes(testNode, "a/b/d/e") == 1);
}

void test_xmlhelper(void) {
  xmlChar *test_xml = BAD_CAST "<test>"
                               "  <a id=\"0\" />"
                               "  <a id=\"1\" />"
                               "  <a id=\"3\">"
                               "    <b><c/></b>"
                               "    <b><d><e/></d></b>"
                               "  </a>"
                               "</test>";
  xmlDocPtr test_doc = xmlParseDoc(test_xml);
  xmlChar *test_attribute_xml = BAD_CAST "<test attribute1=\"test\" attribute2=\"127\" attribute3=\"0.05\" attribute4=\"\"/>";
  xmlDocPtr test_attribute_doc = xmlParseDoc(test_attribute_xml);
  xmlChar *test_content_xml = BAD_CAST "<root>"
                                       "  <test1>Some content</test1>"
                                       "  <test2>127</test2>"
                                       "  <test3>0.05</test3>"
                                       "  <test4></test4>"
                                       "  <test5>"
                                       "    <test6>Test 123!</test6>"
                                       "  </test5>"
                                       "</root>";
  xmlDocPtr test_content_doc = xmlParseDoc(test_content_xml);

  test_parseVariableValue();
  test_xml_attribute(test_attribute_doc);
  test_xml_attribute_long(test_attribute_doc);
  test_xml_find_nodes(test_doc);
  test_xml_find_node(test_doc);
  test_xml_content(test_content_doc);
  test_xml_content_long(test_content_doc);
  test_xml_content_float(test_content_doc);
  test_xml_count_nodes(test_doc);

  xmlFreeDoc(test_doc);
  xmlFreeDoc(test_attribute_doc);
  xmlFreeDoc(test_content_doc);
}
